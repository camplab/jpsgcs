import jpsgcs.graph.Graphs;
import jpsgcs.graph.Network;
import jpsgcs.jtree.JTree;
import jpsgcs.jtree.Clique;
import jpsgcs.jtree.Separator;
import jpsgcs.jtree.CliqueSeparatorGraph;
import jpsgcs.viewgraph.PaintableGraph;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.viewgraph.StringNode;
import jpsgcs.viewgraph.VertexRepresentation;
import jpsgcs.graph.GraphLocator;
import jpsgcs.graph.DAGLocator;

import java.util.Map;
import java.util.LinkedHashMap;
import java.awt.Frame;
import java.awt.Color;

public class ViewCSG
{
	public static void main(String[] args)
	{
		try
		{
			Network<String,Object> g = Graphs.read();

			JTree<String> jt = new JTree<String>(g);
			CliqueSeparatorGraph<String> csg = new CliqueSeparatorGraph<String>(jt);

			Map<Clique<String>,VertexRepresentation> map = new LinkedHashMap<Clique<String>,VertexRepresentation>();
			for (Clique<String> c : csg.getVertices())
			{
				map.put(c,new StringNode(clean(c.toString()),Color.cyan,1));

				for (Clique<String> d : csg.inNeighbours(c))
					map.put(d,new StringNode(clean(d.toString()),Color.yellow,0));
			}

			PaintableGraph<Clique<String>,Object> pg = 
					new PaintableGraph<Clique<String>,Object>(csg,map);

                        GraphLocator<Clique<String>,Object> loc = new DAGLocator<Clique<String>,Object>();

			Frame f = new GraphFrame<Clique<String>,Object>(pg,loc);
		}
		catch (Exception e)
		{
			System.err.println("Caught in ViewGraph.main()");
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static String clean(String s)
	{
		return s.replaceAll("[\\[\\]]","");
	}
}
