
import jpsgcs.util.InputFormatter;
import java.util.Map;
import java.util.LinkedHashMap;

public class RenameGraph
{
	public static void main(String[] args)
	{
		try
		{
			InputFormatter f = new InputFormatter(args[0]);
			Map<String,String> map = new LinkedHashMap<String,String>();

			while (f.newLine())
			{
				String a = f.nextString();
				String b = f.nextString();
				map.put(a,b);
			}

			f = new InputFormatter();
			while (f.newLine())
			{
				while (f.newToken())
				{
					String a = f.getString();
					String b = map.get(a);
					if (b == null)
						b = a;
					System.out.print(b+"\t");
				}
				System.out.println();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}
}
