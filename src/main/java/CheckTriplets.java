import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.LinkageIndividual;

/**
 	This program checks a pedigree specified by a list of triplets for self 
	consisency and completeness. 

<ul>
        Usage : <b> java CheckTriplets < triplet.file > output.ped </b> </li>
</ul>
        where
<ul>
<li> <b> triplet.file </b> is a file specifiying the pedigree as a list
	of triplet. One triplet per line. Order is assumed to be individual id,
	father's id, mother's id. Any other data following this is ignored.  </li>
<li> <b> output.ped </b> is the checked pedigree output file. </li>
</ul>

<p>	Error messages are output to the screen.
	The checked pedigree is output in LINKAGE format.

*/

public class CheckTriplets
{
	public static void main(String[] args)
	{
		try
		{
			LinkagePedigreeData p = new LinkagePedigreeData(new LinkageFormatter(),null,LinkageIndividual.TRIPLET);
			p.writeTo(System.out);
		}
		catch (Exception e)
		{
			System.err.println("Caught in CheckTriplets:main()");
			e.printStackTrace();
		}
	}
}
