import jpsgcs.pedmcmc.LDModel;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.Function;
import java.util.Random;

public class CompileLDModel
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			LinkageFormatter parin = new LinkageFormatter();
			LinkageParameterData par = new LinkageParameterData(parin);
			LDModel ld = new LDModel(parin);
			if (ld.getLocusVariables() == null)
				ld = new LDModel(new LinkageInterface(new LinkageDataSet(par,null)));

			GraphicalModel g = new GraphicalModel(ld,rand,false);
			g.collect();
			g.distribute();
			ld.clear();
			for (Function f : g.cliquePotentials())
				ld.add(f);

			par.writeTo(System.out);
			System.out.println();
			ld.writeTo(System.out);
		}
		catch (Exception e)
		{
			System.err.println("Caught in CompileLDModel.main()");
			e.printStackTrace();
		}
	}
}
