import jpsgcs.util.Main;

import jpsgcs.util.InputFormatter;
import jpsgcs.graph.Network;
import jpsgcs.graph.GraphLocator;
import jpsgcs.graph.RootedLocalLocator;

import jpsgcs.viewgraph.PaintableGraph;
import jpsgcs.viewgraph.Blob;
import jpsgcs.viewgraph.VertexRepresentation;
import jpsgcs.viewgraph.GraphFrame;

import java.io.IOException;
import java.util.Map;
import java.util.Vector;
import java.util.LinkedHashMap;
import java.awt.Panel;
import java.awt.Frame;


public class ViewCollab
{
	public static void read(Network<String,Object> g) throws IOException
	{
		InputFormatter f = new InputFormatter();
		StringBuffer s = new StringBuffer();
		
		while (f.newLine())
			s.append(f.thisLine()+" ");

		String l = s.toString();
		l = l.replace("\t"," ");
		l = l.replaceAll("  *"," ");

		for (String w : l.split(","))
		{
			if (!w.trim().startsWith("author"))
				continue;
			
			String x = w.split("=")[1];
			x = x.replace("\"","");
			x = x.replace("{","");
			x = x.replace("}","");
			x = x.replace(" and "," = ");

			Vector<String> v = new Vector<String>();
			for (String y : x.split("="))
				v.add(y.trim());

			for (int i=0; i<v.size(); i++)
				for (int j=i+1; j<v.size(); j++)
					g.connect(v.get(i),v.get(j));
		}

		for (String x : g.getVertices())
			System.out.println(x);
	}

	public static void main(String[] args)
	{
		try
		{
			boolean useblobs = false;

			String[] bargs = Main.strip(args,"-b");
			if (bargs != args)
			{
				useblobs = true;	
				args = bargs;
			}
		
			Network<String,Object> g = new Network<String,Object>();

			read(g);

			PaintableGraph<String,Object> pg = null;

			if (useblobs)
			{
				Map<String,VertexRepresentation> map = new LinkedHashMap<String,VertexRepresentation>();
				Blob blob = new Blob();
				for (String s : g.getVertices())
					map.put(s,blob);
				pg = new PaintableGraph<String,Object>(g,map);
			}
			else
			{
				pg = new PaintableGraph<String,Object>(g);
			}

			Frame f = new GraphFrame<String,Object>(pg);
		}
		catch (Exception e)
		{
			System.err.println("Caught in ViewCollab.main()");
			e.printStackTrace();
			System.exit(1);
		}
	}
}
