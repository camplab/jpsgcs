import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LocusSampler;
import jpsgcs.pedmcmc.LDLocusSampler;
import jpsgcs.pedmcmc.LocusMeiosisSampler;
import jpsgcs.pedmcmc.ExtendedLMSampler;
import jpsgcs.pedapps.TLods;
import java.util.Random;

/**
        This program samples the posterior distribution of inheritance vectors
        given the observed genotypes and, hence, calculates Tlod linkage
	statistics between a trait phenotype and genetic markers in pedigrees.
<ul>
        Usage : <b> java McTLods input.par input.ped [n_samples] [n_burnin] [error] [-v/t] </b>
</ul>
<p>
        where
<ul>
        <li> <b> input.par </b> is the input LINKAGE parameter file. </li>
        <li> <b> input.ped </b> is the input LINAKGE pedigree file. </li>
        <li> <b> n_samples </b> is an optional parameter specifiying the number of iterations
        to sample. The default is 1000. </li>
	<li> <b> n_burnin  </b> is an optional parameter specifying the number iterations to
	do before sampling begins. The default is 0. </li>
	<li> <b> error </b> is an optional parameter specifying the probability of a genotyping
	error. The default is 0. </li>
	<li> <b> -v/t </b> switches between verbose and terse output. The default is verbose. </li>
</ul>
<p>
        Sampling is done using the Markov chain Monte Carlo method of
        blocked Gibbs updating.
<p>
	The output gives a table for each pedigree. Each row of the table corresponds
	to a marker, each column to a value of the recombination fraction between the
	trait and the marker. Each entry is the Tlod, or fully informative two point
	lod score.
	The values of the recombination fraction used are (0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 
	0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1).
*/

public class McTLods extends McProg
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			double[] thetas = {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1};

			readOptions(args,"McTlods");

			error = 0.001;
			maxerrall = 4;

			for (GeneticDataSource data : alldata)
			{
				if (verbose)
					System.err.println("Setting up sampler");

				data.downcodeAlleles();

				LocusSampler mc = makeSampler(data,false,rand);
				
				mc.initialize();

				if (n_burnin > 0 && verbose)
					System.err.println("Burn in");
				
				for (int s=0; s<n_burnin; s++)
				{
					mc.sample();
					if (verbose)
						System.err.print(".");
				}

				if (n_burnin > 0 && verbose)
					System.err.println();
				
				TLods tlod = new TLods(data,mc.getInheritances(),thetas,veryverbose);

				if (verbose)
					System.err.println("Sampling");

				for (int s=0; s<n_samples; s++)
				{
					mc.sample();
					tlod.update();
					if (verbose) 
						System.err.print(".");
				}

				if (verbose) 
					System.err.println();

				double[][] lods = tlod.results();


				for (int i=1; i<lods.length; i++)
				{
					for (int k=0; k<lods[i].length; k++)
						System.out.printf(" %5.4f",lods[i][k]);
					System.out.println();
				}

				System.out.println();
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in McTLods:main()");
			e.printStackTrace();
		}
	}
}
