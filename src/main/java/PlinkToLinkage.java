import jpsgcs.plink.PlinkData;
import java.io.PrintStream;

public class PlinkToLinkage
{
	public static void main(String[] args)
	{
		try
		{
			PlinkData x = null;
			PrintStream par = null;
			PrintStream ped = null;

			switch(args.length)
			{
			case 4: x = new PlinkData(args[0],args[1]);
				par = new PrintStream(args[2]);
				ped = new PrintStream(args[3]);
				break;
			case 5: x = new PlinkData(args[0],args[1],args[2]);
				par = new PrintStream(args[3]);
				ped = new PrintStream(args[4]);
				break;
			}
			
			x.writeLinkageFiles(par,ped);
		}
		catch (Exception e)
		{
			System.err.println("Caught in PlinkToLinkage:main().");
			e.printStackTrace();
		}
	}
}
