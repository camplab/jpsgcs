
import jpsgcs.util.ArgParser;
import jpsgcs.util.InputFormatter;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.Variable;
import jpsgcs.markov.Function;

import java.util.Random;
import java.util.Set;
import java.util.LinkedHashSet;

public class SharingProbs
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();
			ArgParser a = new ArgParser(args);
			int maxtopx = a.intAfter("-max",100);
			boolean dopeds = a.gotOpt("-peds");

			double detectionprob = 0.001;

			InputFormatter f = new InputFormatter(args[0]);
			Set<double[]> disc = new LinkedHashSet<double[]>();
			Set<double[]> test = new LinkedHashSet<double[]>();
	
			while (f.newLine())
			{
				String name = f.nextString();
				int isdisc = f.nextInt();
				double[] p = { f.nextDouble(), f.nextDouble(), f.nextDouble() };
				if (isdisc == 1)
					disc.add(p);
				else
					test.add(p);
			}
	
			int ndisc = disc.size();
			
			disc.addAll(test);
			double[][] probs = (double[][]) disc.toArray(new double[0][]);

			int topx = (dopeds ? probs.length+1 : maxtopx);
			double[] x = new double[topx];

			f = new InputFormatter(args[1]);
			int k = 0;
			while (f.newLine())
			{
				f.nextString();
				f.nextString();
				f.nextString();
				double rate = f.nextDouble();

				System.err.println("Doing gene "+(k++)+"\t"+rate);

				LocusField lf = new LocusField(probs,rate,detectionprob,ndisc);
				GraphicalModel g = new GraphicalModel(lf,rand,false);

				Variable total = (dopeds ? lf.getResult2() : lf.getResult());
				Function result = g.variableMargins().get(total);

				int j=0;
				for (j=0; j<x.length; j++)
					x[j] = 0;
				for (j=0, total.init(); total.next() && j<x.length; j++)
					x[j] = result.getValue();

				for (j=0; j<x.length; j++)
					System.out.print(x[j]+" ");
				System.out.println();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
