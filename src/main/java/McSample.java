import jpsgcs.linkage.LinkageInterface;
import jpsgcs.util.Main;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LocusSampler;
import jpsgcs.pedapps.FullInfoLinkageData;
import jpsgcs.pedapps.LogLikelihood;
import jpsgcs.pedapps.AncestralAlleles;
import jpsgcs.pedapps.HaplotypeGraphFrame;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Random;

/**
	This is a program that runs an MCMC sampler on a pedigree with some
	genotyped individuals and outputs sampled inheritance states.
	The output is in the form of fully informative LINKAGE files.
	For each pedigree a new paramter file is created. In this all loci
	except the first one (which is presumed to be the trait or disease locus)
	are replaced with numbered allele loci where the number of alleles is 
	twice the number of founders. Each required simulation is output into 
	a LINKAGE pedigree file. These  pedigree files specify all genotypes for
	 all individuals in the pedigree at each of the artificial loci specified
	in the corresponding parameter file. By tracing the inheritance of alleles
	back to the appropriate founder, all the inheritances at all the loci in
	the data can be reconstructed.

<ul>
	Usage: <b> java McSample input.par input.ped [n_burning] [n_samples] [space] [n_max] [prefix] [-v/t] [-g] </b>
</ul>
<p>
	where
<ul>
	<li><b> input.par </b> is the input LINKAGE parameter file. </li>
	<li><b> input.ped </b> is the input LINKAGE pedigree file. </li>
	<li> <b> n_burnin </b> is the optional parameter specifiying the number of iterations to do 
	before samples are taken. The default is 0.</li>
	<li> <b> n_samples </b> is the number of samples required. The default is 1000. </li>
	<li> <b> space </b> is the number of MCMC updates to make between the points at which the 
	samples are taken. The default is 0, that is: every update results in a used sample. </li>
	<li> <b> n_max </b> is the number of updates to be made by the random uphill optimizer after
	MCMC sampling has finished. The default is 0. </li>
	<li> <b> prefix </b> specifies the prefix used for the names of the files where the output is put
	as described below.
	The default is "out". </li>
	<li> <b> -v </b> if this option is specified the program operates in verbose mode with messages printed
	to standard error at key steps in the program. This is the default. </li>
	<li> <b> -t </b> if this option is spececified the program operates in terse mode: no messages.</li>
	<li> <b> -g </b> if this option is specified, a GUI is launched that shows the pedigree and the 
	haplotypes being sampled as the MCMC updates are made. This is off by default.</li>

<p>
	From a random starting configuration the program makes <b>n_burnin </b> MCMC updates without outputting
	any results. It then makes <b> n_samples </b> x <b> space </b> random MCMC updates, outputting the 
	inheritances every <b> space </b> updates, for a total of <b> n_samples </b> samples. It then
	makes <b> n_max </b> updates of a random uphill search before outputting the final state, which
	is likely to be a local maximum of the posterior probability distribution of inheritances states given
	the pedigree structure and observed genotypes. In some simple problems this may also be a
	global maximum.
<p>
	The output is put in several files. Suppose that the <b> prefix </b> is specified as "myfiles", and
	that there are pedigrees with numbers 178 and 249 in the <b> input.ped </b> file. There will be
	parameter files called "myfiles.178.par" and "myfiles.249.par" that specify the artificial loci used
	to describe the inheritance states. Note that different files must be used for each pedigree because
	the number of founders, and hence the number of alleles needed to specify the states will typically
	differ. 
<p>
	If 5 simulations are required, the inheritances will be specified in files called "myfiles.178.1.ped", 
	up to "myfiles.178.5.ped" and "myfiles.249.1.ped" to "myfiles.249.5.ped". 
<p> 
	The final inheritance states will be in files called "myfiles.178.max.ped" and "myfiles.249.max.ped".
<p>
	Finally, the log likelihoods for each output inheritance state are written to standard output. In the
	above example the first 5 numbers will correspond to the loglikelihoods for the inheritances in
	"myfiles.178.1.ped" through "myfiles.178.5.ped". The next will be for "myfiles.178.max.ped". Then
	the six likelihoods for the samples for pedigree 249 would follow.
<p>
	The above example would be run using something like the following command:

<br>
<br>
	<b> java McSample input.par input.ped 100 5 10 1000 myfiles </b>

<p>
	The haplotypes generated in these simulations can be viewed using the <b> ViewHaplotypes </b> program.
	For example to view the second simulation for pedigree 249 type:
<br>
<br>
	<b> java ViewHaplotypes myfiles.249.par myfiles.249.2.ped </b>
<p>
	The lod scores for each simulation can also be calculated using the usual programs. For example
	to calculate the two point lod scores that correspond to the final sample for pedigree 178 type:
<br>
<br>
	<b> java TwoPointLods myfiles.178.par myfiles.178.max.ped  </b>
*/

public class McSample extends McProg
{
	public static int n_maxim = 0;
	public static int n_space = 1;
	public static boolean show = false;
	public static String prefix = "out";

	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			readOptions(args,"McSample");

			boolean linkfirst = false;
                        int places = (int) (1.00000000001 + (Math.log10(n_samples)));

			for (GeneticDataSource data : alldata)
			{
				if (verbose)
					System.err.println("Pedigree "+data.pedigreeName(0)+"\nSetting up sampler");

				data.downcodeAlleles();

				FullInfoLinkageData fild = new FullInfoLinkageData(data,linkfirst);
                               	PrintStream p = new PrintStream(prefix+"."+data.pedigreeName(0)+".par");
                               	fild.getParameterData().writeTo(p);
                               	p.close();

				LocusSampler mc = makeSampler(data,linkfirst,rand);

				LogLikelihood ll = new LogLikelihood(data,mc.getInheritances(),linkfirst,error,maxerrall);

				AncestralAlleles anc = null;
				HaplotypeGraphFrame frame = null;
				if (show)
				{
					anc = new AncestralAlleles(data,linkfirst);
					frame = new HaplotypeGraphFrame(data,anc,true,true,true,false,false);
				}

				mc.initialize();

				if (verbose)
					System.err.println("Burn in");

				for (int s=0; s<n_burnin; s++)
				{
					mc.sample();
					if (show)
						anc.update(mc.getInheritances());
				}

				if (verbose)
					System.err.println("Sampling");

				for (int s=0; s<n_samples; s++)
				{
					for (int t=0; t<n_space; t++)
						mc.sample();

					if (show)
						anc.update(mc.getInheritances());

					System.out.println(ll.logLikelihood());

                                	p = new PrintStream(prefix+"."+data.pedigreeName(0)+"."+format(s+1,places)+".ped");
					fild.set(mc.getInheritances());
                                	fild.getPedigreeData().writeTo(p);
                                	p.close();
				}

				if (verbose)
					System.err.println("Maximizing");

				for (int s=0; s<n_maxim; s++)
				{
					mc.maximize();
					if (show)
						anc.update(mc.getInheritances());
				}

				System.out.println(ll.logLikelihood());

                               	p = new PrintStream(prefix+"."+data.pedigreeName(0)+".max.ped");
				fild.set(mc.getInheritances());
                                fild.getPedigreeData().writeTo(p);
                               	p.close();

				if (verbose) 
					System.err.println("Done");
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in McSample:main()");
			e.printStackTrace();
		}
	}

        public static String format(int i, int p)
        {
                String r = ""+i;
                for (int j = (int)(1.000000000001 + Math.log10(i)); j<p; j++)
                        r = "0"+r;
                return r;
        }

	public static void readOptions(String[] args, String prog) throws IOException
	{
		String[] bargs = Main.strip(args,"-t");
		if (bargs != args)
		{
			verbose = false;
			args = bargs;
		}

		bargs = Main.strip(args,"-v");
		if (bargs != args)
		{
			verbose = true;
			args = bargs;
		}

		bargs = Main.strip(args,"-g");
		if (bargs != args)
		{
			show = true;
			args = bargs;
		}

		switch (args.length)
		{
		case 8: sampler = Integer.parseInt(args[7]);

		case 7: prefix = args[6];

		case 6: n_maxim = Integer.parseInt(args[5]);

		case 5: n_space = Integer.parseInt(args[4]);

		case 4: n_samples = Integer.parseInt(args[3]);
			
		case 3: n_burnin = Integer.parseInt(args[2]);

		case 2: readData(args[0],args[1]);
			break;

		default:
			System.err.println("Usage: java "+prog+" input.par input.ped [n_burnin] [n_samples] [n_space] [n_maxim] [prefix] [-v/t] [-g]");
			System.exit(1);
		}
	}
 }
