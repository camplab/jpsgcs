import jpsgcs.pedmcmc.LocusProduct;
import jpsgcs.pedapps.TwoLocusLodScores;
import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;

/**
        This program calculates two point hetlod scores on a grid of points.

<ul>
        Usage : <b> java HetLods input.par input.ped </b> </li>
</ul>
        where
<ul>
<li> <b> input.par </b> is a LINKAGE parameter file </li>
<li> <b> input.ped </b> is a LINKAGE pedigree file </li>
</ul>
<p>
        The values of theta used are {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1.0}.
<p>
        The values of alpha used are {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1.0}.
<p>
        The output is a matrix of hetlods for each marker. Theta varies across the columns
        and alpha varies down the rows of each matrix. Each matrix is separated by
        a blank line.
*/

public class HetLods 
{
	public static void main(String[] args)
	{
		try
		{
			double[] thetas = {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1.0};
			double[] alphas = {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1.0};

			GeneticDataSource[] x = null;
			switch(args.length)
			{
			case 2: x = Linkage.readAndSplit(args[0],args[1]);
				break;
			default: 
				System.err.println("Usage: java HetLods input.par input.ped");
				System.exit(1);
			}

			for (GeneticDataSource data : x)
				data.downcodeAlleles();

			for (int i=1; i<x[0].nLoci(); i++)
			{
				double[][] h = new double[alphas.length][thetas.length];

				for (GeneticDataSource data : x)
				{
					LocusProduct trait = new LocusProduct(data,0);
					LocusProduct mark = new LocusProduct(data,i);

					TwoLocusLodScores link = new TwoLocusLodScores(trait,mark,data.sexLinked());

					double[] lod = new double[thetas.length];

					for (int k=0; k<thetas.length; k++)
					{
						lod[k] = link.evaluate(thetas[k]);
					}

					for (int j=0; j<h.length; j++)
						for (int k=0; k<h[j].length; k++)
							h[j][k] += Math.log10(alphas[j] * Math.pow(10,lod[k]) + (1-alphas[j]));
				}

				for (int j=0;  j<h.length; j++)
				{
					for (int k=0; k<h[j].length; k++)
						System.out.printf("%8.4f ",h[j][k]);
					System.out.println();
				}
				System.out.println();
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in HetLods:main()");
			e.printStackTrace();
		}
	}
}
