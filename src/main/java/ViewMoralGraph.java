
import jpsgcs.util.InputFormatter;
import jpsgcs.util.ArgParser;
import jpsgcs.graph.Graphs;
import jpsgcs.graph.Network;
import jpsgcs.graph.GraphLocator;

import jpsgcs.viewgraph.PaintableGraph;
import jpsgcs.viewgraph.StringNode;
import jpsgcs.viewgraph.VertexRepresentation;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.graph.DAGLocator;

import java.util.Map;
import java.util.Set;
import java.util.Random;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.awt.Frame;
import java.awt.Color;

public class ViewMoralGraph
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			ArgParser ap = new ArgParser(args);
			
			int colouropt = ap.intAfter("-c",0);
			boolean arrows = !ap.gotOpt("-a");

			Network<String,Object> g = new Network<String,Object>(arrows);

			Map<String,String> pas = new LinkedHashMap<String,String>();
			Map<String,String> mas = new LinkedHashMap<String,String>();

			Set<String> males = new LinkedHashSet<String>();
			Set<String> females = new LinkedHashSet<String>();

			InputFormatter f = new InputFormatter();

			while(f.newLine())
			{
				String bod = f.nextString();
				String pa = f.nextString();
				String ma = f.nextString();
				
				g.add(bod);

				if (rand.nextDouble() < 0.5)
					males.add(bod);
				else
					females.add(bod);
				

				if (!pa.equals("0"))
				{
					g.connect(pa,bod);
					pas.put(bod,pa);
				}

				if (!ma.equals("0"))
				{
					g.connect(ma,bod);
					mas.put(bod,ma);
				}

				if (!pa.equals("0") && !ma.equals("0"))
				{
					g.connect(pa,ma);
					g.connect(ma,pa);
				}
			}


			for (String x : g.getVertices())
			{
				String pa = pas.get(x);
				if (pa != null)
				{
					females.remove(pa);
					males.add(pa);
				}

				String ma = mas.get(x);
				if (ma != null)
				{
					males.remove(ma);
					females.add(ma);
				}
			}

			Map<String,VertexRepresentation> map = new LinkedHashMap<String,VertexRepresentation>();
			for (String x : g.getVertices())
			{
				map.put(x,new StringNode(x));
			}

			if (colouropt == 1)
			{
				for (String x : males)
					map.get(x).setColor(Color.cyan);
				for (String x : females)
					map.get(x).setColor(Color.yellow);
			}

			if (colouropt == 2)
			{
				for (String x : g.getVertices())
					map.get(x).setColor(Color.white);
		
				Set<String> s = new LinkedHashSet<String>(males);
				while (!s.isEmpty())
				{
					for (String x : new LinkedHashSet<String>(s))
					{
						String pa = pas.get(x);
						if (pa == null)
						{
							map.get(x).setColor(Color.red);
							s.remove(x);
						}
						else
						{
							if (((StringNode)map.get(pa)).getColor().equals(Color.cyan))
							{
								map.get(x).setColor(Color.red);
								s.remove(x);
							}
							if (((StringNode)map.get(pa)).getColor().equals(Color.red))
							{
								map.get(x).setColor(Color.cyan);
								s.remove(x);
							}
						}
					}
				}

				s = new LinkedHashSet<String>(females);
				while (!s.isEmpty())
				{
					for (String x : new LinkedHashSet<String>(s))
					{
						String ma = mas.get(x);
						if (ma == null)
						{
							map.get(x).setColor(Color.green);
							s.remove(x);
						}
						else
						{
							if (((StringNode)map.get(ma)).getColor().equals(Color.yellow))
							{
								map.get(x).setColor(Color.green);
								s.remove(x);
							}
							if (((StringNode)map.get(ma)).getColor().equals(Color.green))
							{
								map.get(x).setColor(Color.yellow);
								s.remove(x);
							}
						}
					}
				}
			}

			PaintableGraph<String,Object> pg = new PaintableGraph<String,Object>(g,map);

			if (arrows)
			{
                        	Frame frame = new GraphFrame<String,Object>(pg,new DAGLocator<String,Object>());
			}
			else
			{
				Frame frame = new GraphFrame<String,Object>(pg);
			}

		}
		catch (Exception e)
		{
			System.err.println("Caught in ViewGraph.main()");
			e.printStackTrace();
			System.exit(1);
		}
	}
}
