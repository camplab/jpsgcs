
import jpsgcs.markov.Variable;
import jpsgcs.markov.Function;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.functions.*;

public class LocusField extends MarkovRandomField
{
	double tol = 1-1e-12;
	int maxystates = 1000;

	Variable[] S = null;
	Variable[] Z = null;
	Variable[] C = null;
	Variable[] D = null;
	Variable[] X = null;
	Variable[] Y = null;

	Variable[] L = null;
	Variable[] K = null;

	double mean = 0;

	int qpois(double p, double l)
	{
		double x = Math.exp(-l);
		double y = x;
		int i = 0;
		while (y < p)
		{
			x *= l/(++i);
			y += x;
		}
		return i;
	}

	public LocusField(double[][] kprobs, double rate, double detect, int ndisc)
	{
// Mean doesn't take into account detection rate yet.

		double pdo = 1;
		for (int i=0; i<ndisc; i++)
			pdo *= kprobs[i][0];
		pdo = 1-pdo;

		mean = 0;
		for (int i=0; i<kprobs.length; i++)
		{
			double mui = kprobs[i][1] * rate + kprobs[i][2] * 2 * rate;
			if (i >= ndisc)
				mui *= pdo;
			mean += mui;
		}
		
		int n = kprobs.length;

		S = new Variable[n];
		D = new Variable[n];
		Z = new Variable[n];
		C = new Variable[n];
		X = new Variable[n];
		Y = new Variable[n];
		K = new Variable[n];
		L = new Variable[n];

		int maxcount = 1+qpois(tol,2*rate);

		Variable prev = new Variable(1);
		prev.setState(0);
		Variable prev2 = new Variable(1);
		prev2.setState(0);
		Variable prev3 = new Variable(1);
		prev3.setState(0);

		for (int i=0; i<n; i++)
		{
			S[i] = new Variable(3);
			add(new Univariate(S[i],kprobs[i]));

			Z[i] = new Variable(2);
			C[i] = new Variable(3);
			D[i] = new Variable(2);
		

			if (i < ndisc)
			{
				add(new Detect(D[i],S[i],detect,1));
				add(new Or(Z[i],prev,D[i]));
				//add(new Equals(C[i],S[i]));
				add(new Product(C[i],S[i],D[i]));
			}
			else
			{
				add(new Detect(D[i],S[i],detect,1));
				add(new Equals(Z[i],prev));
				add(new Product(C[i],S[i],Z[i]));
			}

			prev = Z[i];

			X[i] = new Variable(maxcount);
			add(new ConditionalPoisson(X[i],C[i],rate));
		
			int nystates = prev2.getNStates() + X[i].getNStates() - 1;
			if (nystates > maxystates)
				nystates = maxystates;
			Y[i] = new Variable(nystates);
			add(new Sum(Y[i],prev2,X[i]));
			prev2 = Y[i];

			L[i] = new Variable(2);
			add(new Or(L[i],X[i]));
			K[i] = new Variable(i+2);
			add(new Sum(K[i],prev3,L[i]));
			prev3 = K[i];
		}
	}

	Variable getResult()
	{
		return Y[Y.length-1];
	}
	
	Variable getResult2()
	{
		return K[K.length-1];
	}
}
