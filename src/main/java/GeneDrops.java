import jpsgcs.util.Main;
import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.pedapps.GeneDropper;
import jpsgcs.pedapps.ThreadedGeneDropper;

import java.io.PrintStream;
import java.util.Random;

/**
	This program uses the multi locus gene drop method to simulate genotypes 
	on a pedigree to match those in the input. 
<ul>
	Usage : <b> java GeneDrops input.par(ld) input.ped n prefix [-a] </b> </li>
</ul>
	where 
<ul>
<li> <b> input.par(ld) </b> is the input LINKAGE parameter file which may or may not have and LD model
        appended to it. If no LD model is explicitly specified, linkage equilibrium is assumed. </li>
<li> <b> input.ped </b> is the input LINKAGE pedigree file. </li>
<li> <b> n </b> is the number of simulations required. </li>
<li> <b> prefix </b> is the prefix in the names of the output files. For example, if 
	the prefix is "out" and n=5, the output will be in files called
	"out.1", "out.2", ... "out.5". </li>
<li> <b> -a </b> if this option is specified, simulated genotypes are output for all the
	individuals in the pedigree, not just the ones observed in the input. </li>
</ul>

<p>
	That is, if an individual's 
	genotype at a certain marker is observed -- that is not zero -- in the specified input file,
	an observation will be simulated and specified in the output file.
	The recombination fractions specified in the input parameter file are used so
	that the simulations on the ith locus are simulated properly conditional on the i-1th.
<p>
	If a linkage disequilibrium model for the founder haplotypes is specfied this 
	is used and the alleles frequencies in the input parameter file are
	ignored. An LD  model file can be estimated from data using other programs
	such as <a href="HapGraph.html"> HapGraph </a> or <a href="IntevalLD.html"> IntervalLD </a>.

*/

public class GeneDrops
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();
			LinkageFormatter parin = null;
			LinkageFormatter pedin = null;
			
			boolean allout = false;
			String prefix = null;
			int n = 0;

			String[] bargs = Main.strip(args,"-a");
			if (bargs != args)
			{
				allout = true;
				args = bargs;
			}
			
			switch(args.length)
			{
			case 4: parin = new LinkageFormatter(args[0]);
				pedin = new LinkageFormatter(args[1]);
				n = Integer.parseInt(args[2]);
				prefix = args[3];
				break;

			default: System.err.println("Usage: java GeneDrops input.par(ld) input.ped n prefix [-a]");
				System.exit(1);
			}

			GeneticDataSource x = Linkage.read(parin,pedin);

			LDModel ldmod = new LDModel(parin);
			if (ldmod.getLocusVariables() == null)
			{
                                System.err.println("Warning: parameter file has no LD model appended.");
                                System.err.println("Assuming linkage equilirbiurm and given allele frequencies.");
                                ldmod = new LDModel(x);
			}

			GeneDropper g = new GeneDropper(x,ldmod,rand);
			int places = (int) (1.00000000001 + (Math.log10(n)));

			for (int i=0; i<n; i++)
			{
				g.geneDrop(allout);
				PrintStream p = new PrintStream(prefix+"."+format(i+1,places));
				x.writePedigree(p);
				p.close();
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in GeneDrops:main()");
			e.printStackTrace();
		}
	}

	public static String format(int i, int p)
	{
		String r = ""+i;
		for (int j = (int)(1.000000000001 + Math.log10(i)); j<p; j++)
			r = "0"+r;
		return r;
	}
}
