import jpsgcs.jtree.ContinuousDataMatrix;
import jpsgcs.jtree.WSMGraphLaw;
import jpsgcs.jtree.GGIMLikelihood;
import jpsgcs.jtree.EdgePenaltyPrior;
import jpsgcs.jtree.ProductGraphLaw;
import jpsgcs.jtree.JTree;
import jpsgcs.jtree.BornnCaronPrior;
import jpsgcs.jtree.ModifiedBCPrior;
import jpsgcs.jtree.JTreeSampler;
import jpsgcs.jtree.GiudiciGreen;
import jpsgcs.jtree.UniformDecomposable;
import jpsgcs.jtree.UniformJTree;
import jpsgcs.jtree.MultiPairJTree;
import jpsgcs.jtree.MultiPairJTree1;
import jpsgcs.jtree.MultiPairJTree2;
import jpsgcs.graph.Network;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.markov.Parameter;
import jpsgcs.util.Main;
import jpsgcs.util.Monitor;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.ConcurrentModificationException;
import java.util.Random;

public class GGIMProbs
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

		// Set the default parameters.

			int n_samples = 1000000;
			int n_burn = 100000;
			int randomits = 1000;
			int sampler = 0;
			int prichoice = 0;
			double penalty = 2.2;

		// Read the command line arguments.

			switch (args.length)
			{
			case 5: penalty = Double.parseDouble(args[4]);

			case 4: sampler = Integer.parseInt(args[3]);

			case 3: prichoice = Integer.parseInt(args[2]);

			case 2: n_samples = Integer.parseInt(args[1]);

			case 1: n_burn = Integer.parseInt(args[0]);

			case 0: break;

			default:
				System.err.println("Usage: java GGIMProbs [n_burn] [n_samples] [priorchoice] [sampler] [penalty]");
				System.exit(1);
			}

		// Read in the data from standard input.

			ContinuousDataMatrix data = new ContinuousDataMatrix();

		// Create a graph of Integers for the conditional independence graph.
		// And find a junction tree of the graph.

			Integer[] vertex = new Integer[data.nColumns()];

			Network<Integer,Object> g = new Network<Integer,Object>();
			for (int i=0; i<data.nColumns(); i++)
			{
                            vertex[i] = i;
                            g.add(vertex[i]);
			}

			JTree<Integer> jt = new JTree<Integer>(g,rand);

		// Read the true graph from file.

			

		// Make a prior graph distribution, likelihood and posterior.

			WSMGraphLaw<Integer> prior = null;

			switch(prichoice)
			{
		//	case 3: prior = new NonBCPrior<Integer>(2);
		//		break;

			case 2: prior = new EdgePenaltyPrior<Integer>(penalty);
				break;
		 
			case 1: prior = new BornnCaronPrior<Integer>(0.1,0.001);
			//case 1: prior = new BornnCaronPrior<Integer>(10,0.1);
				break;

			case 0: prior = new ModifiedBCPrior<Integer>(2,4);
				break;
			}
			
			GGIMLikelihood likelihood = new GGIMLikelihood(data,1.0,0.4,rand);
			WSMGraphLaw<Integer> posterior = new ProductGraphLaw<Integer>(prior,likelihood);
			
		// Create a junction tree sampler to sample from the poserior.

		 	JTreeSampler<Integer> jts = null;

			switch(sampler)
			{
			case 2: jts = new GiudiciGreen<Integer>(jt,posterior);
				break;

			case 1: jts = new UniformDecomposable<Integer>(jt,posterior);
				break;
			
			// Only a multipoint mixes reliably.
			case 0:
			default:
				jts = new MultiPairJTree2<Integer>(jt,posterior,true);
				break;
			}

		// Run the sampler.

			jts.setTemperature(1);
			jts.randomize();

			for (int i=0; i<n_burn; i++)
			{
				if (i % randomits == 0)
					jts.randomize();

				jts.randomUpdate();
			}

			for (int i=0; i<n_samples; i++)
			{
				if (i % randomits == 0)
					jts.randomize();

				if (rand.nextDouble() < 0.5)
					jts.randomConnection();
				else
					jts.randomDisconnection();

				System.out.println(i);
			}
		}
		catch (ConcurrentModificationException cme)
		{
		}
		catch (Exception e)
		{
			System.err.println("Caught in GGIMProbs.main()");
			e.printStackTrace();
		}
	}
}
