import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;
import jpsgcs.jtree.MultinomialMarginalLikelihood;
import jpsgcs.jtree.EdgePenaltyPrior;
import jpsgcs.jtree.ProductGraphLaw;
import jpsgcs.jtree.SMGraphLaw;
import jpsgcs.jtree.WSMGraphLaw;
import jpsgcs.jtree.JTree;
import jpsgcs.jtree.JTreeSampler;
import jpsgcs.jtree.UniformDecomposable;
import jpsgcs.jtree.GiudiciGreen;
import jpsgcs.graph.Network;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.util.Monitor;
import jpsgcs.util.Main;
import jpsgcs.jtree.DiscreteDataMatrix;
import jpsgcs.markov.Parameter;
import java.util.ConcurrentModificationException;
import java.util.Random;

/**
	This program estimates a graphical model for discrete multivariate data.
<ul>
	Usage : <b> java FitGM < data [s] [-v] </b>
</ul>
	where
<ul>
	<li> <b> data </b> is the input data file. </li>
	<li> <b> s </b> is an optional parameter specifying the total number of iterations to be carried out.
	The default is 1000000. </li>
	<li> <b> -v </b> is a visualization option. If specified, a GUI showing the 
	conditional indpendence graph for the model is displayed. </li>
	
</ul>
<p>
	The input is assumed to have a line for each multivariate observation.
	Each line consists of a white space separated list of integer values.
	The multivariate observations are assumed to be independent and complete.

<p>
	The model is estimated by two rounds of Markov chain Monte Carlos searching.
	The first round of iterations is done with Metropolis sampling. The second
	is a random uphill search.
*/

public class FitGMAnneal
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			Monitor.quiet(Boolean.valueOf(System.getProperty("JPSGCS.monitor.quiet", "true")));

			boolean visualize = false;
			double penalty = 0;
			int totalits = 1000000000;
			int randomits = 100000;

			String[] bargs = Main.strip(args,"-v");
			if (bargs != args)
			{
				visualize = true;
				args = bargs;
			}
			
			switch (args.length)
			{
			case 1: totalits = Integer.parseInt(args[0]);

			case 0: break;

			default:
				System.err.println("Usage: java FitGMLD [s] [-v]");
				System.exit(1);
			}

			DiscreteDataMatrix data = new DiscreteDataMatrix();

			Variable[] vars = new Variable[data.nColumns()];
			for (int j=0; j<vars.length; j++)
				vars[j] = new Variable(data.statesOfColumn(j));


			SMGraphLaw<Variable> like = new MultinomialMarginalLikelihood(vars,data,1);
			SMGraphLaw<Variable> prior = new EdgePenaltyPrior<Variable>(penalty);
			WSMGraphLaw<Variable> posterior = new ProductGraphLaw<Variable>(like,prior);

			Network<Variable,Object> g = new Network<Variable,Object>();
			for (Variable v : vars)
				g.add(v);

                        Parameter temp = new Parameter("Temperature",0,1000,100);

			GraphFrame frame = ( visualize ? new GraphFrame<Variable,Object>(g,temp) : null );

			JTree<Variable> jt = new JTree<Variable>(g,rand);
			JTreeSampler<Variable> jts = new UniformDecomposable<Variable>(jt,posterior);
			//JTreeSampler<Variable> jts = new GiudiciGreen<Variable>(jt,posterior);
			jts.randomize();

			// Go into the simulation loop.

			for (int i=1; i<=totalits; i++)
			{
                                jts.setTemperature(temp.getValue()/100.0);

				if (i % randomits == 0)
				{
					jts.randomize();
					System.err.print(".");
				}
					
				jts.randomUpdate();
			 }

		}
		catch (ConcurrentModificationException cme)
		{
		}
		catch (Exception e)
		{
			System.err.println("Caught in FitGMLD.main()");
			e.printStackTrace();
		}
	}
}
