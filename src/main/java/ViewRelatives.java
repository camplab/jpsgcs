
import jpsgcs.util.ArgParser;
import jpsgcs.util.InputFormatter;
import jpsgcs.markov.Parameter;
import jpsgcs.graph.Network;
import jpsgcs.graph.Graph;
import jpsgcs.graph.Graphs;
import jpsgcs.viewgraph.PaintableGraph;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.viewgraph.VertexRepresentation;
import jpsgcs.viewgraph.Blob;

import java.awt.Frame;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.LinkedHashSet;

public class ViewRelatives 
{
	public static void main(String[] args)
	{
		try
		{
			ArgParser ap = new ArgParser(args);
			boolean shownames = ap.gotOpt("-names");
			boolean view = ap.gotOpt("-v");
			double threshold = ap.doubleAfter("-t",0.99);
			int maxits = 1000000000;

			Network<String,Object> g = new Network<String,Object>();
			
			List<String> x = new ArrayList<String>();
			List<String> y = new ArrayList<String>();
			List<Double> z = new ArrayList<Double>();
			Set<String> oxen = new LinkedHashSet<String>();

			InputFormatter f = new InputFormatter();

// New format
			while (f.newLine())
			{
				f.nextString();
				String xs = f.nextString();
				f.nextString();
				String ys = f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				double pihat = f.nextDouble();
				int xg = f.nextInt();
				int yg = f.nextInt();

				x.add(xs);
				y.add(ys);
				z.add(pihat);
				if (xg == 2)
					oxen.add(xs);
				if (yg == 2)
					oxen.add(ys);
			}
/*
//	Old format.
			f.newLine();
			while (f.newLine())
			{
				f.nextString();
				String xs = f.nextString();
				f.nextString();
				String ys = f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				double pihat = f.nextDouble();
				f.nextString();
				f.nextString();
				f.nextString();
				f.nextString();
				int xg = f.nextInt();
				int yg = f.nextInt();

				x.add(xs);
				y.add(ys);
				z.add(pihat);
				if (xg == 2)
					oxen.add(xs);
				if (yg == 2)
					oxen.add(ys);
			}
*/

			String[] sx = (String []) x.toArray(new String[0]);
			String[] sy = (String []) y.toArray(new String[0]);
			Double[] sz = (Double []) z.toArray(new Double[0]);

			for (int i=0; i<sx.length; i++)
			{
				g.add(sx[i]);
				g.add(sy[i]);

				if (sz[i] >= threshold)
					g.connect(sx[i],sy[i]);
			}
			
			Outputter op = new Outputter<String,Object>(g);

			if (view)
			{
				Parameter thresh = new Parameter("Threshold",0,1,threshold);
				GraphFrame<String,Object> frame = new GraphFrame<String,Object>(g,thresh);

				WindowListener[] wl = frame.getWindowListeners();
				for (WindowListener w : wl)
					frame.removeWindowListener(w);
				frame.addWindowListener(op);
				for (WindowListener w : wl)
					frame.addWindowListener(w);

				for (String v : oxen)
					frame.getRepresentation(v).setColor(Color.red);

				for (String v : g.getVertices())
				{
					if (!shownames)
					{
						Blob b = new Blob();
						b.setSize(8,8);
						frame.setRepresentation(v,b);
					}
	
					if (oxen.contains(v))
					{
						frame.getRepresentation(v).setColor(Color.red);
					}
				}

				for (int j=0; j<maxits; j++)
				{
					Thread.sleep(100);
	
					double newthresh = thresh.getValue();
					double xx = newthresh - threshold;
					if (xx < 0) 
						xx = -xx;
	
					if (Math.abs(newthresh-threshold) > 0.0000001)
					{
						threshold = newthresh;
	
						for (int i=0; i<sz.length; i++)
						{
							if (sz[i] > threshold)
							{
								if (!g.connects(sx[i],sy[i]))
									g.connect(sx[i],sy[i]);
							}
							else
							{
								if (g.connects(sx[i],sy[i]))
									g.disconnect(sx[i],sy[i]);
							}
						}
					} 
				}
			}

			op.output();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.err.println();
			System.err.println("Usage: java ViewRelatives < inputfile -t [threshold|0.99] -v -names"); 
			System.err.println("\t -t sets the linking threshold. The default is 0.99.");
			System.err.println("\t -v turns on the viewer.");
			System.err.println("\t -names turns on showing the names for the vertices.");
			System.exit(1);
		}
	}
}

class Outputter<V,E> extends WindowAdapter
{
	private Graph<V,E> g = null;

	public Outputter(Graph<V,E> graph)
	{
		g = graph;
	}

	public void output()
	{
		//Set<Set<V>> comps = Graphs.components(g);
		Set<Set<V>> comps = Graphs.getCliques(g);
//		for (Set<V> c : comps)
//			System.out.println(c.size());

		for (V v : g.getVertices())
		{
			System.out.print(v+"\t");
			for (Set<V> c : comps)
			{
				if (c.size() > 1)
				{
					if (c.contains(v))
						System.out.print("1 ");
					else
						System.out.print("0 ");
				}
			}
			System.out.println();
		}
	}

	public void windowClosing(WindowEvent e)
	{
		output();
	}
}

