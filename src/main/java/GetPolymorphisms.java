import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.LinkageInterface;

import java.io.PrintStream;
import java.util.Vector;

/**
	This program selects from the input data the marker loci for which at
	least two alleles are observed. 

<ul>
	Usage : <b> java GetPolymorphisms input.par input.ped [output.par] [output.ped] </b> </li>
</ul>
	where 
<ul>
<li> <b> input.par </b> is the input LINKAGE parameter file. </li>
<li> <b> input.ped </b> is the input LINKAGE pedigree file. </li>
<li> <b> output.par </b> is the output LINKAGE parameter file. 
	The default is to write to standard output.</li>
<li> <b> output.ped </b> is the output LINKAGE pedigree file. 
	The default is to write to standard output.</li>
</ul>

*/

public class GetPolymorphisms
{
	public static void main(String[] args)
	{
		try
		{
			boolean harddowncode = true;

			LinkageDataSet ld = null;
			PrintStream parout = System.out;
			PrintStream pedout = System.out;

			switch(args.length)
			{
			case 4: pedout = new PrintStream(args[3]);
			case 3: parout = new PrintStream(args[2]);
			case 2: ld = new LinkageDataSet(args[0],args[1]);
				break;
			default:
				System.err.println("Usage: java GetPolymorphisms input.par input.ped [output.par] [output.ped}");
				System.exit(0);
			}

			ld.downCode(harddowncode);
			ld.countAlleleFreqs();
			GeneticDataSource d = new LinkageInterface(ld);

			Vector<Integer> out = new Vector<Integer>();
			for (int i=0; i<d.nLoci(); i++)
				if (d.nAlleles(i) > 1)
                                    out.add(i);

			int[] x = new int[out.size()];
			for (int i=0; i<out.size(); i++)
				x[i] = out.get(i).intValue();
			
			ld = new LinkageDataSet(ld,x);

			ld.getParameterData().writeTo(parout);
			ld.getPedigreeData().writeTo(pedout);
		}
		catch (Exception e)
		{
			System.err.println("Caught in GetPolymorphisms:main()");
			e.printStackTrace();
		}
	}
}
