import jpsgcs.pedapps.AlleleSharing;
import jpsgcs.genio.BasicGeneticData;
import jpsgcs.genio.Genio;
import jpsgcs.util.ArgParser;


public class SGS extends AlleleSharing
{
	public static void main(String[] args)
	{
		try
		{
			ArgParser ap = new ArgParser(args);
			BasicGeneticData x = Genio.read(ap);

			boolean runs = ap.gotOpt("-r");

			int[] s = hetSharing(x);
			int np = x.nProbands();
			int[] r = runs(s,np);

			if (runs)
			{
				for (int i=0; i<r.length; )
				{
					System.out.println(r[i]);
					if (r[i] == 0)
						i += 1;
					else
						i += r[i];
				}
			}
			else
			{
				int[] t = runs(s,np-1);
				int[] u = runs(s,np-2);
				int[] v = runs(s,np-3);

				int[] p = pairedSGS(x);
				double[] q = weightedPairedSGS(x);
	
				for (int i=0; i<s.length; i++)
					System.out.println(i+"\t"+s[i]+"\t"+r[i]+"\t"+t[i]+"\t"+u[i]+"\t"+v[i]+"\t"+p[i]+"\t"+q[i]);

				System.err.println("MAX: \t"+max(r)+"\t"+max(t)+"\t"+max(u)+"\t"+max(v)+"\t"+max(p)+"\t"+max(q));
			}

		}
		catch (Exception e)
		{
			System.err.println("Caught in SGS:main().");
			e.printStackTrace();
			System.err.println();
			System.err.println("Usage: java SGS linkage.parfile linkage.pedfile");
			System.err.println("Usage: java -plink plink.mapfile plink.pefile proband.file");
		}
	}
}
