import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LocusSampler;
import jpsgcs.pedmcmc.LDLocusSampler;
import jpsgcs.pedmcmc.ExtendedLMSampler;
import jpsgcs.pedmcmc.LocusMeiosisSampler;
import jpsgcs.pedapps.ErrorCount;
import java.util.Random;

/**
	This program uses Markov chain Monte Carlo methods for fully
	informative multi locus error detection for linked loci
	in a pedigree.

<ul>
        Usage : <b> java McCheck input.par input.ped [n_samples] [n_burnin] [-v/t] </b>
</ul>
<p>
        where
<ul>
        <li> <b> input.par </b> is the input LINKAGE parameter file. </li>
        <li> <b> input.ped </b> is the input LINAKGE pedigree file. </li>
        <li> <b> n_samples </b> is an optional parameter specifiying the number of iterations
        to sample. The default is 1000. </li>
        <li> <b> n_burnin  </b> is an optional parameter specifying the number iterations to
        do before sampling begins. The default is 0. </li>
        <li> <b> -v/t </b> switches between verbose and terse output. The default is verbose. </li>
</ul>

<p>
	The output is a matrix of probailties, one row per individual, one
	column per locus, specifying the posterior probability that each genotype
	observation is in error.
	
*/

public class McCheck extends McProg
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			error = 0.01;
			maxerrall = 4;

			readOptions(args,"McCheck");

			if (error <= 0.0)
			{
				System.err.println("Must have posititive error probability.");
				System.err.println("Setting it to 1%");
				error = 0.01;
				maxerrall = 4;
			}

			for (GeneticDataSource data : alldata)
			{
				if (verbose)
					System.err.println("Setting up sampler");

				data.downcodeAlleles();
		
				LocusSampler mc = makeSampler(data,false,rand);

				mc.initialize();

				if (n_burnin > 0 && verbose)
					System.err.println("Burn in");

				for (int s=0; s<n_burnin; s++)
				{
					mc.sample();
					if (verbose)
						System.err.print(".");
				}

				System.err.println();

				if (n_burnin > 0 && verbose)
					System.err.println();

				ErrorCount err = new ErrorCount(mc.getErrors());

				if (verbose)
					System.err.println("Sampling");

				for (int s=0; s<n_samples; s++)
				{
					mc.sample();
					err.update();
					if (verbose)
						System.err.print(".");
				}

				if (verbose)
					System.err.println();

				double[][] res = err.results();
				for (int i=0; i<res.length; i++)
				{
					for (int j=0; j<res[i].length; j++)
						System.out.printf("%6.4f ",res[i][j]);
					System.out.println();
				}
				System.out.println();
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in McCheck:main()");
			e.printStackTrace();
		}
	}
}
