import jpsgcs.util.Main;
import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.pedapps.ConditionalGeneDropper;

import java.io.PrintStream;
import java.util.Random;

public class ConditionalGeneDrops
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			LinkageFormatter parin = null;
			LinkageFormatter pedin = null;
			
			boolean allout = false;
			String prefix = null;
			int n = 0;
			int where = -1;

			String[] bargs = Main.strip(args,"-a");
			if (bargs != args)
			{
				allout = true;
				args = bargs;
			}
			
			switch(args.length)
			{
			case 5: parin = new LinkageFormatter(args[0]);
				pedin = new LinkageFormatter(args[1]);
				n = Integer.parseInt(args[2]);
				prefix = args[3];
				where = Integer.parseInt(args[4]);
				break;

			default: System.err.println("Usage: java ConditinalGeneDrops input.par(ld) input.ped n prefix k [-a]");
				System.exit(1);
			}

			GeneticDataSource x = Linkage.read(parin,pedin);

			LDModel ldmod = new LDModel(parin);
			if (ldmod.getLocusVariables() == null)
			{
                                System.err.println("Warning: parameter file has no LD model appended.");
                                System.err.println("Assuming linkage equilirbiurm and given allele frequencies.");
                                ldmod = new LDModel(x);
			}

			ConditionalGeneDropper g = new ConditionalGeneDropper(x,ldmod,where,rand);
			int places = (int) (1.00000000001 + (Math.log10(n)));

			for (int i=0; i<n; i++)
			{
				g.geneDrop(allout);
				PrintStream p = new PrintStream(prefix+"."+format(i+1,places));
				x.writePedigree(p);
				p.close();
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in ConditionalGeneDrops:main()");
			e.printStackTrace();
		}
	}

	public static String format(int i, int p)
	{
		String r = ""+i;
		for (int j = (int)(1.000000000001 + Math.log10(i)); j<p; j++)
			r = "0"+r;
		return r;
	}
}
