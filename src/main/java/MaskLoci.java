import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.linkage.LinkageLocus;
import java.util.Vector;

/**
 	This program takes a subset of loci from LINKAGE parameter and pedigree
	files and sets the genotypes to unknown. 

<ul>
        Usage : <b> java MaskLoci input.par input.ped l1 [l2] ... > output.ped </b> </li>
</ul>
        where
<ul>
<li> <b> input.par </b> is the original LINKAGE parameter file </li>
<li> <b> input.ped </b> is the original LINKAGE pedigree file </li>
<li> <b> output.ped </b> is the file where the masked pedigree data will be put.</li>
<li> <b> l1 [l2] ... </b> is a list of at least one locus index. The indexes match the order of the 
	loci in the input parameter file. The first locus has index 0, the last of n has index n-1.</li>
</ul>

<p> This is useful for evaluating allele imputation methods.

*/

	
public class MaskLoci
{
	public static void main(String[] args)
	{
		try
		{
			if (args.length < 3)
				System.err.println("Usage: java MaskLoci input.par input.ped l1 [l2] ...");

			LinkageDataSet l = new LinkageDataSet(args[0],args[1]);
		
			Vector<Integer> out = new Vector<Integer>();
			for (int i=2; i<args.length; i++)
			{
				if (args[i].contains("-"))
				{
					String[] s = args[i].split("-");
					int a = Integer.parseInt(s[0]);
					int b = 0;
					if (s.length == 2)
                                            b = Integer.parseInt(s[1]);
					else
						b = l.getParameterData().nLoci()-1;

					if (b > l.getParameterData().nLoci()-1)
						b = l.getParameterData().nLoci()-1;

					for (int k=a; k<=b; k++)
                                            out.add(k);
				}
				else
                                    out.add(Integer.parseInt(args[i]));
			}

			LinkageIndividual[] y = l.getPedigreeData().getIndividuals();

			for (Integer x : out)
			{
				int i = x.intValue();
				for (int j=0; j<y.length; j++)
					y[j].getPhenotype(i).setUninformative();
			}

			l.getPedigreeData().writeTo(System.out);
		}
		catch (Exception e)
		{
			System.err.println("Caught in MaskLoci.main()");
			e.printStackTrace();
		}
	}
}
