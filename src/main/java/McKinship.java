import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LocusSampler;
import jpsgcs.pedmcmc.LDLocusSampler;
import jpsgcs.pedmcmc.LocusMeiosisSampler;
import jpsgcs.pedmcmc.ExtendedLMSampler;
import jpsgcs.pedapps.Kinship;

import java.util.Random;

/**
	This program samples the posterior distribution of inheritance vectors
	given the observed genotypes and, hence, estimates postrior probabilities
	of kinship. 

<ul>
        Usage : <b> java McKinship input.par input.ped [n_samples] [n_burnin] [error] [-v/t] </b>
</ul>
<p>
        where
<ul>
        <li> <b> input.par </b> is the input LINKAGE parameter file. </li>
        <li> <b> input.ped </b> is the input LINAKGE pedigree file. </li>
        <li> <b> n_samples </b> is an optional parameter specifiying the number of iterations
        to sample. The default is 1000. </li>
        <li> <b> n_burnin  </b> is an optional parameter specifying the number iterations to
        do before sampling begins. The default is 0. </li>
        <li> <b> error </b> is an optional parameter specifying the probability of a genotyping
        error. The default is 0.01. </li>
        <li> <b> -v/t </b> switches between verbose and terse output. The default is verbose. </li>
</ul>

<p>
	Sampling is done using the Markov chain Monte Carlo method of 
	blocked Gibbs updating.
<p>
	The output gives a matrix of posterior kinship coefficients for each pedigree
	in the input file.
*/

public class McKinship extends McProg
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			error = 0.01;
			maxerrall = 4;

			readOptions(args,"McKinship");

			for (GeneticDataSource data : alldata)
			{
				if (verbose)
					System.err.println("Setting up sampler");

				data.downcodeAlleles();

				LocusSampler mc = makeSampler(data,true,rand);

				mc.initialize();

				if (n_burnin > 0 && verbose)
					System.err.println("Burn in");

				for (int s=0; s<n_burnin; s++)
				{
					mc.sample();
					if (verbose) 
						System.err.print(".");
				}

				if (n_burnin > 0 && verbose)
					System.err.println();

				Kinship kin = new Kinship(data,mc.getInheritances(),rand);

				if (verbose)
					System.err.println("Sampling");

				for (int s=0; s<n_samples; s++)
				{
					mc.sample();
					kin.update();
					if (verbose) 
						System.err.print(".");
				}

				if (verbose) 
					System.err.println();

				for (int j=0; j<kin.nPositions(); j++)
				{
					double[][] k = kin.kinships(j);
					for (int a=0; a<k.length; a++)
					{
						System.out.print(data.individualName(a)+" :\t");
						for (int b=0; b<k[a].length; b++)
							System.out.print(k[a][b]+"\t");
						System.out.println();
					}
					System.out.println();
				}
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in McKinship:main()");
			e.printStackTrace();
		}
	}
}
