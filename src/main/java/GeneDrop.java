import jpsgcs.pedapps.GeneDropper;
import jpsgcs.pedapps.ThreadedGeneDropper;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.Linkage;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.util.Main;

import java.io.PrintStream;
import java.util.Random;

/**
	This program uses the multi locus gene drop method to simulate genotypes 
	on a pedigree to match those in the input. 

<ul>
	Usage : <b> java GeneDrop input.par(ld) input.ped [ldmodelfile] [-a] > output.ped </b> </li>
</ul>
	where 
<ul>
<li> <b> input.par(ld) </b> is the input LINKAGE parameter file which may or may not have and LD model 
	appended to it. If no LD model is explicitly specified, linkage equilibrium is assumed.  </li>
<li> <b> input.ped </b> is the input LINKAGE pedigree file. </li>
<li> <b> -a </b> if this option is specified, simulated genotypes are output for all the
	individuals in the pedigree, not just the ones observed in the input.
<li> <b> output.ped </b> is the simulated output LINKAGE pedigree file. 
</ul>

<p>
	If an individual's 
	genotype at a certain marker is observed -- that is not zero -- in the specified input file,
	an observation will be simulated and specified in the output file.
	The recombination fractions specified in the input parameter file are used so
	that the simulations on the ith locus are simulated properly conditional on the i-1th.
<p>
	If a linkage disequilibrium model for the founder haplotypes is specfied this 
	is used and the alleles frequencies in the input parameter file are
	ignored. An LD  model file can be estimated from data using
	<a href="FitGMLD.html"> FitGMLD </a>.
*/

public class GeneDrop 
{
	public static void main(String[] args)
	{
		try
		{
			// Temporary change for testing.
			Random rand = new Random();
			//Random rand = new Random(1);

			LinkageFormatter parin = null;
			LinkageFormatter pedin = null;

			boolean allout = false;

			String[] bargs = Main.strip(args,"-a");
			if (bargs != args)
			{
				allout = true;
				args = bargs;
			}

			switch(args.length)
			{
			case 2: parin = new LinkageFormatter(args[0]);
				pedin = new LinkageFormatter(args[1]);
				break;

			default: System.err.println("Usage: java GeneDrop input.par(ld) input.ped [-a]");
				System.exit(1);
			}
			
			GeneticDataSource data = Linkage.read(parin,pedin);
			LDModel ldmod = new LDModel(parin);

			if (ldmod.getLocusVariables() == null)
			{
				System.err.println("Warning: parameter file has no LD model appended.");
				System.err.println("Assuming linkage equilibriurm and given allele frequencies.");
				ldmod = new LDModel(data);
			}

			GeneDropper g = new GeneDropper(data,ldmod,rand);
			g.geneDrop(allout);
			data.writePedigree(System.out);
		}
		catch (Exception e)
		{
			System.err.println("Caught in GeneDrop:main()");
			e.printStackTrace();
		}
	}
}
