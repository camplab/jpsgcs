import jpsgcs.linkage.Linkage;
import jpsgcs.genio.BasicGeneticData;
import jpsgcs.genio.Genio;

/** 
	Takes data from LINKAGE format parameter and pedigree files and
	prepares it for input into the FASTPHASE haplotyping program.

<ul>
	Usage : <b> java LinkageToFastPhase input.par input.ped </b> </li>
</ul>
	where 
<ul>
<li> <b> input.par </b> is the input LINKAGE parameter file. </li>
<li> <b> input.ped </b> is the input LINKAGE pedigree file. </li>
</ul>

*/

public class LinkageToFastPhase
{
	public static void main(String[] args)
	{
		try
		{
			switch(args.length)
			{
			case 2:
				break;
			default:
				System.err.println("Usage: java LinkageToFastPhase input.par input.ped");
				System.exit(0);
			}

			BasicGeneticData x = Linkage.read(args[0],args[1]);
			Genio.writeAsFastPhase(x,System.out);
		}
		catch (Exception e)
		{
			System.err.println("Caught in LinkageToFastPhase:main()");
			e.printStackTrace();
		}
	}
}
