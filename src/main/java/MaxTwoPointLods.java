import jpsgcs.pedmcmc.LocusProduct;
import jpsgcs.pedmcmc.ErrorLocusProduct;
import jpsgcs.pedapps.TwoLocusLodScores;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.util.Curve;
import jpsgcs.util.Point;
import jpsgcs.util.GoldenSection;

/**
	This program finds the maximum lod score for values of the 
	recombination between 0 and 0.5 and between 0.5 and 1.
<ul>
        Usage : <b> java MaxTwoPointLods input.par input.ped [error]</b> </li>
</ul>
        where
<ul>
<li> <b> input.par </b> is a LINKAGE parameter file </li>
<li> <b> input.ped </b> is a LINKAGE pedigree file </li>
<li> <b> error  </b> is an optional parameter specifying the genotyping error
probability. The default is 0. </li>

</ul>
<p>
	The method used is a golden section search that takes about 20
	function evaluations to get to within 0.001 of the maximizing value.
	The methods also assumes that the lod score is convex over
	(0,0.5) and over (0.5,1). This is not always true.
<p>
	The program maximizes the lod scores separately over the two intervals
	(0,0.5) and (0.5,1), because there may be a local maximum in each
	of these intervals. The output is a bare table of 4 columns
	giving the maximizing value and maximum in the region (0,0.5)
	followed by the maximizing value and maximum in the region (0.5,1).
	Values of the recombination parameter in the region (0.5,1) have
	no genetic interpretation, but may occur if the phase information
	is low and/or the model is misspecified.
	If you don't think you want values in the (0.5,1) region, just
	ignore the last two columns.
*/

public class MaxTwoPointLods 
{
	public static void main(String[] args)
	{
		try
		{
			GeneticDataSource[] x = null;
			double prec = 0.001;
			double error = 0;
			int maxerrall = 0;

			switch(args.length)
			{
			case 3: error = Double.parseDouble(args[2]);
				maxerrall = 4;

			case 2: x = Linkage.readAndSplit(args[0],args[1]);
				break;
			default: 
				System.err.println("Usage: java TwoPointLods input.par input.ped [error]");
				System.exit(1);
			}

			for (GeneticDataSource data : x)
			{
				data.downcodeAlleles();
				LocusProduct trait = new LocusProduct(data,0);
			
				for (int i=1; i<data.nLoci(); i++)
				{
					LocusProduct mark = null;
					if (error > 0 && data.nAlleles(i) <= maxerrall)
						mark = new ErrorLocusProduct(data,i,error);
					else
						mark = new LocusProduct(data,i);

					TwoLocusLodScores link = new TwoLocusLodScores(trait,mark,data.sexLinked());
					LodScore l = new LodScore(link);

					Point xy = GoldenSection.maximum(l,0,0.5,prec);
					System.out.printf("%6.4f %6.4f\t\t",xy.x,xy.y);

					xy = GoldenSection.maximum(l,0.5,1.0,prec);
					System.out.printf("%6.4f %6.4f\t\t",xy.x,xy.y);

                                        System.out.println();
				}

				System.out.println();
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in MaxTwoPointLods:main()");
			e.printStackTrace();
		}
	}
}

class LodScore implements Curve
{
	public LodScore(TwoLocusLodScores l)
	{
		link = l;
	}

	public double f(double x)
	{
		return link.evaluate(x);
	}

	private TwoLocusLodScores link = null;
}
