
import jpsgcs.util.ArgParser;
import jpsgcs.genio.Genio;
import jpsgcs.genio.BasicGeneticData;

/** 
	Translates between genetic data formats. 
	These data formats are not equivalent so information is lost in translation. 
	A fair effort is made to fill in as much data as possible.
	The order of the individuals in the genotype data files is not necessarily maintained.

	The input format is specified with a -[format] option, the output format is 
	specified by -out[format]. The number of input and output
	files depends on the format.

	Linkage 	-link		2 files: Parameter and pedigree data.
	Premake Linkage -premake	2 files: Parameter and premake pedigree data.
	Phase		-phase		1 file: Phase format.
	Fast Phase	-fastphase	1 file: Fast phase Format.
	Plink		-plink		3 files: Map file, pedigree & genotype file, proband file (list of pedigree name, individual name pairs, one per line).

	The following tranlsations are supported:

	Linkage to Linkage, Premake, Phase, Fastphase.
	Premake to Linkage, Premake, Phase, Fastphase.
	Plink to Linkage, Premake, Plink, Phase, FastPhase.

	(Can not input Phase or FastPhase).

	For example, to translate from Premake Linkage files to Plink use:

	% java GenIO input.par input.ped out.map out.ped out.proband -premake -outplink

*/
		
public class GenIO
{
	public static void main(String[] args)
	{
		try
		{
			ArgParser a = new ArgParser(args);
			BasicGeneticData data = Genio.read(a);
			Genio.write(data,a);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
