import jpsgcs.apps.zeroloop.ZeroLoopPedGenerator;
import jpsgcs.apps.zeroloop.Individual;
import jpsgcs.util.Main;
import java.util.Random;

/**
	This program generates random zero loop pedigrees with specified numbers of 
	marriages and individuals.

<ul>
	Usage: <b> java ZeroLoopPed nmar nind [nsim] [-m/p] [-l] </b>
</ul>
	where
<ul>
	<li> <b> nmar </b> is the required number of marriages or matings. </li>
	<li> <b> nind </b> is the required number of individuals. This must be at least
				2 more than twice the number of matings. </li>
	<li> <b> nsims </b> is the number of pedigrees to simulate. The pedigrees will be 
				separated by a blank line in the output file. The default is 1. </li>
	<li> <b> -m </b> is an optional paramter. If m is specified individals will be monogamous. 
				This is false by default.</li>
	<li> <b> -l </b> is an optional paramter. If -l is specified the individuals will be relabelled
			in sequential order in the output file. The default is not to relabel.  </li>
</ul>
*/
public class ZeroLoopPed
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			int nmar = 10;
			int nind = 50;
			int sims = 1;
			boolean mono = false;
			boolean relabel = false;

			String[] bargs = Main.strip(args,"-l");
			if (bargs != args)
			{
				relabel = true;
				args = bargs;
			}

			bargs = Main.strip(args,"-m");
			if (bargs != args)
			{
				mono = true;
				args = bargs;
			}

			switch (args.length)
			{
			case 3: sims = Integer.parseInt(args[2]);

			case 2: nind = Integer.parseInt(args[1]);
                            nmar = Integer.parseInt(args[0]);
				break;

			default:
				System.err.println("Usage: ZeroLoopPed nmar nind [nsims] [-m] [-l]");
				System.exit(1);
			}

			if (nind < 2*nmar+1)
			{
				System.err.println("Number of individuals must be > 1 + 2 * number of marriages");
				System.exit(1);
			}

			ZeroLoopPedGenerator z = new ZeroLoopPedGenerator(nmar,nind,mono,relabel,rand);
			
			for (int i=0; i<sims; i++)
			{
				for (Individual j : z.next())
					System.out.println(j);
				if (i < sims-1)
					System.out.println();
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in ZeroLoopPed");
			e.printStackTrace();
		}
	}
}
