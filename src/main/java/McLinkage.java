import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LocusSampler;
import jpsgcs.pedmcmc.LDLocusSampler;
import jpsgcs.pedmcmc.LocusMeiosisSampler;
import jpsgcs.pedmcmc.ExtendedLMSampler;
import jpsgcs.pedapps.MultiPoints;
import jpsgcs.pedapps.TLods;
import jpsgcs.pedapps.GeneticMap;
import jpsgcs.util.Monitor;
import java.util.Random;

/**
	The programs combines 
	<a href="McMultiPoints.html"> McMultiPoints </a>
	and
	<a href="McTLods.html"> McTLods </a>
	to give both true multi point lod scores and Tlod statistics.

<ul>
        Usage : <b> java McLinkage input.par input.ped [n_samples] [n_burnin] [error] [-v/t] </b>
</ul>
<p>
        where
<ul>
        <li> <b> input.par </b> is the input LINKAGE parameter file. </li>
        <li> <b> input.ped </b> is the input LINAKGE pedigree file. </li>
        <li> <b> n_samples </b> is an optional parameter specifiying the number of iterations
        to sample. The default is 1000. </li>
        <li> <b> n_burnin  </b> is an optional parameter specifying the number iterations to
        do before sampling begins. The default is 0. </li>
        <li> <b> error </b> is an optional parameter specifying the probability of a genotyping
        error. The default is 0. </li>
        <li> <b> -v/t </b> switches between verbose and terse output. The default is verbose. </li>
</ul>
*/

public class McLinkage extends McProg
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			boolean dotlods = true;
			boolean domulti = true;
			double[] thetas = {0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1};
			double[] pos = null;

			MultiPoints mp = null;
			TLods tl = null;

			readOptions(args,"McLinkage");

			Monitor.quiet(!verbose);
			LocusSampler.verbo = veryverbose;

			pos = GeneticMap.locusCentiMorgans(alldata[0],false,5);

			for (GeneticDataSource data : alldata)
			{

				Monitor.show("Setting up sampler");

				data.downcodeAlleles();

				Monitor.show("Alleles downcoded");

				LocusSampler mc = makeSampler(data,false,rand);

				Monitor.show("Sampler made");

				mc.initialize();

				Monitor.show("Sampler initialized");

				if (n_burnin > 0) Monitor.show("Starting burn in");
			
				for (int s=0; s<n_burnin; s++)
				{
					mc.sample(veryverbose);
					if (verbose) 
						System.err.print(".");
					if (veryverbose)
						Monitor.show("Burn "+s);
				}
				if (verbose) System.err.println();


				if (n_burnin > 0) Monitor.show("Burn in done");
			
				if (domulti)
					mp = new MultiPoints(data,mc.getInheritances(),pos);

				if (dotlods)
					tl = new TLods(data,mc.getInheritances(),thetas);

				Monitor.show("Sampling");

				for (int s=0; s<n_samples; s++)
				{
					mc.sample(veryverbose);
					
					if (domulti)
						mp.update();
					if (dotlods)
						tl.update();
					if (verbose) 
						System.err.print(".");
					if (veryverbose)
						Monitor.show("Sample "+s);
				}

				if (verbose) 
					System.err.println();

				if (domulti)
				{
					double[] lods = mp.results();

					for (int i=0; i<lods.length; i++)
						System.out.printf(" %8.4f\t%5.4f\n",pos[i],lods[i]);
					System.out.println();
				}

				if (dotlods)
				{
					double[][] tlods = tl.results();
				
					for (int i=1; i<tlods.length; i++)
					{
						for (int j=0; j<tlods[i].length; j++)
							System.out.printf(" %8.4f",tlods[i][j]);
						System.out.println();
					}
				}

				System.out.println();
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in McLinkage:main()");
			e.printStackTrace();
		}
	}
}
