

import jpsgcs.jtree.SMGraphLaw;
import java.util.Set;

public class EdgePrior<V> extends SMGraphLaw<V>
{
	public EdgePrior(double a)
	{
		alpha = a;
	}

	
	public double logPotential(Set<V> c)
	{
		return -alpha * c.size()*(c.size()-1) /2.0;
	}

	private double alpha = 1;
}
