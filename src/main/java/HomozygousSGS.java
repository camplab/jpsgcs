import jpsgcs.pedapps.AlleleSharing;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.Linkage;

/**
	This program calculates the runs of homozygous sharing for a set of
	individuals in a pedigree. 

<ul>
	Usage : <b> java HozygousSGS input.par input.ped [-r] </b> </li>
</ul>
	where 
<ul>
<li> <b> input.par </b> is the input LINKAGE parameter file. </li>
<li> <b> input.ped </b> is the input LINKAGE pedigree file. </li>
<li> <b> -r </b> if this optional parameter is specified the lengths of
        the runs where S_i = n are output, as required to estimate the
        distribution of run lengths. </li>
</ul>

<p>
	The individuals to consider are specified
	by having a 1 in the proband field of the input LINKAGE pedigree file.
	The <a href="MakeProbands.html"> MakeProbands </a> program can be
	used to set this field.

<p>
	The output is a table with 1 line for each locus in the input LINKAGE
	parameter file. On each line appear

	<ul>
		<li> The name of the locus </li>
		<li> The largest number of individuals at that locus that can share both alleles.
			We denote this by S_i. </li>
		<li> The longest run containing the locus for which S_i = n, where n is the number
		of probands. </li>
		<li> The longest run containing the locus for which S_i >= n-1. </li>
		<li> The longest run containing the locus for which S_i >= n-2. </li>
		<li> The longest run containing the locus for which S_i >= n-3. </li>
	</ul>
<p>
	Note that this format choice means that a run of length r will appear r times in
	the output: once for each locus in the run. This should be taken into account
	in any estimates of the run length distribution.
        Alternatively use the <b> -r </b> option below to list each run only once.


<p>
	The largest runs for which S_i = n, S_i >= n-1, etc seen across the whole data set
	is written to the screen.
*/

public class HomozygousSGS extends AlleleSharing
{
	public static void main(String[] args)
	{
		try
		{
			GeneticDataSource x = null;
			boolean runs = false;

			switch(args.length)
			{
			case 3: if (args[2].equals("-r"))
				runs = true;
			case 2: x = Linkage.read(args[0],args[1]);
				break;
			default:
				System.err.println("Usage: java HSGS input.par input.ped");
				System.exit(1);
			}

			int[] s = homSharing(x);
			int np = x.nProbands();
			int[] r = runs(s,np);

			if (runs)
			{
				for (int i=0; i<r.length; )
				{
					System.out.println(r[i]);
					if (r[i] == 0)
						i += 1;
					else
						i += r[i];
				}
			}
			else
			{
				int[] t = runs(s,np-1);
				int[] u = runs(s,np-2);
				int[] v = runs(s,np-3);
	
				int[] p = pairedHomSGS(x);
				double[] w = weightedPairedHomSGS(x);

				for (int i=0; i<s.length; i++)
					System.out.println(x.locusName(i)+"\t"+s[i]+"\t"+r[i]+"\t"+t[i]+"\t"+u[i]+"\t"+v[i]+"\t"+p[i]+"\t"+w[i]);

				System.err.println("\t"+max(r)+"\t"+max(t)+"\t"+max(u)+"\t"+max(v)+"\t"+max(p)+"\t"+max(w));
			}

		}
		catch (Exception e)
		{
			System.err.println("Caught in HozygousSGS:main().");
			e.printStackTrace();
		}
	}
}
