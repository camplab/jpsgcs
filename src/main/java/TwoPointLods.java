import jpsgcs.pedmcmc.LocusProduct;
import jpsgcs.pedmcmc.ErrorLocusProduct;
import jpsgcs.pedapps.TwoLocusLodScores;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;

/**
        This program calculates two point lod scores on a grid of points.
<ul>
        Usage : <b> java TwoPointLods input.par input.ped [error] </b> </li>
</ul>
        where
<ul>
<li> <b> input.par </b> is a LINKAGE parameter file </li>
<li> <b> input.ped </b> is a LINKAGE pedigree file </li>
<li> <b> error  </b> is an optional parameter specifying the genotyping error
probability. The default is 0. </li>

</ul>
        The results are written to the standard output file as
        a bare table. There is a line of output for each marker
        containing the lod score for recombination fractions of
        <ul><b> 0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1.0 </b> </ul>
        respectively.
*/

public class TwoPointLods 
{
	public static void main(String[] args)
	{
		try
		{
			double[] thetas = {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1.0};
			GeneticDataSource[] x = null;
			double error = 0;
			int maxerral = 0;

			switch(args.length)
			{
			case 3: error = Double.parseDouble(args[2]);
				maxerral = 4;

			case 2: x = Linkage.readAndSplit(args[0],args[1]);
				break;
			default: 
				System.err.println("Usage: java TwoPointLods input.par input.ped [error]");
				System.exit(1);
			}

			for (GeneticDataSource data : x)
			{
				data.downcodeAlleles();

				LocusProduct trait = new LocusProduct(data,0);

				for (int i=1; i<data.nLoci(); i++)
				{
					LocusProduct mark = null;

					if (error > 0 && data.nAlleles(i) <= maxerral)
						mark = new ErrorLocusProduct(data,i,error);
					else
						mark = new LocusProduct(data,i);

					TwoLocusLodScores link = new TwoLocusLodScores(trait,mark,data.sexLinked());
					
					for (int k=0; k<thetas.length; k++)
						System.out.printf("%8.4f ",link.evaluate(thetas[k]));

					System.out.println();
				}
				System.out.println();
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in TwoPointLods:main()");
			e.printStackTrace();
		}
	}
}
