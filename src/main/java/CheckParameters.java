import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkageFormatter;

/**
        This program reads in a LINKAGE parameter, checks that it is correctly
	foratted and then outputs the checked file.
<ul>
        Usage : <b> java CheckParameters < input.par > output.par </b> </li>
</ul>
        where
<ul>
<li> <b> input.par </b> is the input LINKAGE parameter file. </li>
<li> <b> output.par </b> is the checked output LINKAGE parameter file.
</ul>

<p>
        Error and warning messages are printed to the screen. The checks made by this program are
        made whenever data is input.

<p>
	This program is effectively the first part of the <a href="CheckFormat.html"> CheckFormat </a>
	program that checks a combination of LINKAGE parameter and pedigree files.

<p>
        See my <a href="linkage.html"> LINKAGE format page </a> for information
        about the way this suite of programs uses this format.

*/

public class CheckParameters
{
	public static void main(String[] args)
	{
		try
		{
			LinkageParameterData p = new LinkageParameterData(new LinkageFormatter());
			p.writeTo(System.out);
		}
		catch (Exception e)
		{
			System.err.println("Caught in CheckParameters:main().");
			System.err.println("Likely to be an unexpected type of data formatting error.");
			e.printStackTrace();
		}
	}
}
