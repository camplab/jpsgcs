import jpsgcs.pedapps.ConditionalGeneDropper;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.Linkage;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.util.Main;

import java.io.PrintStream;
import java.util.Random;

public class ConditionalGeneDrop 
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			LinkageFormatter parin = null;
			LinkageFormatter pedin = null;

			int where = -1;
			boolean allout = false;

			String[] bargs = Main.strip(args,"-a");
			if (bargs != args)
			{
				allout = true;
				args = bargs;
			}

			switch(args.length)
			{
			case 3: where = Integer.parseInt(args[2]);
				parin = new LinkageFormatter(args[0]);
				pedin = new LinkageFormatter(args[1]);
				break;

			default: System.err.println("Usage: java ConditionalGeneDrop input.par(ld) input.ped k [-a]");
				System.exit(1);
			}
			
			GeneticDataSource data = Linkage.read(parin,pedin);
			LDModel ldmod = new LDModel(parin);

			ConditionalGeneDropper g = null;

			if (ldmod.getLocusVariables() == null)
				g = new ConditionalGeneDropper(data,where,rand);
			else
				g = new ConditionalGeneDropper(data,ldmod,where,rand);

			g.geneDrop(allout);
			data.writePedigree(System.out);
		}
		catch (Exception e)
		{
			System.err.println("Caught in ConditionalGeneDrop:main()");
			e.printStackTrace();
		}
	}
}
