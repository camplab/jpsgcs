import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.Linkage;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.pedapps.GeneDropper;
import jpsgcs.pedapps.AlleleSharing;
import java.util.Random;

public class SimSGSRegions extends AlleleSharing
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

                        LinkageFormatter parin = null;
                        LinkageFormatter pedin = null;

                        int n_sims = 1000;

                        switch(args.length)
                        {
                        case 3: n_sims = Integer.parseInt(args[2]);

                        case 2: parin = new LinkageFormatter(args[0]);
                                pedin = new LinkageFormatter(args[1]);
                                break;

                        default:
                                System.err.println("Usage: java SimSGSRegions input.par(ld) input.ped [n_sims]");
                                System.exit(1);
                        }

                        GeneticDataSource x = Linkage.read(parin,pedin);
                        LDModel ldmod = new LDModel(parin);
                        if (ldmod.getLocusVariables() == null)
                        {
                                System.err.println("Warning: parameter file has no LD model appended.");
                                System.err.println("Assuming linkage equilirbiurm and given allele frequencies.");
                                ldmod = new LDModel(x);
                        }

			GeneDropper g = new GeneDropper(x,ldmod,rand);

			int np = x.nProbands();

			int[] count = new int[x.nLoci()];
			for (int i=0; i<n_sims; i++)
			{
				g.geneDrop();
				int[] s = hetSharing(x);
				s = runs(s,np);
				int mx = max(s);
				for (int j=0; j<count.length; j++)
					if (s[j] >= mx)
						count[j]++;
			}

			for (int j=0; j<count.length; j++)
				System.out.println(count[j]);
		}
		catch (Exception e)
		{
			System.err.println("Caught in SimSGSRegions:main().");
			e.printStackTrace();
		}
	}
}
