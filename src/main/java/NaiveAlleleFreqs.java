import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.util.Main;

/**
	This program estimates allele frequencies from raw allele counts. 
	These will be the maximum likelihood estimates if the individuals are
	unrelated. They will be unbiased even if the individuals are related.
	For MLEs from pedigree data see GeneCountAlleles.

<ul>
        Usage : <b> java NaiveAlleleFreqs input.par input.ped [-a] </b> </li>
</ul>
        where
<ul>
<li> <b> input.par </b> is the original LINKAGE parameter file. </li>
<li> <b> input.ped </b> is the original LINKAGE pedigree file. </li>
<li> if <b> -a </b> is specified the frequencies for all loci are estimated (see below).</li>
</ul>

<p>
	The output is a linkage parameter file that is 
	the same as the old one, except that the original allele
	frequencies will be replaced by the new allele frequencies.
        This is writen to standard output.

<p>
	In a typical use of this program the first marker in the input files is often
	a phenotype, not a marker, hence, because pedigrees are usually
	ascertained in ways that bias the estimation of alleles at trait loci 
	the default is not to estimate these frequencies but only those 
	for subsequent loci. 
	To force estimation at all loci, specify the <b> -a </b> option on the command line.
*/

public class NaiveAlleleFreqs
{
	public static void main(String[] args)
	{
		try
		{
			LinkageDataSet x = null;

			boolean all = false;
			String[] bargs = Main.strip(args,"-a");
			if (bargs != args)
			{
				all = true;
				args = bargs;
			}
			
			switch(args.length)
			{
			case 2: x = new LinkageDataSet(args[0],args[1]);
				break;

			default:
				System.err.println("Usage: java GeneCountAlleles input.par input.ped [-a]");
				System.exit(1);
			}

			x.countAlleleFreqs();

/*
			if (all)
				x.countAlleleFreqs(0);

			for (int i=1; i<x.getParameterData().nLoci(); i++)
				x.countAlleleFreqs(i);
*/

			x.getParameterData().writeTo(System.out);
		}
		catch (Exception e)
		{
			System.err.println("Caught in EstimateAlleleFreqs:main().");
			e.printStackTrace();
		}
	}
}
