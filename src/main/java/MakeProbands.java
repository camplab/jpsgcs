import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkagePhenotype;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.util.InputFormatter;

import java.util.LinkedHashSet;

/**
	This programs marks inividuals in a pedigree file as being or not being
	probands. 

<ul>
	<li> Usage : <b> java MakeProbands input.par input.ped mincount </b> </li>
	<li> Usage : <b> java MakeProbands input.ped probandfile </b> </li>
</ul>
	where 
<ul>
<li> <b> input.par </b> is the input LINKAGE parameter file. </li>
<li> <b> input.ped </b> is the input LINKAGE pedigree file. </li>
<li> <b> mincount </b> this is an optional integer parameter specifying the minimum number
	of called genotypes that an idividuals must have to be made a proband. </li>
<li> <b> probandfile </b> if the argument after the specified LINKAGE files cannot be 
	read as an integer, it is assumed to be the name of a file specifying the
	probands. The file must contain one line for each proband and each line
	must have the kindred id number followed by the individual number.
</ul>

<p> This program is designed to make 
	<a ref="SGS.html"> SGS </a>,
	<a ref="SimSGS.html"> SimSGS </a>,
	<a ref="HGS.html"> HGS </a> and 
	<a ref="SimHGS.html"> SimHGS </a>
	easier to use.

*/

public class MakeProbands
{
	public static void main(String[] args)
	{
		try
		{
			int mincount = 10;
			InputFormatter f = null;
			
			LinkageParameterData par = null;
			LinkagePedigreeData ped = null;

			switch(args.length)
			{
				case 3: mincount = Integer.parseInt(args[2]);
					par = new LinkageParameterData(args[0]);
					ped = new LinkagePedigreeData(args[1],par);
					break;

				case 2: ped = new LinkagePedigreeData(args[0],null);
					f = new InputFormatter(args[1]);
					break;

				default:
					System.err.println("Usage: java MakeProbands input.par input.ped nmarkers");
					System.err.println("Usage: java MakeProbands input.ped probandfile");
					System.exit(1);
			}

			LinkageIndividual[] ind = ped.getIndividuals();
			LinkedHashSet<String> names = new LinkedHashSet<String>();

			if (f == null)
			{
				for (int i=0; i<ind.length; i++)
				{
					ind[i].proband = 0;
					int count = 0;
					LinkagePhenotype[]  phen = new LinkagePhenotype[par.nLoci()];
					if (phen != null)
						for (int j=0; j<phen.length && count < mincount; j++)
							if (ind[i].getPhenotype(j).informative())
								count++;
	
					if (count >= mincount)
					{
						ind[i].proband = 1;
						names.add(ind[i].pedid+" "+ind[i].id);
					}
				}
			}
			else
			{
				LinkedHashSet<String> prob = new LinkedHashSet<String>();
				while (f.newLine())
				{
					String kind = f.nextString();
					String name = f.nextString();
					prob.add(kind+" "+name);
				}

				for (int i=0; i<ind.length; i++)
				{
					ind[i].proband = 0;
					if (prob.contains(ind[i].pedid+" "+ind[i].id))
					{
						ind[i].proband = 1;
						names.add(ind[i].pedid+" "+ind[i].id);
					}
				}
			}

			for (String s : names)
				System.err.println(s+"\t");

			ped.writeTo(System.out);
		}
		catch (Exception e)
		{
			System.err.println("Caught in MakeProbands:main()");
			e.printStackTrace();
		}
	}
}
