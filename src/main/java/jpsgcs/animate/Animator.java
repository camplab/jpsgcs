package jpsgcs.animate;

import jpsgcs.util.SafeRunnable;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.BorderLayout;
import java.util.Collection;

public class Animator implements Paintable, SafeRunnable
{
    private ActiveCanvas canv = null;
    private Loop loop = null; 
    private Collection<Paintable> stuff = null;

    public Animator(Collection<Paintable> p)
    {
        stuff = p;

        loop = new Loop(this);
        canv = new ActiveCanvas(this);
        canv.setAxes(false);
        canv.setSize(1000,1000);
        canv.setBackground(Color.white);

        Panel pan = new Panel();
        pan.setLayout(new BorderLayout());
        pan.add(canv,BorderLayout.CENTER);

        Frame f = new Frame();
        f.addWindowListener(new FrameQuitter());
        f.add(pan);
        f.pack();
        f.setVisible(true);
        loop.start();
    }

    public void loop()
    {
        try
            {
                canv.repaint();
                Thread.sleep(40);
            }
        catch (Exception e)
            {
                e.printStackTrace();
            } 
    }

    public void paint(Graphics g)
    {
        for (Paintable p : stuff)
            p.paint(g);
    }

    public Loop getLoop()
    {
        return loop;
    }
}
