package jpsgcs.animate;

import jpsgcs.markov.Parameter;
import java.awt.Panel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Canvas;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

public class PaintablePanel<V,E> extends Panel
{
    public PaintablePanel(Paintable p)
        {
            can = new ActiveCanvas(p);
            setLayout(new BorderLayout());
            add(can,BorderLayout.CENTER);
        }

    public PaintablePanel(Paintable p, Parameter[] par)
        {
            can = new ActiveCanvas(p);
            setLayout(new BorderLayout());
            add(can,BorderLayout.CENTER);
            makeScrollPanel(par);
        }

    public void makeScrollPanel(Parameter[] par)
    {
        Panel p = new Panel();

        p.setLayout(new GridLayout(par.length,1));
                
        w = new ParameterScrollWidget[par.length];

        for (int i=0; i<par.length; i++)
            {
                w[i] = new ParameterScrollWidget(par[i]);
                p.add(w[i].getPanel());
            }

        add(p,BorderLayout.SOUTH);
    }

    public ParameterScrollWidget[] getParameterScrolls()
    {
        return w;
    }

    public ActiveCanvas getCanvas()
    {
        return can;
    }

    private ParameterScrollWidget[] w = null;
    private ActiveCanvas can = null;
}
