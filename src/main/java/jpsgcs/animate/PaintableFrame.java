package jpsgcs.animate;

import jpsgcs.markov.Parameter;

import java.awt.Frame;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.MenuItem;
import java.awt.Menu;
import java.awt.MenuBar;

public class PaintableFrame extends Frame
{
    public PaintableFrame(Paintable p, Parameter[] par)
    {
        this(p,par,100,100);
    }

    public PaintableFrame(Paintable p, Parameter[] par, int w, int h)
    {
        pan = new PaintablePanel(p,par);

        getCanvas().getTransform().setToIdentity();
        getCanvas().setSize(w,h);
        getCanvas().setAxes(false);
        getCanvas().setBackground(new Color(255,255,200));

        add(pan);

        MenuBar bar = new MenuBar();
        bar.add(new PlotMenu(this,pan.getCanvas()));
        setMenuBar(bar);

        addWindowListener(new FrameQuitter());

        pack();
        setVisible(true);
    }

        
    public ParameterScrollWidget getParameterScrollbar(Parameter p)
    {
        for (ParameterScrollWidget w : pan.getParameterScrolls())
            if (w.getParameter() == p)
                return w;

        return null;
    }

    public ActiveCanvas getCanvas()
    {
        return pan.getCanvas();
    }

    public void flash()
    {
        pan.getCanvas().repaint();
    }

    private PaintablePanel pan = null;
}
