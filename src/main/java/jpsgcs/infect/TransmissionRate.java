package jpsgcs.infect;

import jpsgcs.markov.Parameter;

public class TransmissionRate extends Parameter
{
    public TransmissionRate(String n, double df, int maxocc, double time)
    {
        super(n,0,10000000.0,df);
        deltatime = time;

        values = new double[maxocc+1];
        logvals = new double[values.length];
        mlogvals = new double[values.length];

        setValue(df);
    }

    public void setValue(double v)
    {
        val = v;
        double vv = Math.exp(-v*deltatime);
        //double vv = 1-v;

        values[0] = 1;
        for (int i=1; i<values.length; i++)
            values[i] = values[i-1]*vv;

        for (int i=0; i<values.length; i++)
            {
                values[i] = 1-values[i];
                logvals[i] = Math.log(values[i]);
                mlogvals[i] = Math.log(1-values[i]);
            }
    }

    public double getInterval()
    {
        return deltatime;
    }

    public double getValue()
    {
        return val;
    }

    public double getValue(int i)
    {
        return values[i];
    }

    public double logValue(int i)
    {
        return logvals[i];
    }

    public double logOneMinusValue(int i)
    {
        return mlogvals[i];
    }

    private double val = 0;
    private double deltatime = 0;
    private double[] values = null;
    private double[] logvals = null;
    private double[] mlogvals = null;
}
