package jpsgcs.infect;

import jpsgcs.viewgraph.Blob;
import java.awt.Graphics;
import java.awt.Color;

public class PadyBlob extends Blob
{
    public PadyBlob(Pady pad, int s)
    {
        p = pad;
        setSize(s,s);

        if (p.getResult() == -1)
            {
                setBorderColors(Color.green,Color.green);
                setShape(1);
            }
        else if (p.getResult() == 1)
            {
                setBorderColors(Color.red,Color.red);
                setShape(1);
            }
    }

    public void paint(Graphics g, double dx, double dy, boolean b)
    {
        setColor(p.getState() == 0 ? Color.white : (p.getState() == 1 ? Color.gray : Color.yellow));
        super.paint(g,dx,dy,b);
    }

    private Pady p = null;
}
