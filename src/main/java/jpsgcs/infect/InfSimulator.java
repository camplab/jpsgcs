package jpsgcs.infect;

import jpsgcs.hashing.RandomSet;
import java.util.Random;

public class InfSimulator
{
	public static final int ADMIT = 0;
	public static final int INFEC = 1;
	public static final int DISCH = 2;
	public static final int TEST = 3;

	public InfSimulator(double ilambda, double iimportation, double idetection, double iend, int icapacity, double imeanstay, double ibalance, double itestperpady, boolean iwithinf, boolean itestonadmit, Random r)
	{
		rand = r;
		now = 0;
		npeople = 0;
		end = iend;
		delta = 1/imeanstay;
		alpha = delta * icapacity * ibalance;
		lambda = ilambda;
		importation = iimportation;
		detection = idetection;
		capacity = icapacity;
		withinf = iwithinf;
		testperpady = itestperpady;
		testonadmit = itestonadmit;

		all = new RandomSet<Person>(rand);
		sus = new RandomSet<Person>(rand);
	}

	public boolean hasNext()
	{
		return now < end || store != null || store2 != null;
	}
	
public String simps = "";
public int count = 0;
public int cimps = 0;
public int chimps = 0;

	public Event nextEvent()
	{
			if (store != null)
			{
				Event res = store;
				store = null;
				return res;
			}

			if (store2 != null)
			{
				Event res = store2;
				store2 = null;
				return res;
			}

		if (now >= end)
			return null;

	//	while (now < end)
		while (true)
		{
/*
			if (store != null)
			{
				Event res = store;
				store = null;
				return res;
			}

			if (store2 != null)
			{
				Event res = store2;
				store2 = null;
				return res;
			}
*/

			Person p = null;

			double next = exp(alpha);
			int type = ADMIT;
	
			double t = exp(delta * all.size());
			if (t < next)
			{
				next = t;
				type = DISCH;
			}
	
			t = exp(lambda * sus.size() * (all.size() - sus.size()));
			if (t < next)
			{
				next = t;
				type = INFEC;
			}
	
			t = exp(testperpady * all.size());
			if (t < next)
			{
				next = t;
				type = TEST;
			}
	
			now += next;

			switch(type)
			{
			case ADMIT:
				if (all.size() >= capacity)
					break;
	
				p = new Person(++npeople);
				p.admit = now;
				all.add(p);
				sus.add(p);
					
				Event res = new ContinuousEvent(now,p.index,Event.ADMISSION);

				if (rand.nextDouble() < importation)
				{
					p.infected = true;
					p.inftime = p.admit;
					sus.remove(p);
					if (withinf)
						store = new ContinuousEvent(now,p.index,Event.INFECTION);

					//simps = simps+" "+p.index;

					++cimps;
				}

				++count;

				if (testonadmit)
				{
					int testres = (p.infected && rand.nextDouble() < detection ? Event.POSTEST : Event.NEGTEST);
					//now += 0.0000001;
					store2 =  new ContinuousEvent(now,p.index,testres);
					//if (testres == Event.POSTEST)
						//++chimps;
				}

				return res;

			case DISCH:
				p = all.next();
				all.remove(p);
				sus.remove(p);
				p.disch = now;
				return new ContinuousEvent(now,p.index,Event.DISCHARGE);

			case TEST:
				p = all.next();
				int testres = (p.infected && rand.nextDouble() < detection ? Event.POSTEST : Event.NEGTEST);
				return new ContinuousEvent(now,p.index,testres);

			case INFEC:
				p = sus.next();
				sus.remove(p);
				p.infected = true;
				p.inftime = now;
				if (withinf)
					return new ContinuousEvent(now,p.index,Event.INFECTION);
				break;
			}
		}

	//	return null;
	}

	public void setTransmission(double t)
	{
		lambda = t;
	}

// Private data and methods.

	protected Event store = null;
	protected Event store2 = null;
	protected int npeople = 0;
	protected double end = 0;
	protected double now = 0;
	protected double delta = 0;
	protected double alpha= 0;
	protected double lambda = 0;
	protected double testperpady = 0;
	protected double importation = 0;
	protected double detection = 0;
	protected int capacity =0;
	protected boolean withinf = false;
	protected boolean testonadmit = false;
	protected Random rand = null;

	protected RandomSet<Person> all = null;
	protected RandomSet<Person> sus = null;
	
	protected double exp(double rate)
	{
		return -Math.log(rand.nextDouble()) / rate;
	}
}
