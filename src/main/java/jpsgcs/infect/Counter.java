package jpsgcs.infect;

import jpsgcs.markov.Variable;

public class Counter extends Variable
{
    public Counter(int day, int max)
    {
        super(max);
        d = day;
    }

    public String toString()
    {
        return "C:"+d;
    }

    public void increase()
    {
        setState(getState()+1);
    }

    public void decrease()
    {
        setState(getState()-1);
    }

    private int d = 0;
}
