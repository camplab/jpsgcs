package jpsgcs.infect;

import jpsgcs.markov.Parameter;
import java.util.ArrayList;
import java.util.Random;

abstract public class InfectionSampler
{
    /**
       Sub classes need to implement and updating scheme for the 
       parameters used in this sampler.
    */
    abstract public void update();

    abstract public void maximize();

    /**
       This constructor just sets up a Random object that can be used for 
       simulations. Sub class contructors need to specify the parameters
       and the updating scheme.
    */  
    public InfectionSampler()
    {
        rand = new Random();
    }

    /**
       Return an array of paramter names. The order of the names will
       correspond to the order of the columns of the output.
    */
    final public String[] parNames()
    {
        String[] s = new String[pars.length];
        for (int i=0; i<s.length; i++)
            s[i] = pars[i].name();
        return s;
    }

    /**
       Returns the number of parameters.
    */
    final public int nParameters()
    {
        return pars.length;
    }

    /**
       Runs the updating scheme the specified number of times and returns
       an array of parameter values. The length of the array is 
       nparams * nsims, with the parameter values after the first update
       followed by those from the second, and so on.
    */
    final public double[] run(int nsims, boolean max)
    {
        double[] res = new double[nsims*pars.length];

        for (int i=0, k=0; i<nsims; i++)
            {
                if (max)
                    maximize();
                else
                    update();
                for (int j=0; j<pars.length; j++)
                    res[k++] = pars[j].getValue();
            }

        return res;
    }

    /**
       Runs the updating scheme once and returns an array of parameter values.
    */
    final public double[] run(boolean max)
    {
        return run(1,max);
    }

    //  Private data and methods. 

    protected Random rand = null;
    protected Parameter[] pars = null;

    //  The following are some simple utility functions.

    protected double logit(double x)
    {
        return Math.log(x/(1-x));
    }

    protected double invLogit(double x)
    {
        double y = Math.exp(x);
        return y / (1+y);
    }

    protected double log(double x)
    {
        return Math.log(x);
    }

    protected double invlog(double x)
    {
        return Math.exp(x);
    }

    protected double dlog(double x)
    {
        return 1/x;
    }

    protected double rnorm(double sigma)
    {
        return rand.nextGaussian() * sigma;
    }

    protected double rexp()
    {
        return -Math.log(rand.nextDouble());
    }

    protected double runif()
    {
        return rand.nextDouble();
    }
}
