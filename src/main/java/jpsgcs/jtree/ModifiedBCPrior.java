package jpsgcs.jtree;

import java.util.Set;
import java.util.LinkedHashSet;

public class ModifiedBCPrior<V> extends WSMGraphLaw<V>
{
	public ModifiedBCPrior(double x, double y)
	{
		pc = x;
		ps = y;
	}
	
	public double logPotential(Set<V> c, boolean asclique)
	{
		if (asclique)
		{
			return pc*(c.size()-1);
		}
		else
		{
			return ps*c.size();
		}
	}

	public String toString()
	{
		return "ModifiedBC("+pc+","+ps+")";
	}

	private double pc = 1;
	private double ps = 1;
}
