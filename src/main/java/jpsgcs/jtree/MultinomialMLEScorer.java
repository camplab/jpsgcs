package jpsgcs.jtree;

import jpsgcs.markov.Variable;
import jpsgcs.markov.MultiVariable;
import jpsgcs.markov.DenseTable;
import jpsgcs.markov.Table;
import jpsgcs.markov.MarkovRandomField;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Random;

public class MultinomialMLEScorer extends SMGraphLaw<Variable>
{
	public MultinomialMLEScorer(Variable[] x, IntegerMatrix y, double pen, Random r)
	{
		rand = r;

		alpha = pen;

		v = x;
		map = new LinkedHashMap<Variable,Integer>();
		for (int i=0; i<x.length; i++)
			map.put(v[i],i);

		data = y;

		expos = new double[y.nRows()];
		for (int i=0; i<expos.length; i++)
			expos[i] = 1;

		store = new LinkedHashMap<Set<Variable>,Double>();
	}

	public void sampleParameters()
	{
		for (int i=0; i<expos.length; i++)
			expos[i] = -Math.log(rand.nextDouble());
		store.clear();
	}

	public void maximizeParameters()
	{
		for (int i=0; i<expos.length; i++)
			expos[i] = 1;
		store.clear();
	}

	public double logPotential(Set<Variable> ss)
	{
		LinkedHashSet<Variable> s = new LinkedHashSet<Variable>(ss);
		Double d = store.get(s);
		if (d != null)
			return d.doubleValue();

/*
		double[] tab = getTable(s).getTable();
		//double x = logLikelihood(tab) - alpha * (tab.length-1);
		
		System.err.println(logLikelihood(tab) + "\t" + logLikelihood(s) + "\t" +
			(logLikelihood(tab) - logLikelihood(s)));
		//double x = logLikelihood(s) - alpha * (tab.length-1);

*/
		double df = 1;
		for (Variable v : s)
			df *= v.getNStates();

		double x = logLikelihood(s) - alpha * (df-1);

		//System.err.println(s+"\t"+x);

		store.put(s,x);

		return x;
	}

	private double logGamma(double x)
	{
		double tmp = (x - 0.5) * Math.log(x + 4.5) - (x + 4.5);
      		double ser = 1.0 + 76.18009173 / (x + 0) - 86.50532033 / (x + 1)
                       			+ 24.01409822 / (x + 2) - 1.231739516 / (x + 3)
                       			+ 0.00120858003 / (x + 4) - 0.00000536382 / (x + 5);

      		return tmp + Math.log(ser * Math.sqrt(2 * Math.PI));
	}

	private double logLikelihood(double[] tab)
	{
		double ll = 0;
		double nn = 0;

		for (int i=0; i<tab.length; i++)
			if (tab[i] > 0)
			{
				ll += tab[i] * Math.log(tab[i]);
				nn += tab[i];
			}

		return ll - nn*Math.log(nn);
	}

	private double logLikelihood(Set<Variable> s)
	{
		double tot = 0;
		double ll = 0;
		DenseTable t = new DenseTable(s);
		DenseTable p = new DenseTable(s);
		t.allocate();
		p.allocate();

		for (int i=0; i<data.nRows(); i++)
		{
			for (Variable v : s)
				v.setState(data.value(i,index(v)));
			t.increase(1);
			p.increase(expos[i]);
			tot += expos[i];
		}

		p.scale(1/tot);

		MultiVariable m = new MultiVariable(s);

		for (m.init(); m.next(); )
			if (t.getValue() > 0)
				ll += t.getValue() * Math.log(p.getValue());

		return ll;
	}

	public void setPenalty(double a)
	{
		alpha = a;
	}

	public double getPenalty()
	{
		return alpha;
	}

	public void clear()
	{
		store.clear();
	}

	public DenseTable getTable(Set<Variable> s)
	{
		DenseTable t = new DenseTable(s);
		t.allocate();

		for (int i=0; i<data.nRows(); i++)
		{
			for (Variable v : s)
				v.setState(data.value(i,index(v)));
			t.increase(1);
		}

		return t;
	}
	
	public MarkovRandomField fitModel(JTree<Variable> jt)
	{
		MarkovRandomField mod = new MarkovRandomField();

		Map<Clique<Variable>,Clique<Variable>> seq = jt.cliqueElimination();
		
		for (Clique<Variable> c : seq.keySet())
		{
			Set<Variable> s = null;
			if (seq.get(c) != null)
				s = jt.connection(c,seq.get(c));

			DenseTable tc = getTable(c);
			
			if (s == null)
			{
				mod.add(tc);
				continue;
			}

			DenseTable ts = new DenseTable(s);
			ts.allocate();
			
			MultiVariable m = tc.getMultiVariable();
			for (m.init(); m.next(); )
				ts.increase(tc.getValue());

			for (m.init(); m.next(); )
			{
				double x = tc.getValue();
				if (x > 0)
				{
					x /= ts.getValue();
					tc.setValue(x);
				}
			}
			
			mod.add(tc);
		}

		return mod;
	}

// Private data.

	private double alpha = 1;
	private IntegerMatrix data = null;
	private Variable[] v = null;
	private Map<Variable,Integer> map = null;
	private Map<Set<Variable>,Double> store = null;
	private double[] expos = null;
	private Random rand = null;

	private int index(Variable x)
	{
		return map.get(x).intValue();
	}
}
