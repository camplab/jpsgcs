package jpsgcs.jtree;

//import jpsgcs.graph.IdentityNetwork;
import jpsgcs.graph.Network;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

//public class CliqueSeparatorGraph<V> extends IdentityNetwork<Clique<V>,Object>
public class CliqueSeparatorGraph<V> extends Network<Clique<V>,Object>
{
	public CliqueSeparatorGraph(JTree<V> jt)
	{
		//super(true);
		super(true,true);
		rand = jt.rand;

		ArrayList<Clique<V>> x = getSortedSeparators(jt);

		for (int i = 0; i<x.size(); i++)
		{
			Clique<V> u = x.get(i);
			add(u);

			Set<Clique<V>> par = new LinkedHashSet<Clique<V>>();
			for (int j = i-1; j>=0; j--)
				if (x.get(j).size() < u.size())
					par.add(x.get(j));

			while (!par.isEmpty())
			{
				Clique<V> v = par.iterator().next();
				par.remove(v);

				if (u.containsAll(v))
				{
					connect(v,u);
					par.removeAll(ancestors(v));
				}
			}
		}

		for (Clique<V> u : jt.getCliques())
		{
			add(u);

			Set<Clique<V>> par = new LinkedHashSet<Clique<V>>();
			for (int j = x.size()-1; j>=0; j--)
				if (x.get(j).size() < u.size())
					par.add(x.get(j));

			while (!par.isEmpty())
			{
				Clique<V> v = par.iterator().next();
				par.remove(v);

				if (u.containsAll(v))
				{
					connect(v,u);
					par.removeAll(ancestors(v));
				}
			}
		}
	}

	public Set<Clique<V>> ancestors(Clique<V> v)
	{
		Set<Clique<V>> s = new LinkedHashSet<Clique<V>>();

		ArrayList<Clique<V>> a = new ArrayList<Clique<V>>();
		a.add(v);

		for(int i=0; i<a.size(); i++)
		{
			for (Clique<V> y : inNeighbours(a.get(i)))
			{
				if (!s.contains(y))
				{
					s.add(y);
					a.add(y);
				}
			}
		}

		return(s);
	} 

	public ArrayList<Clique<V>> getSortedSeparators(JTree<V> jt)
	{
		ArrayList<Clique<V>> x = new ArrayList<Clique<V>>();
		int i = 0;

		for (Clique<V> s : jt.getDistinctSeparators())
		{
			x.add(i,s);
			for (int j=i; j>0; j--)
			{
				if (x.get(j).size() < x.get(j-1).size())
				{
					Clique<V> t = x.get(j-1);
					x.set(j-1,x.get(j));
					x.set(j,t);
				}
			}
			i++;
		}

		return x;
	}

	protected Random rand = null;
}
