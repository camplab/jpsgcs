package jpsgcs.jtree;

public class SubMatrix
{
	public int[] index;
	public double[][] matrix;

	public SubMatrix(int[] q, double[][] m)
	{
		index = q;
		matrix = m;
	}
}
