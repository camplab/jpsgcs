package jpsgcs.jtree;

public interface DoubleMatrix extends Matrix
{
	public double value(int i, int j);
	public void setValue(int i, int j, double x);
}
