package jpsgcs.jtree;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Set;

public class OrderedJTree<V> extends OnePairSampler<V>
{
	public OrderedJTree(JTree<V> jt, V[] o, double max, Set<V> ok)
	{
		this(jt,o,max,ok,null);
	}

	public OrderedJTree(JTree<V> jt, V[] o, double mx, Set<V> ok, WSMGraphLaw<V> cs)
	{
		super(jt,cs,false);
		map = new LinkedHashMap<V,Integer>();
		for (int i=0; i< o.length; i++)
			map.put(o[i],i);
		ord = o;
		max = mx;

		allowed = ok;
	}

	public UpdateInfo<V> proposeConnection()
	{
		UpdateInfo<V> up = new UpdateInfo<V>();

		if (jt.separators.isEmpty())
		{
			up.errcode = 1;
			return up;
		}

		up.S = jt.separators.next();

		up.Cx = up.S.getX();
		up.x = up.Cx.next();
		while (up.S.contains(up.x)) 
			up.x = up.Cx.next();

		up.Cy = up.S.getY();
		up.y = up.Cy.next();
		while (up.S.contains(up.y)) 
			up.y = up.Cy.next();

		if (!(allowed.contains(up.x) && allowed.contains(up.y)))
		{
			up.errcode = 1;
			return up;
		}

		if (Math.abs(rank(up.x)-rank(up.y)) > max)
		{
			up.errcode = 1;
			return up;
		}

		up.Aij = jt.separators.size() * (up.Cx.size()-up.S.size()) * (up.Cy.size()-up.S.size());
		up.type = -1;

		if (up.Cx.size() == up.S.size() + 1)
		{
			if (up.Cy.size() == up.S.size() + 1)
			{
				up.Aij /= (jt.cliques.size()-1) * (up.S.size()+2) * (up.S.size()+1) / 2.0;

				for (Clique<V> N : jt.getNeighbours(up.Cx))
					if (N != up.Cy && !N.contains(up.x))
						up.Aij /= 2;

				for (Clique<V> N : jt.getNeighbours(up.Cy))
					if (N != up.Cx && !N.contains(up.y))
						up.Aij /= 2;
				
				up.type = 0;
			}
			else
			{
				up.Aij /= jt.cliques.size() * (up.S.size()+2) * (up.S.size()+1) / 2.0;
				up.type = 2;
			}
		}
		else
		{
			if (up.Cy.size() == up.S.size() + 1)
			{
				up.Aij /= jt.cliques.size() * (up.S.size()+2) * (up.S.size()+1) / 2.0;
				up.type = 1;
			}
			else
			{
				up.Aij /= (jt.cliques.size()+1) * (up.S.size()+2) * (up.S.size()+1) / 2.0;
				up.type = 3;
			}
		}

		return up;
        }

	public UpdateInfo<V> proposeDisconnection()
	{
		UpdateInfo<V> up = new UpdateInfo<V>();

		if (jt.cliques.isEmpty())
		{
			up.errcode = 1;
			return up;
		}

		up.Cxy = jt.cliques.next();
		if (up.Cxy.size() < 2)
		{
			up.errcode = 2;
			return up;
		}

		up.x = up.Cxy.next();
		up.y = up.Cxy.next();
		while (up.y == up.x) 
			up.y = up.Cxy.next();

		if (!(allowed.contains(up.x) && allowed.contains(up.y)))
		{
			up.errcode = 1;
			return up;
		}

		int N = 0;
		int Nx = 0;
		int Ny = 0;
		up.Cx = null;
		up.Cy = null;

		up.Cxy.remove(up.x);
		up.Cxy.remove(up.y);

		for (Clique<V> P : jt.getNeighbours(up.Cxy))
		{
			if (P.contains(up.x))
			{
				if (P.contains(up.y))
				{
					up.Cxy.add(up.x);
					up.Cxy.add(up.y);
					up.errcode = 3;
					return up;
				}
				else
				{
					Nx++;
					if (jt.connection(P,up.Cxy).containsAll(up.Cxy))
						up.Cx = P;
				}
			}
			else
			{
				if (P.contains(up.y))
				{
					Ny++;
					if (jt.connection(P,up.Cxy).containsAll(up.Cxy))
						up.Cy = P;
				}
				else
				{
					N++;
				}
			}
		}

		up.Cxy.add(up.x);
		up.Cxy.add(up.y);

		up.Aij = jt.cliques.size() * up.Cxy.size() * (up.Cxy.size() - 1) / 2.0;
		up.type = -1;

		if (up.Cx == null)
		{
			if (up.Cy == null)
			{
				up.Aij /= jt.separators.size()+1;
				up.Aij /= Math.pow(0.5,N);
				up.type = 0;
			}
			else
			{
				if (Ny > 1)
				{
					up.errcode = 4;
					return up;
				}
				up.Aij /= jt.separators.size() * (up.Cy.size() - up.Cxy.size() + 2);
				up.type = 2;
			}
		}
		else
		{
			if (up.Cy == null)
			{
				if (Nx > 1)
				{
					up.errcode = 5;
					return up;
				}
				up.Aij /= jt.separators.size() * (up.Cx.size() - up.Cxy.size() + 2);
				up.type = 1;
			}
			else
			{
				if (Nx > 1 || Ny > 1 || N > 0)
				{
					up.errcode = 6;
					return up;
				}
				up.Aij /= (jt.separators.size()-1) * (up.Cx.size() - up.Cxy.size() + 2) * (up.Cy.size() - up.Cxy.size() + 2);
				up.type = 3;
			}
		}

		return up;
	}

// Private data and methods.

	private V[] ord = null;
	private Map<V,Integer> map = null;
	protected double max = 0;
	protected Set<V> allowed = null;

	protected int rank(V x)
	{
		return map.get(x).intValue();
	}

	protected V ranked(int i)
	{
		if (i < 0 || i >= ord.length)
			return null;
		return ord[i];
	}

	protected int topRank(Clique<V> s)
	{
		int r = rank(s.next())+1;
		for ( ; r < ord.length && s.contains(ord[r]); r++);
		return r-1;
	}

	protected V bot(Clique<V> s)
	{
		if (s.isEmpty())
			return null;

		V x = s.next();
		int r = rank(x) - 1;
		while (r >= 0 && s.contains(ord[r]))
		{
			x = ord[r];
			r--;
		}
		return x;
	}

	protected V top(Clique<V> s)
	{
		if (s.isEmpty())
			return null;

		V x = s.next();
		int r = rank(x) + 1;
		while (r < ord.length && s.contains(ord[r]))
		{
			x = ord[r];
			r++;
		}
		return x;
	}
}
