package jpsgcs.jtree;

public interface IntegerMatrix extends Matrix
{
	public int value(int i, int j);
}
