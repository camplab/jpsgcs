package jpsgcs.stoch;

public class Discrete implements Variable
{
    public Discrete()
    {
    }

    public Discrete(int i)
    {
        v = i;
    }

    public double getValue()
    {
        return v;
    }

    public int getState()
    {
        return v;
    }

    public void setValue(int i)
    {
        v = i;
    }

    public void setValue(double d)
    {
        v = (int) (d+0.5);
    }

    private int v = 0;
}
