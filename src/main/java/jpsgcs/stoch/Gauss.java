package jpsgcs.stoch;

import jpsgcs.util.Matrix;
import jpsgcs.util.MatrixInverseData;

import java.util.Random;

public class Gauss
{
    private static final Random rand = new Random();
    private static final double log2pi = Math.log(2 * Math.PI);

    public static double logf(double x, double mu, double sigma2)
    {
        return - 0.5 * ( (x-mu) * (x-mu) / sigma2  + log2pi + Math.log(sigma2) );
    }

    public static double f(double x, double mu, double sigma2)
    {
        return Math.exp(logf(x,mu,sigma2));
    }

    public static double logf(double x)
    {
        return - 0.5 * ( x*x + log2pi );
    }

    public static double f(double x)
    {
        return Math.exp(logf(x));
    }

    public static double logf(double[] x, double[] mu, double[][] invSigma, double logDeterminant)
    {
        double res = 0;

        for (int i=0; i<x.length; i++)
            for (int j=0; j<x.length; j++)
                res += (x[i]-mu[i]) * invSigma[i][j] * (x[j]-mu[j]);

        return - 0.5 * ( res + logDeterminant + x.length * log2pi );
    }

    public static double logf(double[] x, double[] mu, double[][] Sigma)
    {
        MatrixInverseData mdata = Matrix.invert(Sigma);
        return logf( x, mu, mdata.inv(), Math.log(mdata.det()) );
    }

    public static double f(double[] x, double[] mu, double[][] Sigma)
    {
        return Math.exp(logf(x,mu,Sigma));
    }

    public static double random()
    {
        /*
          double x = 0;
          double y = 0;
          double s = 0;

          do
          {
          x = 2*Math.random()-1;
          y = 2*Math.random()-1;
          s = x*x + y*y;
          }
          while (s >= 1);

          s = Math.sqrt(-2*Math.log(s)/s);

          return x * s;
        */
        return rand.nextGaussian();
    }

    public static double random(double mu, double sigma2)
    {
        return random() * Math.sqrt(sigma2) + mu;
    }

    public static double[] random(double[] mu, double[][] Sigma)
    {
        double[][] chol = Matrix.cholesky(Sigma);
        return random(mu,chol,true);
    }

    public static double[] random(double[] mu, double[][] Cholesky, boolean dummy)
    {
        double[] x =  new double[mu.length];

        double[] z = new double[mu.length];
        for (int i=0; i<z.length; i++)
            z[i] = random();

        for (int i=0; i<x.length; i++)
            {
                x[i] = mu[i];
                for (int j=0; j<=i; j++)
                    x[i] += Cholesky[i][j] * z[j];
            }

        return x;
    }

    /*
      Test main.
    */

    public static void main(String[] args)
    {
        try
            {
                /*
                  double[] v = new double[2];
                  double[] mu = { 1.0 , 1.0 };
                  double[][] Sigma = { {4,0} , {0,4} };

                  for (double x = -3; x < 3; x += 0.01)
                  {
                  v[0] = v[1] = x;
                  System.out.println(x+"\t"+Gauss.f(x,1,4)+"\t"+Math.sqrt(Gauss.f(v,mu,Sigma)));
                  }
                  for (int i=0; i<1000; i++)
                  System.out.println(Gauss.random());
                */

                /*
                  double[][] Sigma = { {2,1} , {3,4} };
                        
                  print(Sigma);
                  System.out.println();
        
                  double[][] trans = Matrix.transpose(Sigma);
                  print(trans);
                  System.out.println();

                  double[][] prod = Matrix.product(Sigma,Sigma);
                  print(prod);
                  System.out.println();
                  System.out.println("===============================================");

                  double[][] Sym = { {4,.9} , {.9,2} };
                  print(Sym);
                  System.out.println();
                        
                  double[][] chol = Matrix.cholesky(Sym);
                  print(chol);
                  System.out.println();

                  double[][] check = Matrix.product(chol,Matrix.transpose(chol));
                  print(check);
                  System.out.println();

                  System.out.println("===============================================");
                */

                double[][] Sigma = { {1,-0.25} , {-0.25,1} };

                print(Sigma);
                System.out.println();

                double[][] chol = Matrix.cholesky(Sigma);
                print(chol);
                System.out.println();

                print(Matrix.product(chol,Matrix.transpose(chol)));
                System.out.println();
                        
                /*

                  double[] mu = { 100, 50 };

                  for (int i=0; i<100000; i++)
                  {
                  double[] x = Gauss.random(mu,Sigma);
                  System.out.println(x[0]+"\t"+x[1]);
                  }
                */


                        
            }
        catch (Exception e)
            {
                e.printStackTrace();
            }
    }

    public static void print(double[][] x)
    {
        for (int i=0; i<x.length; i++)
            {
                for (int j=0; j<x[i].length; j++)
                    System.err.print(x[i][j]+"\t");
                System.err.println();
            }
    }
                
}
