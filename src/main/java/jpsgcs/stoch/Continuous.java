package jpsgcs.stoch;

public class Continuous implements Variable
{
    public Continuous()
    {
    }

    public Continuous(double d)
    {
        v = d;
    }

    public double getValue()
    {
        return v;
    }

    public void setValue(double d)
    {
        v = d;
    }

    private double v = 0;
}
