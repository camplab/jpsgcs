package jpsgcs.apps.zeroloop;

import jpsgcs.markov.Variable;

public class Edge extends Variable
{
    public Edge(Object ff, Object tt)
    {
        super(2);
        f = ff;
        t = tt;
    }
        
    public Object to()
    {
        return t;
    }

    public Object from()
    {
        return f;
    }

    private Object f = null;
    private Object t = null;
}
