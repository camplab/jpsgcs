package jpsgcs.apps.zeroloop;

public class Individual implements PedNode
{
    public Individual(int i)
    {
        nm = i;
    }
        
    public String toString()
    {
        return nm+"\t"+ (par == null ? 0+"\t"+0 : par.pa.nm+"\t"+par.ma.nm) + "\t"+ (sex == 0 ? 2 : 1 );
    }
                
    public int nm = 0;
    public int sex = 0;
    public Marriage par = null;
}
