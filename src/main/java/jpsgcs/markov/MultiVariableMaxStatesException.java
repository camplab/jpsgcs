package jpsgcs.markov;

public class MultiVariableMaxStatesException extends RuntimeException {
  public MultiVariableMaxStatesException (String msg) {
    super(msg);
  }
}
