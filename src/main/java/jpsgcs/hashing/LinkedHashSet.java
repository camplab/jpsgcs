package jpsgcs.hashing;

import java.util.Set;
import java.util.Collection;

public class LinkedHashSet<E> extends HashSet<E> implements Set<E>
{
    public LinkedHashSet() 
        {
            super(true);
        }

    public LinkedHashSet(Collection<? extends E> c) 
        {
            super(c,true);
        }
}
