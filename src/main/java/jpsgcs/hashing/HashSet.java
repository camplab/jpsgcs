package jpsgcs.hashing;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.Set;

public class HashSet<E> extends AbstractSet<E> implements Set<E>
{
    public HashMap<E,Object> map()
        {
            return map;
        }

    public HashSet() 
        {
            this(false);
        }

    public HashSet(boolean linked)
        {
            map = linked ? new LinkedHashMap<E,Object>() : new HashMap<E,Object>();
        }

    public HashSet(Collection<? extends E> c) 
        {
            this(c,false);
        }

    public HashSet(Collection<? extends E> c, boolean linked)
        {
            map = linked ? new LinkedHashMap<E,Object>() : new HashMap<E,Object>();
            addAll(c);
        }

    public Iterator<E> iterator() 
    {
        return map.keySet().iterator();
    }

    public int size() 
    {
        return map.size();
    }

    public boolean isEmpty() 
    {
        return map.isEmpty();
    }

    public boolean contains(Object o) 
    {
        return map.containsKey(o);
    }

    public boolean add(E o) 
    {
        return map.put(o, PRESENT)==null;
    }

    public boolean remove(Object o) 
    {
        return map.remove(o)==PRESENT;
    }

    public void clear() 
    {
        map.clear();
    }


    private HashMap<E,Object> map;
    private static final Object PRESENT = new Object();

}
