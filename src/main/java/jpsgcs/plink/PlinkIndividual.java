package jpsgcs.plink;

import java.io.PrintStream;

public class PlinkIndividual
{
	protected String ped = null;
	protected String name = null;
	protected String pa = null;
	protected String ma = null;
	protected int sex = 0;
	protected int aff = 0;
	protected int[][] allele = null;

	protected int proband = 0;
	protected int index = 0;

	public PlinkIndividual(String pd, String nm, String p, String m, int x, int af)
	{
		ped = pd;
		name = nm;
		if (!p.equals("0"))
			pa = p;
		if (!m.equals("0"))
			ma = m;
		sex = x;
		aff = af;
	}

	public PlinkIndividual(String pd, String nm, String p, String m, int x, int af, int nloc)
	{
		this(pd,nm,p,m,x,af);
		allele = new int[nloc][2];
	}

	public void writeAsLinkage(PrintStream pedf, boolean premake)
	{
		pedf.print(ped+" "+name+"     ");
		pedf.print((pa == null ? "0" : pa)+" "+(ma == null ? "0" : ma));

		if (!premake)
			pedf.print(" 0 0 0");

		pedf.print("     "+sex);

		if (!premake)
			pedf.print(" "+proband);

		pedf.print("     "+aff+" "+aff+"  ");

		for (int j=1; j<allele.length; j++)
			pedf.print("  "+allele[j][0]+" "+allele[j][1]);
		pedf.println();
	}

	public String toString()
	{
		StringBuffer b = new StringBuffer();
		b.append(ped+" "+name+" "+(pa == null ? "0" : pa) +" "+(ma == null ? "0" : ma) +" "+sex+" "+aff+" ");
		if (allele != null)
		{
			for (int i=1; i<allele.length; i++)
				b.append(allele[i][0]+" "+allele[i][1]+" ");
		}

		b.deleteCharAt(b.length()-1);
		return b.toString();
	}
}
