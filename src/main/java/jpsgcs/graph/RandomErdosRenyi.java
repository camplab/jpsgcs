package jpsgcs.graph;

import jpsgcs.util.PoissonRV;

/**
   This program generates a random graph with a given number of vertices and
   edges.

   <ul>
   <li>Usage: <b> java RandomERGraph [v] [e] > output.graph </b>
   </li>
   </ul>

   Where
   <ul>
   <li> <b> v </b> is the number of vertices. Default is 1000. </li>
   <li> <b> e </b> is the number of edges. Default is 2000. </li>
   </ul>
   The output from this program can be immediately input to the
   graph viewing program ViewGraph.
*/
        

public class RandomErdosRenyi
{
    public static Network<String,String> next(int n)
    {
        return next(n,Math.log(n)/n);
    }

    public static Network<String,String> next(int n, double p)
    {
        PoissonRV X = new PoissonRV(n*(n-1)/2.0*p);
        return  next(n,X.next());
    }
        
    public static Network<String,String> next(int n, int e)
    {
        Network<String,String> g = new Network<String,String>();

        String[] s = new String[n];
        for (int i=0; i<s.length; i++)
            {
                s[i] = new String(""+i);
                g.add(s[i]);
            }

        if (e >= (n*(n-1))/2)
            {
                for (int i=0; i<s.length; i++)
                    for (int j=i+1; j<s.length; j++)
                        g.connect(s[i],s[j]);
            }
        else
            {
                for (int i=0; i<e; i++)
                    {
                        int a = 0;
                        int b = 0;

                        while ( a == b || g.connects(s[a],s[b]) )
                            {
                                a = (int)(Math.random()*s.length);
                                b = (int)(Math.random()*s.length);
                            }

                        g.connect(s[a],s[b]);
                    }

            }

        return g;
    }

    public static void main(String[] args)
    {
        try
            {
                int v = 1000;
                int e = 2000;

                switch(args.length)
                    {
                    case 2: e = Integer.parseInt(args[1]);
                    case 1: v = Integer.parseInt(args[0]);
                    }

                System.out.println(RandomErdosRenyi.next(v,e));
                        
            }
        catch (Exception e)
            {
                System.err.println("Caught in RandomERGraph.main()");
                e.printStackTrace();
                System.exit(1);
            }
    }
}
