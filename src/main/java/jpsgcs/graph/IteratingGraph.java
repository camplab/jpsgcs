package jpsgcs.graph;

import java.util.Collection;
import java.util.ArrayList;

public class IteratingGraph<V,E> extends Network<V,E>
{
    public IteratingGraph(Collection<V> vv)
        {
            for (V u : vv)
                super.add(u);
                
            ArrayList<V> a = new ArrayList<V>(getVertices());
            e = (Flipper<V,E>[]) new Flipper[(a.size()*(a.size()-1))/2];

            for (int i=0, k=0; i<a.size(); i++)
                for (int j=i+1; j<a.size(); j++)
                    e[k++] = new Flipper<V,E>(this,a.get(i),a.get(j));
        }

    public void init()
    {
        for (int i=0; i<e.length; i++)
            e[i].init();
        count = 0;
    }

    public boolean next()
    {
        while (count >= 0)
            {
                if (count == e.length)
                    {
                        count--;
                        return true;
                    }

                if (e[count].next())
                    count++;
                else
                    e[count--].init();
            }
        return false;
    }

    public int nEdges()
    {
        int x = 0;
        for (int i=0; i<e.length; i++)
            x += e[i].getState();
        return x;
    }

    public void randomize()
    {
        randomize(0.5);
    }

    public void randomize(double p)
    {
        for (int i=0; i<e.length; i++)
            e[i].randomize(p);
    }

    public void clear() 
    { 
    }

    public boolean add(V v) 
    { 
        return false; 
    }

    public boolean remove(Object v) 
    { 
        return false; 
    }

    // Private data.

    private Flipper<V,E>[] e = null;
    private int count = 0;

    public static void main(String[] args)
    {
        try
            {
                Collection<Integer> x = new ArrayList<Integer>();
                for (int i=0; i<4; i++)
                    x.add(i);
        
                IteratingGraph<Integer,Object> g = new IteratingGraph<Integer,Object>(x);

                for (g.init(); g.next(); )
                    {
                        System.out.println("========");
                        System.out.println(g);
                    }
            }
        catch (Exception e)
            {
                e.printStackTrace();
            }
    }
}

class Flipper<V,E>
{
    public Flipper(Network<V,E> h, V x, V y)
        {
            g = h;
            u = x;
            v = y;
        }

    public void init()
    {
        state = -1;
    }

    public boolean next()
    {
        boolean b = ++state < 2;
        if (b)
            {
                if (state == 1)
                    g.connect(u,v);
                else
                    g.disconnect(u,v);
            }
        return b;
    }

    public void randomize(double p)
    {
        if (Math.random() < p)
            {
                state = 1;
                g.connect(u,v);
            }
        else
            {
                state = 0;
                g.disconnect(u,v);
            }
    }

    public int getState()
    {
        return state;
    }

    private Network<V,E> g = null;
    private V u = null;
    private V v = null;
    private int state = -1;
}
