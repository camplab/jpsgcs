package jpsgcs.graph;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

/*
        This is a flexible and reasonably efficient skeleton data structure 
	for implementing graphs.
        Derived classes can extend this to make directed or undirected graphs,
        to allow multiple connections between vertices or not, and
        to allow association of objects with edges or not.

        It is essentially a list of adjacencies implemented as a mapping
        of the two vertices to an object associated with the ordered/unordered pair.
        There is a mapping for forward edges and one for reverse edges, allowing
        for directed graphs. For undirected graphs a single mapping is
        used.
*/

public abstract class GraphSkeleton<V,T> 
{
	protected Map<V,Map<V,T>> forward = null;
	protected Map<V,Map<V,T>> backward = null;

	abstract protected Map<V,T> makeMap();

	public boolean contains(Object x)
	{
		return forward.containsKey(x);
	}

	public boolean connects(Object x, Object y)
	{
		Map<V,T> n = forward.get(x);
		return n == null ? false : n.containsKey(y) ;
	}

	public Set<V> getVertices()
	{
		return Collections.unmodifiableSet(forward.keySet());
	}

	public Set<V> outNeighbours(Object x)
	{
		Map<V,T> n = forward.get(x);
		return n == null ? null : Collections.unmodifiableSet(n.keySet());
	}

	public Set<V> inNeighbours(Object x)
	{
		Map<V,T> n = backward.get(x);
		return n == null ? null : Collections.unmodifiableSet(n.keySet());
	}

	public void clear()
	{
		forward.clear();
		backward.clear();
	}

	public void clearEdges()
	{
		for (Map<V,T> n : forward.values())
			n.clear();
		for (Map<V,T> n : backward.values())
			n.clear();
	}

	public boolean add(V x)
	{
		if (forward.containsKey(x))
			return false;

		forward.put(x,makeMap());
		if (backward != forward)
			backward.put(x,makeMap());

		return true;
	}

	public boolean disconnect(Object x, Object y)
	{
		if (!contains(x) || !contains(y) || !connects(x,y))
			return false;

		forward.get(x).remove(y);
		backward.get(y).remove(x);

		return true;
	}

	public boolean remove(Object x)
	{
		if (!contains(x))
			return false;

		for (V y : forward.get(x).keySet())
			if (y != x)
				backward.get(y).remove(x);
		forward.remove(x);

		if (backward != forward)
		{
			for (V y : backward.get(x).keySet())
				if (y != x)
					forward.get(y).remove(x);
			backward.remove(x);
		}

		return true;
	}

	public String toString()
	{
		StringBuffer s = new StringBuffer();

		for (V v : getVertices())
		{
			s.append(v+"\t");
			for (V u : inNeighbours(v))
			//for (V u : outNeighbours(v))
				s.append(u+" ");
			s.append("\n");
		}

		if (s.length() > 0)
			s.deleteCharAt(s.length()-1);

		return s.toString();
	}
}
