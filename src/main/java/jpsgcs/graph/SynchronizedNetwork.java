package jpsgcs.graph;

import java.util.Set;
import java.util.Map;
import java.util.Collections;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class SynchronizedNetwork<V,E> extends Network<V,E>
{
    public SynchronizedNetwork()
        {
            this(false);
        }

    public SynchronizedNetwork(boolean directed)
        {
            super(directed);
        }

    synchronized public Set<V> getVertices()
                            {
                                return new LinkedHashSet<V>(super.getVertices());
                            }

    synchronized public Set<V> getNeighbours(Object x)
                            {
                                return new LinkedHashSet<V>(super.getNeighbours(x));
                            }

    synchronized public Set<V> outNeighbours(Object x)
                            {
                                return new LinkedHashSet<V>(super.outNeighbours(x));
                            }

    synchronized public Set<V> inNeighbours(Object x)
                            {
                                return new LinkedHashSet<V>(super.inNeighbours(x));
                            }

    synchronized public boolean isDirected()
        {
            return super.isDirected();
        }

    synchronized public boolean connect(V x, V y, E e)
        {
            return super.connect(x,y,e);
        }

    synchronized public boolean connect(V x, V y)
        {
            return super.connect(x,y);
        }

    synchronized public E connection(Object x, Object y)
        {
            return super.connection(x,y);
        }

    synchronized public boolean contains(Object x)
        {
            return super.contains(x);
        }

    synchronized public boolean connects(Object x, Object y)
        {
            return super.connects(x,y);
        }

    synchronized public void clear()
        {
            super.clear();
        }

    synchronized public boolean add(V x)
        {
            return super.add(x);
        }

    synchronized public boolean disconnect(Object x, Object y)
        {
            return super.disconnect(x,y);
        }

    synchronized public boolean remove(Object x)
        {
            return super.remove(x);
        }

    synchronized public String toString()
        {
            return super.toString();
        }
}
