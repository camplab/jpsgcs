package jpsgcs.graph;

public interface Coordinated
{
    public double x();
    public double y();
}
