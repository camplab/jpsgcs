package jpsgcs.linkage;

public class LinkageId<T> implements Comparable<LinkageId<T>> {
    private T id = null;

    public String stringGetId() {
        return ""+id;
    }

    public T getId()
    {
        return id;  
    }

    public void setElement(T id) {
        this.id = id;
    }

    public LinkageId(T someid) {
        this.id = someid;
    }

    @Override
    public int compareTo(LinkageId<T> other) {
        if (other == null) {
            return 1;
        }
        return stringGetId().compareTo(other.stringGetId());
    }

    @Override
    public boolean equals(Object other) {
        if ( null == other )
            return false;
        if (this == other) 
            return true;
        if (other instanceof LinkageId<?>) {
            LinkageId<T> otherL = (LinkageId<T>)other;
            return compareTo(otherL) == 0;
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return stringGetId();
    }
}
