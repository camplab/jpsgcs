package jpsgcs.linkage;

import jpsgcs.util.StringFormatter;

/**
 This class represents the data for an individual for a locus
 in the linkage numbered allele input format.
*/
public class NumberedAllelePhenotype extends LinkagePhenotype
{
/** 
 Creates a new phenotype given the specified allele numbers and
 checks that the data is consistent with the parameters for 
 the given locus.
*/
	public NumberedAllelePhenotype(NumberedAlleleLocus l) 
	{
		setLocus(l);
		a1 = 0;
		a2 = 0;
	}

	public void read(LinkageFormatter f)
	{
		int a = f.readInt("allele code",0,true,false);
		int b = f.readInt("allele code",0,true,false);

		double[] freq = getLocus().alleleFrequencies();

		if (a < 0 || a > freq.length || b < 0 || b > freq.length)
		{
			if (a < 0 || a > freq.length)
				f.warn(getLocus().locName()+": allele code "+a+" is out of range "+0+" "+(freq.length)+"\n\tSetting genotype to 0 0");
			if (b < 0 || b > freq.length)
				f.warn(getLocus().locName()+": allele code "+b+" is out of range "+0+" "+(freq.length)+"\n\tSetting genotype to 0 0");
			a = 0;
			b = 0;
		}

		if (a > 0 && freq[a-1] <= 0)
			f.warn(getLocus().locName()+": allele code "+a+" has zero probability" +"\n\tRun GeneCountAlleles and/or CheckErrors on this data.");
		if (b > 0 && freq[b-1] <= 0)
			f.warn(getLocus().locName()+": allele code "+b+" has zero probability" +"\n\tRun GeneCountAlleles and/or CheckErrors on this data.");

		a1 = a;
		a2 = b;
	}

	public String toString()
	{
		return StringFormatter.format(a1,2)+" "+ StringFormatter.format(a2,2);
	}

	public LinkagePhenotype nullCopy()
	{
		return new NumberedAllelePhenotype((NumberedAlleleLocus)getLocus());
	}

	public boolean informative()
	{
		return a1 > 0 && a2 > 0;
	}

	public void setUninformative()
	{
		a1 = 0;
		a2 = 0;
	}

	public int getAllele(int k)
	{
		return k == 0 ? a1-1 : a2-1;
	}

	public boolean setAlleles(int b0, int b1)
	{
		a1 = b0+1;
		a2 = b1+1;
		return true;
	}

	public int getAffectedStatus()
	{
		return -1;
	}

	public int getLiability()
	{
		return -1;
	}
	
	public void reCode(int[] c)
	{
		a1 = c[a1];
		a2 = c[a2];
	}

	public double[][] penetrance()
	{
		return ((NumberedAlleleLocus)getLocus()).penetrance(a1-1,a2-1);
	}

// Private data.

	protected int a1 = 0;
	protected int a2 = 0;
}
