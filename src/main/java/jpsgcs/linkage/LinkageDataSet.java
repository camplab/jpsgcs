package jpsgcs.linkage;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

/**
 This class holds the information from a standard Linkage parameter data file
 and a standard Linkage pedigree file.
*/
public class LinkageDataSet
{
	public LinkageDataSet()
	{
	}

	public LinkageDataSet(String datain, String pedin) throws IOException
	{
		this(datain, pedin, false);
	}

        public LinkageDataSet(LinkageFormatter parin, LinkageFormatter pedin) throws IOException
        {
		parfile = parin.fileName();
                LinkageParameterData pardat = new LinkageParameterData(parin);
		pedfile = pedin.fileName();
                LinkagePedigreeData peddat = new LinkagePedigreeData(pedin,pardat);
                set(pardat,peddat);
        }

/**
 Creates a new LinkageDataSet with only the loci indicated in the given array of indexes.
*/
	public LinkageDataSet(LinkageDataSet l, int[] x)
	{
		set(new LinkageParameterData(l.getParameterData(),x), new LinkagePedigreeData(l.getPedigreeData(),x));
	}

/**
 Creates a new LinkageDatSet from the parameter data and the pedigree data
 in the files whose names are given in the constructor.
*/
	public LinkageDataSet(String datain, String pedin, boolean premake) throws IOException
	{
		parfile = datain;
		LinkageFormatter f = new LinkageFormatter(new BufferedReader(new FileReader(parfile)),"Par file");
		LinkageParameterData lpd = new LinkageParameterData(f);
		
		pedfile = pedin;
		f = new LinkageFormatter(new BufferedReader(new FileReader(pedfile)),"Ped file");
		LinkagePedigreeData ldd = new LinkagePedigreeData(f,lpd,premake);
		set(lpd,ldd);
	}

	public LinkageDataSet(LinkageParameterData parin, LinkagePedigreeData pedin)
	{
		set(parin,pedin);
	}

	public void set(LinkageParameterData parin, LinkagePedigreeData pedin)
	{
		par = parin;
		ped = pedin;
		name = ( parfile == null || pedfile == null ? "" : parfile+":"+pedfile );
	}

/**
 Returns the object containting the data from the linkage .par file.
*/
	public LinkageParameterData getParameterData()
	{
		return par;
	}

/**
 Returns the object containing the data from the linkage .ped file.
*/
	public LinkagePedigreeData getPedigreeData()
	{
		return ped;
	}

/**
 Returns a string representation of the data contained in the data set.
*/
	public String toString()
	{
		return parfile + ":\n" + par + "\n" + pedfile + ":\n" + ped;
	}

	public String name()
	{
		return name;
	}

	public LinkageDataSet[] splitByPedigree()
	{
		LinkagePedigreeData[] lpd = getPedigreeData().splitByPedigree();
		LinkageDataSet[] ld = new LinkageDataSet[lpd.length];
		for (int i=0; i<ld.length; i++)
		{
			ld[i] = new LinkageDataSet(new LinkageParameterData(getParameterData()),lpd[i]);
			//int x = lpd[i].getIndividuals()[0].pedid;
			ld[i].name = ""+lpd[i].getIndividuals()[0].pedid;
		}

		return ld;
	}

	public void countAlleleFreqs()
	{
		LinkageIndividual[] ind = ped.getIndividuals();
		for (int j=0; j<par.nLoci(); j++)
			par.getLocus(j).countAlleleFreqs(ind, j);
	}

	public void downCode(boolean hard)
	{
		downCode(hard ? 2 : 0);
	}
	
	public void downCode(int hard)
	{
		// hard == 2 ==> Hard downcode: only observed alleles kept.
		// hard == 0 ==> Soft downcode: observed alleles plus one catchall kept.
		// hard == 1 ==> Like Hard downcode, but loci with one observed allele treated as in Soft.

		LinkageIndividual[] ind = ped.getIndividuals();
		for (int i=0; i<par.nLoci(); i++)
			par.getLocus(i).downCode(ind,i,hard);
	}

	public int[] downCode(int i, int hard)
	{
		return par.getLocus(i).downCode(ped.getIndividuals(),i,hard);
	}

	public LinkageParameterData getParData() {
		return par;
	}
	
	public LinkagePedigreeData getPedData() {
		return ped;
	}

/*
	public static void downCode(LinkageParameterData par, LinkageIndividual[] ind, int hard)
	{
		for (int i=0; i<par.nLoci(); i++)
			par.getLocus(i).downCode(ind,i,hard);
	}
*/

// Private data.

	protected String parfile = null;
	protected String pedfile = null;
	protected String name = null;
	protected LinkageParameterData par = null;
	protected LinkagePedigreeData ped = null;

/**
 Main reads a Linkage data file and a Linkage pedigree file and writes
 the output to standard output.
*/
	public static void main(String[] args)
	{
		try
		{

			switch(args.length)
			{
			case 2:
				LinkageDataSet l = new LinkageDataSet(args[0],args[1]);
				System.out.println(l);
				break;
			default:
				System.err.println("Specify Linkage parameter file and Linkage pedigree file.");
				System.exit(0);
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in LinkageDataSet:main()");
			e.printStackTrace();
		}
	}
}
