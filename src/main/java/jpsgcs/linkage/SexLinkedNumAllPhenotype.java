package jpsgcs.linkage;

public class SexLinkedNumAllPhenotype extends NumberedAllelePhenotype implements LinkConstants
{
	public SexLinkedNumAllPhenotype(NumberedAlleleLocus l, int s) 
	{
		super(l);
		sex = s;
		a1 = ( sex == MALE ? 1 : 0 ); 
	}

	public void read(LinkageFormatter f)
	{
		if (sex != MALE)
		{
			super.read(f);
			if (a1 > 0) 
				a1 += 1;
			if (a2 > 0)
				a2 += 1;
		}
		else
		{
			int b = f.readInt("allele code",0,true,false);
			int c = f.readInt("allele code",0,true,false);
			if (c != b)
				f.warn(getLocus().locName()+": allele code "+b+" and "+c+" do not match for a male's sex linked genotype.\n\t"+
						"Using the first allele: "+b);

			double[] freq = getLocus().alleleFrequencies();
	
			if (b < 0 || b > freq.length)
			{
				if (b < 0 || b > freq.length)
					f.warn(getLocus().locName()+": allele code "+b+" is out of range "+0+" "+(freq.length)+"\n\tSetting genotype to 0 0");
				b = 0;
			}
	
			if (b > 0 && freq[b-1] <= 0)
				f.warn(getLocus().locName()+": allele code "+b+" has zero probability" +"\n\tRun GeneCountAlleles and/or CheckErrors on this data.");
	
			a2 = b;
			
			if (a2 > 0)
				a2 += 1;
		}
	}

	public String toString()
	{
		int b2 = a2;
		if (b2 > 0)
			b2--;

		if (sex == MALE)
		{
			return f.format(b2,2)+" "+f.format(b2,2);
		}
		else
		{
			int b1 = a1;
			if (b1 > 0)
				b1--;
			return f.format(b1,2)+" "+f.format(b2,2);
		}
	}

	public LinkagePhenotype nullCopy()
	{
		return new SexLinkedNumAllPhenotype((SexLinkedNumAllLocus)getLocus(),sex);
	}

	public void setUninformative()
	{
		a1 = ( sex == MALE ? 1 : 0);
		a2 = 0;
	}

	public boolean setAlleles(int b0, int b1)
	{
		a2 = b1+1;
		if (sex != MALE)
			a1 = b0+1;
		return true;
	}

	public double[][] penetrance()
	{
		return ((SexLinkedNumAllLocus)getLocus()).penetrance(a1-1,a2-1,sex);
	}

// Private data.

	private int sex = 0;
}
