package jpsgcs.linkage;

import java.io.IOException;

/**
 This is the base class from which the linkage locus types are
 derived.
 Only the numbered alleles type of locus is currently properly 
 implemented.
*/

abstract public class LinkageLocus 
{
	public final static int QUANTITATIVE_VARIABLE = 0;
	public final static int AFFECTION_STATUS = 1;
	public final static int BINARY_FACTORS = 2;
	public final static int NUMBERED_ALLELES = 3;

	abstract public LinkagePhenotype makePhenotype(LinkageIndividual a);

	abstract public void read(LinkageFormatter f) throws IOException;

	abstract public LinkageLocus copy();

	abstract public int[] downCode(LinkageIndividual[] ind, int j, int hard);

	abstract public void countAlleleFreqs(LinkageIndividual[] ind, int j);

	abstract public void reCode(int[] c);

	abstract public String toString();

// Static methods.
// These should be the only methods in the whole package that know about 
// the different sorts of LINKAGE loci and phenotypes.

	public static LinkageLocus readLocus(LinkageFormatter b, int sexlinked) throws IOException
	{
		LinkageLocus l = null;
		int type = b.readInt("locus code",0,true,true);

		// To make a new LINKAGE input locus type, derive from LinkageLocus, LinkagePhenotype
		// and change the following switch statement.

		if (sexlinked == 0)
		{
			switch(type)
			{
			case AFFECTION_STATUS:
				l = new AffectionStatusLocus();
				break;
	
			case NUMBERED_ALLELES:
				l = new NumberedAlleleLocus();
				break;
	
			case QUANTITATIVE_VARIABLE:
				l = new QuantitativeLocus();
				break;
	
			case BINARY_FACTORS:
			default:
				b.crash("LINKAGE locus code "+type+" is not yet implemented in these programs.");
			}
		}
		else
		{
			switch(type)
			{
			case NUMBERED_ALLELES:
				l = new SexLinkedNumAllLocus();
				break;

			case AFFECTION_STATUS:
				l = new SexLinkedAffStatLocus();
				break;

			case QUANTITATIVE_VARIABLE:
			case BINARY_FACTORS:
			default:
				b.crash("LINKAGE locus code "+type+" for XY chromosomes is not yet implemented in these programs.");
			}
		}

		l.read(b);
		return l;
	}

	public static LinkageLocus fullyInformativeLocus(int na, boolean sexlinked)
	{
		if (sexlinked)
		{
			LinkageLocus full = new SexLinkedNumAllLocus(na);
			full.line1comment = "Sex linked ancestral origin locus";
			return full;
		}
		else
		{
			LinkageLocus full = new NumberedAlleleLocus(na);
			full.line1comment = "Ancestral origin locus";
			return full;
		}
	}
	
// Not static methods.

	public LinkagePhenotype readPhenotype(LinkageFormatter f, LinkageIndividual a)
	{
		LinkagePhenotype p = makePhenotype(a);
		p.read(f);
		return p;
	}

/**
 Returns the number of alleles that this locus has.
*/
	public int nAlleles()
	{
		return freq.length;
	}

/**
 Returns the allele frequencies for the locus.
*/
	public double[] alleleFrequencies()
	{
		return freq;
	}

	public void setAlleleFrequencies(double[] f)
	{
		freq = f;
	}

	public void checkAndSetAlleleFrequencies(double[] f, LinkageFormatter b)
	{
		double tot = 0;
		for (int i=0; i<f.length; i++)
			tot += f[i];

		if (tot <= 0)
		{
			b.warn("Allele frequencies sum to zero. Setting to uniform");
			for (int i=0; i<f.length; i++)
				f[i] = 1.0/f.length;
		}
			
		freq = f;
	}

/**
 Returns the comment string from the first line of input
 associated with this locus.
*/
	public String firstComment()
	{
		return line1comment;
	}

	public String locName()
	{
		StringBuffer b = new StringBuffer(line1comment);
		if (b.length() > 0)
		{
			while (b.charAt(0) == ' ' || b.charAt(0) == '\t')
				b.deleteCharAt(0);
			while (b.charAt(b.length()-1) == ' ')
				b.deleteCharAt(b.length()-1);
		}
		return b.toString();
	}

	public int getType()
	{
		return type;
	}

// Protected data.

	public int type = 0;
	public double[] freq = null;
	public String line1comment = null;
}
