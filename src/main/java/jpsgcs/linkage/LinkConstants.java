package jpsgcs.linkage;

public interface LinkConstants
{
	public final static int STANDARD = 0;
	public final static int PREMAKE = 1;
	public final static int TRIPLET = 2;

	public final static int PEDONLY = 3;
	public final static int MALE = 1;
	public final static int FEMALE = 2;

	public final static int UNKNOWN = 0;
	public final static int AFFECTED = 2;
	public final static int UNAFFECTED = 1;
}
