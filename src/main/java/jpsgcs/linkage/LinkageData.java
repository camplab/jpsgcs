package jpsgcs.linkage;

import jpsgcs.genio.Family;
import jpsgcs.genio.BasicGeneticData;
import jpsgcs.util.Triplet;
import jpsgcs.util.IntArray;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.LinkedHashSet;
import java.io.PrintStream;
import java.io.*;

/**
	Trial class to replace LinakgeDataSet and LinkageInterface
	with a simpler single class.
*/

public class LinkageData implements BasicGeneticData, LinkConstants
{
	public LinkageData(String parfile, String pedfile, boolean premake) throws IOException
	{
		LinkageFormatter f = new LinkageFormatter(new BufferedReader(new FileReader(parfile)),"Par file");
		par = new LinkageParameterData(f);
		
		f = new LinkageFormatter(new BufferedReader(new FileReader(pedfile)),"Ped file");
		ped = new LinkagePedigreeData(f,par,premake);
	}

	public LinkageData(String datain, String pedin) throws IOException
	{
		this(datain, pedin, false);
	}

	public void countAlleleFreqs()
	{
		LinkageIndividual[] ind = ped.getIndividuals();
		for (int j=0; j<par.nLoci(); j++)
			par.getLocus(j).countAlleleFreqs(ind, j);
	}

	public void downCode(boolean hard)
	{
		downCode(hard ? 2 : 0);
	}
	
	public void downCode(int hard)
	{
		// hard == 2 ==> Hard downcode: only observed alleles kept.
		// hard == 0 ==> Soft downcode: observed alleles plus one catchall kept.
		// hard == 1 ==> Like Hard downcode, but loci with one observed allele treated as in Soft.

		LinkageIndividual[] ind = ped.getIndividuals();
		for (int i=0; i<par.nLoci(); i++)
			par.getLocus(i).downCode(ind,i,hard);
	}

	public int[] downCode(int i, int hard)
	{
		return par.getLocus(i).downCode(ped.getIndividuals(),i,hard);
	}


// BasicGeneticData interface.

	public int proband(int j)
	{
		LinkageIndividual x = ped.getIndividuals()[j];
		return x.proband;
	}

	public int nProbands()
	{
		int n = 0;
		LinkageIndividual[] x = ped.getIndividuals();
		for (int i=0; i<x.length; i++)
			if (x[i].proband == 1)
				n++;
		return n;
	}

	public int getAllele(int i, int j, int k)
	{
		return ped.getIndividuals()[j].getPhenotype(i).getAllele(k);
	}

	public boolean setAlleles(int i, int j, int b0, int b1)
	{
		return ped.getIndividuals()[j].getPhenotype(i).setAlleles(b0,b1);
	}

	public void downcodeAlleles()
	{
		downCode(false);
	}

	public double[][] penetrance(int i, int j)
	{
		LinkagePhenotype pheno = ped.getIndividuals()[j].getPhenotype(i);
		if (pheno != null)
			return pheno.penetrance();
		return null;
	}

	public boolean sexLinked()
	{
		return par.isSexLinked();
	}

// ParameterData interface

	public String locusName(int i)
	{
		return par.getLoci()[i].locName();
	}

	public int nLoci()
	{
		if (par == null)
			return 0; 
		return par.nLoci();
	}

	public int nAlleles(int i)
	{
		return alleleFreqs(i).length;
	}

	public double[] alleleFreqs(int i)
	{
		LinkageLocus[] locs = par.getLoci();
		LinkageLocus ll = locs[i];
		return ll.alleleFrequencies();
	}

	public double getFemaleRecomFrac(int i, int j)
	{
		return par.femaleTheta(i,j);
	}

	public double getMaleRecomFrac(int i, int j)
	{
		return par.maleTheta(i,j);
	}

// PedigreeData interface.
    public double getKinshipCoeff(int i, int j) {
        throw new UnsupportedOperationException("Sorry, getKinshipCoeff() is not implemented for LinkageData, try LinkageDataSet or LinkageInterface.");
    }
    public int[] getRank() {
        throw new UnsupportedOperationException("Sorry, getRank() is not implemented for LinkageData, try LinkageDataSet or LinkageInterface.");
    }
	
	public String pedigreeName(int j)
	{
		LinkageIndividual x = ped.getIndividuals()[j];
		return x.pedid+"";
	}

	public String individualName(int j)
	{
		LinkageIndividual x = ped.getIndividuals()[j];
		return ""+x.id;
	}

	public boolean isMale(int j)
	{
		LinkageIndividual x = ped.getIndividuals()[j];
		return x.sex == MALE;
	}

	public boolean isFemale(int j)
	{
		LinkageIndividual x = ped.getIndividuals()[j];
		return x.sex == FEMALE;
	}

	public int nIndividuals()
	{
		return ped.getIndividuals().length;
	}

	public int pa(int j)
	{
		LinkageIndividual x = ped.getIndividuals()[j];
		x = (LinkageIndividual)ped.getPedigree().getTriplet(x).y;
		return x == null ? -1 : x.index;
	}

	public int ma(int j)
	{
		LinkageIndividual x = ped.getIndividuals()[j];
		x = (LinkageIndividual)ped.getPedigree().getTriplet(x).z;
		return x == null ? -1 : x.index;
	}

	public int[] kids(int j)
	{
		LinkageIndividual x = ped.getIndividuals()[j];
		Object[] kk = ped.getPedigree().kids(x);
		int[] kkk = new int[kk.length];
		for (int i=0; i<kkk.length; i++)
			kkk[i] = ((LinkageIndividual)kk[i]).index;
		return kkk;
	}

	public int[][] nuclearFamilies()
	{
		Family[] f = ped.getPedigree().nuclearFamilies();
		int[][] n = new int[f.length][];
		for (int i=0; i<n.length; i++)
		{
			LinkageIndividual pa = (LinkageIndividual) f[i].getPa();
			LinkageIndividual ma = (LinkageIndividual) f[i].getMa();
			Object[] k = f[i].getKids();
			n[i] = new int[k.length+2];
			n[i][0] = pa == null ? -1 : pa.index;
			n[i][1] = ma == null ? -1 : ma.index;
			for (int j=0; j<k.length; j++)
				n[i][j+2] =  ((LinkageIndividual)k[j]).index;
		}

		return n;
	}

	public void write(PrintStream pars, PrintStream peds)
	{
		write(pars,peds,false);
	}

	public void write(PrintStream pars, PrintStream peds, boolean premake)
	{
		par.writeTo(pars);
                ped.setOutputPremake(premake);
		ped.writeTo(peds);
	}

// Private data.

	protected LinkageParameterData par = null;
	protected LinkagePedigreeData ped = null;
}
