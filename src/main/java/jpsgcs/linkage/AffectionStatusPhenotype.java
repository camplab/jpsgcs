package jpsgcs.linkage;

import java.io.IOException;

/**
 This represents the data for an individual at a locus
 specified in the linkage affected status format.
*/
public class AffectionStatusPhenotype extends LinkagePhenotype implements LinkConstants
{
/**
 Creates a new phenotype from the given status and liability class numbers.
 The data is checked for consistency with the data for the specified locus.
*/

	public AffectionStatusPhenotype(AffectionStatusLocus l)
	{
		setLocus(l);
		status = 0;
		liability = 0;
	}

	public void read(LinkageFormatter f)
	{
		int s = f.readInt("disease status",0,true,false);

		switch(s)
		{
		case UNKNOWN:
		case UNAFFECTED:
		case AFFECTED:
			break;
		default:
			f.warn("disease status "+s+" is out of range "+0+" "+2+"\n\tSetting to 0");
			s = 0;
		}

		int l = 1;
		if (nLiab() > 1)
		{
			l = f.readInt("liability class",0,true,false);
			if (s != 0)
			{
				if (l < 1 || l > nLiab())
				{
					f.warn("liability class "+l+" is out of range "+1+" "+nLiab()+
							"\n\tSetting disease status to 0 and libility class to 0");
					s = 0;
					l = 0;
				}
			}
			else
			{
				l = 0;
			}
		}

		status = s;
		liability = l;
	}

	public String toString()
	{
		if (nLiab() > 1)
			return f.format(status,2)+" "+f.format(liability,2);
		else
			return f.format(status,2);
	}

	public LinkagePhenotype nullCopy()
	{
		return new AffectionStatusPhenotype((AffectionStatusLocus)getLocus());
	}

	public boolean informative()
	{
		return status > 0;
	}

	public void setUninformative()
	{
		status = 0;
		liability = 0;
	}

	public int getAllele(int k)
	{
		return -1;
	}

	public boolean setAlleles(int a1, int a2)
	{
		return false;
	}

	public int getAffectedStatus()
	{
		return status;
	}
	
	public int getLiability()
	{
		return liability;
	}

	public void reCode(int[] c)
	{
		return;
	}

	public double[][] penetrance()
	{
		return ((AffectionStatusLocus) getLocus()).penetrance(status,liability);
	}

// Private data.

	protected int status = 0;
	protected int liability = 0;

	protected int nLiab()
	{
		return ((AffectionStatusLocus) getLocus()).nLiab();
	}
}
