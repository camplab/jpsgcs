package jpsgcs.genio;

public interface PedigreeData {
    public String pedigreeName(int i);

    public String individualName(int i);

    public boolean isMale(int i);

    public boolean isFemale(int i);

    public int[] getRank();

    public double getKinshipCoeff(int i, int j);

    /**
     * Returns the number of individuals who are being considered. Individuals
     * will be accessed by integers between 0 and nIndiviuals()-1 so repeated
     * calls to the methods below must give consistent responses.
     */
    public int nIndividuals();

    /**
     * Returns the position in the list of individuals of the father of the
     * individual in the jth position in the list. If father is not in the list
     * -1 should be returned.
     */
    public int pa(int j);

    /**
     * Returns the position in the list of individuals of the mother of the
     * individual in the jth position in the list. If mother is not in the list
     * -1 should be returned.
     */
    public int ma(int j);

    /**
     * Returns the positions in the list of individuals of the children of the
     * individual in the jth position in the list. If there are no offspring an
     * array of length zero is returned.
     */
    public int[] kids(int j);

    /**
     * Returns a matrix of integers indexing the positions of a nuclear family.
     * One row per family. The first element in the row is the father, the
     * second the mother and the remainder are the indexes of the children.
     **/
    public int[][] nuclearFamilies();
}
