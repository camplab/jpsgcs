package jpsgcs.genio;

import jpsgcs.util.ArgParser;
import jpsgcs.linkage.LinkageData;
import jpsgcs.plink.PlinkData;
import java.io.PrintStream;
import java.io.IOException;
import java.io.FileNotFoundException;

public class Genio
{
	public static void main(String[] args)
	{
		try
		{
			ArgParser a = new ArgParser(args);
			BasicGeneticData data = Genio.read(a);
			Genio.write(data,a);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static final int LINKAGE = 0;
	public static final int PREMAKE = 1;
	public static final int PLINK = 2;
	public static final int FASTPHASE = 3;
	public static final int PHASE = 4;

	public static boolean write(BasicGeneticData d, ArgParser a) throws FileNotFoundException
	{
		int opt = LINKAGE;

		if (a.gotOpt("-outphase"))
			opt = PHASE;
		if (a.gotOpt("-outfastphase"))
			opt = FASTPHASE;
		if (a.gotOpt("-outplink"))
			opt = PLINK;
		if (a.gotOpt("-outpremake"))
			opt = PREMAKE;
		if (a.gotOpt("-outlink"))
			opt = LINKAGE;

		switch(opt)
		{
		case FASTPHASE:
			writeAsFastPhase(d,new PrintStream(a.arg(0)));
			a.remove(0);
			return true;

		case PHASE:
			writeAsPhase(d,new PrintStream(a.arg(0)));
			a.remove(0);
			return true;
	
		case PREMAKE:
			if (d instanceof LinkageData)
			{
				((LinkageData) d).write(new PrintStream(a.arg(0)),new PrintStream(a.arg(1)),true);
				a.remove(0,1);
				return true;
			}
			else if (d instanceof PlinkData)
			{
				((PlinkData) d).writeLinkageFiles(new PrintStream(a.arg(0)),new PrintStream(a.arg(1)),true);
				a.remove(0,1);
				return true;
			}
			else
			{
				throw new RuntimeException("Can only write LinkageData and PlinkData objects as linkage premake files.");
			}

		case LINKAGE:
			if (d instanceof LinkageData)
			{
				((LinkageData) d).write(new PrintStream(a.arg(0)),new PrintStream(a.arg(1)));
				a.remove(0,1);
				return true;
			}
			else if (d instanceof PlinkData)
			{
				((PlinkData) d).writeLinkageFiles(new PrintStream(a.arg(0)),new PrintStream(a.arg(1)));
				a.remove(0,1);
				return true;
			}
			else
			{
				throw new RuntimeException("Can only write LinkageData and PlinkData objects as linkage premake files.");
			}

		case PLINK:
			if (d instanceof PlinkData)
			{
				((PlinkData) d).write(new PrintStream(a.arg(0)),new PrintStream(a.arg(1)),new PrintStream(a.arg(2)));
				a.remove(0,1,2);
				return true;
			} 
			else
			{
				throw new RuntimeException("Can only write PlinkData objects in Plink format.");
			}

		default:
			throw new RuntimeException("Output file format option not recognized.");
		}
	}

	public static BasicGeneticData read(ArgParser a) throws IOException
	{
		int opt = LINKAGE;

		if (a.gotOpt("-phase"))
			opt = PHASE;
		if (a.gotOpt("-fastphase"))
			opt = FASTPHASE;
		if (a.gotOpt("-plink"))
			opt = PLINK;
		if (a.gotOpt("-premake"))
			opt = PREMAKE;
		if (a.gotOpt("-link"))
			opt = LINKAGE;

		BasicGeneticData x = null;

		switch(opt)
		{
		case FASTPHASE:
			throw new RuntimeException("Cannot read FastPhase files.");

		case PHASE:
			throw new RuntimeException("Cannot read Phase files.");

		case PLINK:
			x = new PlinkData(a.arg(0),a.arg(1),a.arg(2));
			a.remove(0,1,2);
			break;

		case PREMAKE:
			x = new LinkageData(a.arg(0),a.arg(1),true);
			a.remove(0,1);
			break;

		case LINKAGE:
			x = new LinkageData(a.arg(0),a.arg(1),false);
			a.remove(0,1);
			break;
		} 

		return x;
	}

	public static void writeAsPhase(BasicGeneticData x, PrintStream s)
	{
		s.println(x.nIndividuals());
		s.println(x.nLoci());

		for (int i=0; i<x.nLoci(); i++)
			s.print(x.nAlleles(i) == 2 ? "S" : "M");
		s.println();

		for (int i=0; i<x.nIndividuals(); i++)
		{
			s.println("#"+i);
			for (int k=0; k<2; k++)
			{
				for (int j=0; j<x.nLoci(); j++)
				{
					s.print(" ");
					if (x.getAllele(j,i,k) == -1)
						s.print(x.nAlleles(j) == 2 ? "?" : "-1");
					else
						s.print(x.getAllele(j,i,k));
				}
				s.println();
			}
		}
	}

	public static void writeAsFastPhase(BasicGeneticData x, PrintStream s)
	{
		s.println(x.nIndividuals());
		s.println(x.nLoci());

		for (int i=0; i<x.nLoci(); i++)
		{
			if (x.nAlleles(i) != 2)
				System.err.println("Warning: Locus "+i+" is not diallelic. Allele numbers > 1 will be set to 1");
			s.print("S");
		}
		s.println();

		for (int i=0; i<x.nIndividuals(); i++)
		{
			s.println("# id "+i);
			for (int k=0; k<2; k++)
			{
				for (int j=0; j<x.nLoci(); j++)
				{
					int allele = x.getAllele(j,i,k);
					s.print(" ");
					s.print( allele == -1 ? "?" : (allele > 1 ? 1 : allele)); 
				}
				s.println();
			}
		}
	}
}
