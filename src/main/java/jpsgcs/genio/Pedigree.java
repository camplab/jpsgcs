package jpsgcs.genio;

import jpsgcs.util.Triplet;
import jpsgcs.graph.Network;
import java.util.Set;
import java.util.Map;
import java.util.LinkedHashSet;
import java.util.LinkedHashMap;
import java.util.Collection;

public class Pedigree
{
	public Pedigree()
	{
		g = new Network<Object,Set<Object>>();
		tripletMap = new LinkedHashMap<Object,Triplet>();
		males = new LinkedHashSet<Object>();
		females = new LinkedHashSet<Object>();
	}

	public void addTriplet(Triplet t)
	{
		Set<Object> s = g.connection(t.y,t.z);
		if (t.y != null || t.z != null)
		{
			if (s == null)
			{
				s = new LinkedHashSet<Object>();
				g.connect(t.y,t.z,s);
			}
			s.add(t.x);
		}
		tripletMap.put(t.x, t);
		males.add(t.y);
		females.add(t.z);
	}

	public Object pa(Object x)
	{
		Triplet t = tripletMap.get(x);
		return t == null ? null : t.y;
	}
	
	public Object ma(Object x)
	{
		Triplet t = tripletMap.get(x);
		return t == null ? null : t.z;
	}
	
	public void addTriplets(Collection<Triplet> t)
	{
		for (Triplet x : t)
			addTriplet(x);
	}

	public Triplet getTriplet(Object x)
	{
		return tripletMap.get(x);
	}

	public Object[] kids(Object x)
	{
		Collection ss = g.getNeighbours(x);
		if (ss == null)
		{
			Object[] result = {};
			return result;
		}

		Set<Object> k = new LinkedHashSet<Object>();
		for (Object i : ss)
			k.addAll(g.connection(x,i));
		return k.toArray();
	}

	public Family[] nuclearFamilies()
	{
		Network<Object,Set<Object>> n = new Network<Object,Set<Object>>();
		for (Object u : g.getVertices())
		{
			n.add(u);
			for (Object q : g.getNeighbours(u))
				n.connect(u,q,g.connection(u,q));
		}

		Set<Family> f = new LinkedHashSet<Family>();

		for (Object u : g.getVertices())
		{
			if (males.contains(u))
			{
				for (Object v : n.getNeighbours(u))
				{
					Family ff = new Family();
					ff.setPa(u);
					ff.setMa(v);
					for (Object w : n.connection(u,v))
						ff.addKid(w);
					f.add(ff);
				}
				n.remove(u);
			}
		}

		return (Family[]) f.toArray(new Family[0]);
	}

	public Collection<Object> individuals()
	{
		return tripletMap.keySet();
	}

// Private data.

	private Network<Object,Set<Object>> g = null;
	private Map<Object,Triplet> tripletMap = null;
	private Set<Object> males = null;
	private Set<Object> females = null;
}
