package jpsgcs.genio;

import java.io.PrintStream;

public interface GeneticDataSource extends BasicGeneticData
{
// THESE THREE FIRST METHODS HAVE BEEN PROMOTED TO BasicGeneticData
/**
 Returns the proband status of the jth individual in the list.
*/
//	public int proband(int j);

/**
 Returns the number of individuals whose proband status > 0.
*/
//	public int nProbands();

/**
 This returns the first or second allele at the ith locus for the jth individual,
 depending on whether k is 0 or 1.
 This is only really possible when the locus is codominant. 
 In other cases this should return a negative value.
*/
//	public int getAllele(int i, int j, int k);

/**
 This sets the alleles at the ith locus for the jth individual to a0 and a1.
 This is only really possible when the locus is codominant, in other cases
 no action should be taken. If the setting was possible true should be 
 returned, otherwise false.
*/
//	public boolean setAlleles(int i, int j, int a0, int a1);

	public void writeIndividual(int i, PrintStream p);

	public void writePedigree(PrintStream p);

	public void writeParameters(PrintStream p);

//	public void downcodeAlleles();
}
