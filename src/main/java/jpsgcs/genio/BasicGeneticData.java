package jpsgcs.genio;

public interface BasicGeneticData extends ParameterData, PedigreeData
{
/**
 Returns the penetrance function at the ith locus for the jth person.
 This is a matrix of dimension nAlleles(j) x nAlleles(j) where the
 entry at row a, column b is the probability of the individual's 
 observed phenotype given that they have allele number a on the
 paternal chromosome and allele number b on the maternal chromosome.
 Again, phenotype is used in a very general way so this function is
 also used to express genotypic information. If an individual j
 is observed with genotype (x,y) at locus i then 
	penetrance(i,j)[x,y] = penetrance(i,j)[y,x] = 1
 with zeros elsewhere.
 If the locus is not perfectly co-dominant, or there is error
 in the observation, this should be expressed appropriately in this
 matrix.
 If no information is present for the jth individual at the ith locus
 this method can return null.
*/
	public double[][] penetrance(int i, int j);

/**
 Returns the proband status of the jth individual in the list.
*/
	public int proband(int j);

/**
 Returns the number of individuals whose proband status > 0.
*/
	public int nProbands();

/**
 This returns the first or second allele at the ith locus for the jth individual,
 depending on whether k is 0 or 1.
 This is only really possible when the locus is codominant. 
 In other cases this should return a negative value.
*/
	public int getAllele(int i, int j, int k);

	public boolean setAlleles(int i, int j, int a0, int a1);

	public void downcodeAlleles();
}
