package jpsgcs.viewgraph;

import jpsgcs.markov.Variable;
import jpsgcs.markov.Parameter;
import jpsgcs.markov.Product;
import jpsgcs.markov.Function;
import jpsgcs.graph.Coordinated;
import jpsgcs.graph.Network;
import jpsgcs.graph.GridLocator;
import jpsgcs.util.InputFormatter;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Map;
import java.util.LinkedHashMap;
import java.io.IOException;

public class Ising
{
    public static int[][] readImage() throws IOException
    {
        InputFormatter f = new InputFormatter();
                
        f.newLine();
        int[][] z = new int[f.nextInt()][f.nextInt()];

        for (int i=0; i<z.length; i++)
            {
                f.newLine();
                for (int j=0; j<z[i].length; j++)
                    {
                        z[i][j] = f.nextInt();
                    }
            }

        return z;
    }


    public static void main(String[] args)
    {
        try
            {
                boolean gibbs = true;
                int pixSize = 1;

                switch(args.length)
                    {
                    case 1: gibbs = args[0].equals("-g");
                    }

                Parameter[] p = new Parameter[5];
                p[0] = new Parameter("Modelled noise",0,1,0.5);
                p[1] = new Parameter("Correlation",0,1,0.5);
                p[2] = new Parameter("True noise",0,1,0);
                p[3] = new Parameter("Speed",0,1000,1);
                p[4] = new Parameter("Temperature",0,10,1);
        
                int [][] z = readImage();
                Pixel[][] x = new Pixel[z.length][z[0].length];
                Pixel[][] y = new Pixel[z.length][z[0].length];

                for (int i=0; i<x.length; i++)
                    for (int j=0; j<x[i].length; j++)
                        {
                            x[i][j] = new Pixel(i,j);
                            y[i][j] = new Pixel(i,j);
                            x[i][j].setState(Math.random() > 0.5 ? 1 : 0);
                        }
        

                setY(y,z,0);

                GraphFrame<Pixel,Object> pix = new PixelFrame(x,p,pixSize,pixSize);
                try {Thread.sleep(1000);} catch (Exception e) {}
                        
                Product prod = new Product();
        
                for (int i=0; i<x.length; i++)
                    for (int j=0; j<x[i].length; j++)
                        {
                            prod.add(new PixelCorrespondence(x[i][j],y[i][j],p[0]));
                                        
                            if (i > 0)
                                prod.add(new PixelCorrespondence(x[i-1][j],x[i][j],p[1]));
        
                            if (j > 0)
                                prod.add(new PixelCorrespondence(x[i][j-1],x[i][j],p[1]));
                        }
        
        
                while(true)
                    {
                        Thread.sleep((int)(1000 - p[3].getValue()));

                        if (p[2].changed())
                            setY(y,z,p[2].getValue());

                        for (int i=0; i<x.length; i++)
                            for (int j=0; j<x[i].length; j++)
                                {
                                    double a = Math.exp(prod.logValue(x[i][j]));
                                    x[i][j].setState(1-x[i][j].getState());
                                    double b = Math.exp(prod.logValue(x[i][j]));
        
                                    if (gibbs)
                                        {
                                            if (p[4].getValue() * Math.log(Math.random()) > Math.log(b/(a+b)))
                                                x[i][j].setState(1-x[i][j].getState());
                                        }
                                    else
                                        {
                                            if (p[4].getValue() * Math.log(Math.random()) > Math.log(b/a))
                                                x[i][j].setState(1-x[i][j].getState());
                                        }
                                }
                    }
            } 
        catch (Exception e) {}
    }

    public static void setY(Pixel[][] y, int[][] z, double p)
    {
        for (int i=0; i<y.length; i++)
            for (int j=0; j<y[i].length; j++)
                {
                    y[i][j].setState(z[i][j]);

                    if (Math.random() < p)
                        y[i][j].setState(1-y[i][j].getState());
                }
    }
}

class PixelFrame extends GraphFrame<Pixel,Object>
{
    public PixelFrame(Pixel[][] pix, Parameter[] p, int x, int y)
        {
            super(paintPixels(pix,x,y),new GridLocator<Pixel,Object>(2*x,2*y),p);
        }

    private static PaintableGraph<Pixel,Object> paintPixels(Pixel[][] pix, int x, int y)
        {
            Network<Pixel,Object> g = new Network<Pixel,Object>();

            Map<Pixel,VertexRepresentation> map = new LinkedHashMap<Pixel,VertexRepresentation>();

            for (int i=0; i<pix.length; i++)
                for (int j=0; j<pix[i].length; j++)
                    {
                        g.add(pix[i][j]);
                        map.put(pix[i][j],new PixelBlob(pix[i][j],x,y));
                    }

            return new PaintableGraph<Pixel,Object>(g,map);
        }
}

class PixelBlob extends Blob
{
    public PixelBlob(Pixel p, int w, int h)
    {
        pix = p;
        setSize(w,h);
    }

    public void paint(Graphics g, double dx, double dy, boolean b)
    {
        int x = (int)(dx-w);
        int y = (int)(dy-h);
        g.setColor(pix.getState() == 0 ? Color.black : Color.red);
        g.fillRect( x, y, 2*w, 2*h );
    }

    private Pixel pix = null;
}

class Pixel extends Variable implements Coordinated
{
    public Pixel(int a, int b)
    {
        super(2);
        i = a;
        j = b;
    }

    public double x()
    {
        return j;
    }

    public double y()
    {
        return i;
    }

    private int i = 0;
    private int j = 0;
}

class PixelCorrespondence extends Function
{
    public PixelCorrespondence(Variable a, Variable b, Parameter q)
    {
        v = new Variable[2];
        v[0] = a;
        v[1] = b;
        p = new Parameter[1];
        p[0] = q;
    }

    public double getValue()
    {
        return v[0].getState() == v[1].getState() ? p[0].getValue() : 1-p[0].getValue();
    }

    public Variable[] getVariables()
    {
        return v;
    }

    public Parameter[] getParameters()
    {
        return p;
    }

    private Variable[] v = null;
    private Parameter[] p = null;
}
