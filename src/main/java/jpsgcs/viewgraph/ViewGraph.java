package jpsgcs.viewgraph;

import jpsgcs.graph.GraphLocator;
import jpsgcs.graph.LocalLocator;
import jpsgcs.animate.FrameQuitter;
import jpsgcs.graph.Network;
import java.awt.Panel;
import java.awt.Frame;
import java.util.StringTokenizer;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ViewGraph
{
    public static void main(String[] args)
    {
        try
            {
                Network<String,String> g = new Network<String,String>();
                        
                BufferedReader b = new BufferedReader(new InputStreamReader(System.in));
                for (String s = b.readLine(); s != null; s = b.readLine())
                    {
                        StringTokenizer t = new StringTokenizer(s);
                        if (t.hasMoreTokens())
                            {
                                String v = t.nextToken();
                                g.add(v);
                                while(t.hasMoreTokens())
                                    g.connect(v,t.nextToken());
                            }
                    }
                                        
                PaintableGraph<String,String> pg = new PaintableGraph<String,String>(g);
                GraphLocator<String,String> loc = new LocalLocator<String,String>();

                Panel p = new GraphPanel<String,String>(pg,loc);

                Frame f = new Frame();
                f.addWindowListener(new FrameQuitter());
                f.add(p);
                f.pack();
                f.setVisible(true);
            }
        catch (Exception e)
            {
                System.err.println("Caught in ViewGraph.main()");
                e.printStackTrace();
                System.exit(1);
            }
    }
}
