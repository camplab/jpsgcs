package jpsgcs.random;

/**
   This class provides replicates from Student's t distribution.
   The method used is to take the ratio of a Gaussian and a  
   Chi Squared.
*/
public class StudentsRV extends RandomVariable
{
    static { className = "StudentsRV"; }

    /**
       Creates a new Student's t random variable with 1 degree of freedom.
       This is the same as a Cauchy random variable.
    */
    public StudentsRV()
    {
        this(1);
    }

    /**
       Creates a new Student's t distribution with the given degrees of freedom.
    */
    public StudentsRV(double df)
    {
        set(df);
        X = new GaussianRV();
    }

    /**
       Sets the degrees of freedom parameter to the given value.
    */
    public void set(double df)
    {
        if (df <= 0)
            throw new ParameterException("Student's t degrees of freedom must be positive");
        Y = new ChiSquaredRV(df);
        d = 1.0/df;
    }

    /**
       Returns the next replicate.
    */
    public double next()
    {
        return X.next()/Math.sqrt(Y.next()*d);
    }

    private double d = 1;
    private RandomVariable X = null;
    private RandomVariable Y = null;
}
