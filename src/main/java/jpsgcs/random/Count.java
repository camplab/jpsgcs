package jpsgcs.random;

/**
   This class represents the state of counting processes.
*/
public class Count extends StochasticState
{
    /**
       This parameter counts the number of events that have occurred
       during the process up to the current time.
    */
    public int count = 0;

    /**
       Creates a new state with the given time and count.
    */
    public Count(double t, int c)
    {
        super(t);
        count = c;
    }

    /**
       Returns a string representation of the process.
    */
    public String toString()
    {
        return "{"+time+","+count+"}";
    }
}
