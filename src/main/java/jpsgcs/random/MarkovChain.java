package jpsgcs.random;

/**
   This is the superclass of the various ways of implementing 
   Markov chains.
*/
abstract public class MarkovChain extends DiscreteStochasticProcess
{
    /**
       Returns the current state.
       The runtime type of the state is MarkovState.
    */
    public StochasticState getState()
    {
        return new MarkovState(count,state);
    }

    /**
       Sets the current state.
       The state must be a MarkovState.
    */
    public void setState(StochasticState s)
    {
        MarkovState c = (MarkovState)s;
        count = c.count;
        time = count;
        state = c.state;
    }

    /**
       Advances the chain n steps.
    */
    public void next(int n)
    {
        for (int i=0; i<n; i++)
            next();
    }

    /**
       Advances the chain by t time units.
    */
    public void advance(double t)
    {
        time += t;
        int n = (int)(time-count);
        next(n);
    }

    protected double time = 0;
    protected int count = 0;
    protected int state = 0;
}
