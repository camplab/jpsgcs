package jpsgcs.random;

/**
   This interface defines what is required of a MarkovChain
   in order for it to use the Metropolis method.
*/
public interface Metropolitan
{
    /**
       Returns a proposed new state for the chain.
    */
    public int proposal();
    /**
       Returns the probability with which the given state should
       be accepted.
    */
    public double acceptance(int t);
}
