package jpsgcs.random;

/**
   This class delivers replicates from a von Mises distribution.
   The method used is rejection.
*/
public class VonMisesRV extends RandomVariable implements DensityFunction
{
    static { className = "VonMisesRV"; }

    /**
       Creates a new von Mises random variable with default parameter 1.
    */
    public VonMisesRV()
    {
        this(1);
    }

    /**
       Creates a new von Mises random variable with specified pararmeter value.
    */
    public VonMisesRV(double kappa)
    {
        set(kappa);
        Envelope X = new UniformRV(-Math.PI,Math.PI);
        setMethod(new Rejection(this,X,2*Math.PI));
    } 

    /**
       Sets the parameter to the given value.
    */
    public void set(double kappa)
    {
        if (kappa <= 0)
            throw new ParameterException("Von Mises kappa parameter must be positive");
        k = kappa;
    }

    /**
       Provides a function proportional to the density function as required by 
       the rejection method.
    */
    public double densityFunction(double x)
    {
        return x > -Math.PI && x <= Math.PI ? Math.exp(k*(Math.cos(x)-1)) : 0 ;
    }

    private double k = 1;
}
