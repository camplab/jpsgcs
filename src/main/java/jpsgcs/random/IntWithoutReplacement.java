package jpsgcs.random;

/**
   This class produces integers sampled without replacement from 0 to n-1.
*/
public class IntWithoutReplacement extends IntSampling
{
    static { className = "IntWithoutReplacement"; }

    /**
       Default constructor.
    */
    public IntWithoutReplacement()
    {
        this(10);
    }

    /**
       Constructs a new sampling scheme with limits of sampling from 
       0 to n-1 inclusive.
    */
    public IntWithoutReplacement(int n)
    {
        set(n);
    }

    /**
       Sets the limits of sampling from 0 to n-1 inclusive.
    */
    public void set(int n)
    {
        x = new int[n];
        for (int i=0; i<x.length; i++)
            x[i] = i;
        restart();
    }

    /**
       Replaces all sampled objects back in the population.
    */
    public void restart()
    {
        next = 0;
    }

    /**
       Returns the next sampled integer.
       If there are no more left to be sampled -1 is returned.
    */
    public int nextI()
    {
        if (next >= x.length)
            return -1;

        int j = (int)(next + U()*(x.length - next));
        int t = x[j];
        x[j] = x[next];
        return (x[next++] = t);
    }

    /**
       Returns the next k sampled integers. If there are fewer than
       k integers left to be sampled, the array is packed with -1s.
    */
    public int[] nextI(int k)
    {
        int[] y = new int[k];
        for (int i=0; i<y.length; i++)
            y[i] = nextI();
        return y;
    }

    private int[] x = null;
    private int next = 0;
}
