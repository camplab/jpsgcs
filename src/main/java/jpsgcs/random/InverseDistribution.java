package jpsgcs.random;

/**
   This interface defines what it is necessary for a random variable
   to provide in order to use the inversion method of transformation.
*/
public interface InverseDistribution
{
    /**
       Returns the value in the range for which the distribution function
       has the given value.
       For the inversion method it is ok to permute the values in the range so that the most
       common are searched first which results in faster searching.
       As a consequence this function may or may not be useful for 
       other ways of using the distribution function's inverse.
    */
    public double inverseDistribution(double u);
}
