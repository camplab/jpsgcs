package jpsgcs.random;

/**
   This class simulates one dimensional BrownianMotion.
*/
public class BrownianMotion extends StochasticProcess
{
    static { className = "BrownianMotion"; }

    /**
       Creates a new Brownian motion with default variance parameter 1.
    */
    public BrownianMotion()
    {
        this(1);
    }

    /**
       Creates a new Brownian motion simulator with the given variance parameter.
    */
    public BrownianMotion(double var)
    {
        X = new GaussianRV(0,1);
        set(var);
    }

    /**
       Sets the variance parameter of the Brownian motion.
    */
    public void set(double var)
    {
        X.set(0,var);
    }

    /**
       Returns the current point in space and time of the process.
       The runtime type of the state is Locus.
    */
    public StochasticState getState()
    {
        return new Locus(time,x);
    }

    /**
       Sets the point in space and time of the process.
       The state must be a Locus.
    */
    public void setState(StochasticState s)
    {
        Locus l = (Locus)s;
        time = l.time;
        x = l.x;
    }

    /**
       Advances the process t time units.
    */
    public void advance(double t)
    {
        time += t;
        x += X.next() * Math.sqrt(t);
    }
        
    private GaussianRV X = null;
    private double x = 0;
    private double time = 0;
}
