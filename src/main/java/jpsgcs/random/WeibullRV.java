package jpsgcs.random;

/**
   This class delivers replicates from a Weibull distribution.
   The method used is inversion.
*/
public class WeibullRV extends RandomVariable implements InverseDistribution
{
    static { className = "WeibullRV"; }

    /**
       Creates a new Weibull random variable with parameter 1.
    */
    public WeibullRV()
    {
        this(1.0);
    }

    /**
       Creates a new Weibull random variable with the given parameter.
    */
    public WeibullRV(double beta)
    {
        set(beta);
        setMethod(new Inversion(this));
    }

    /**
       Sets the beta parameter.
    */
    public void set(double beta)
    {
        if (beta <= 0) 
            throw new ParameterException("Weibull paramter must be positive");
        b = 1/beta;
    }

    /**
       Implements an inverse distribution function as required by the 
       inversion method.
    */
    public double inverseDistribution(double u)
    {
        return Math.pow(-Math.log(u),b);
    }

    private double b = 1.0;
}
