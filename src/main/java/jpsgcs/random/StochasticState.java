package jpsgcs.random;

/**
   This is the superclass of all objects that define
   the state of a stochastic process.
*/
public class StochasticState
{
    /**
       The point in time of the state.
    */
    public double time = 0;

    /**
       Creates a new stochastic process with time set 
       to the given value.
    */
    public StochasticState(double t)
    {
        time = t;
    }

    /**
       Returns a string representation of the state.
    */
    public String toString()
    {
        return "{"+time+"}";
    }
}
