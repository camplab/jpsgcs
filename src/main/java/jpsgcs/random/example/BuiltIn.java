package jpsgcs.random.example;

import java.util.Random;

public class BuiltIn
{
    public static void main(String[] args)
    {
        Random r = new Random(1);
        for (int i=0; i<20; i++)
            System.out.println(r.nextDouble());
        for (int i=0; i<20; i++)
            System.out.println(-Math.log(r.nextDouble()));
    }
}
