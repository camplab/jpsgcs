package jpsgcs.random.example;

public class UnifRV
{
    public UnifRV(CongGen gen)
    {
        g = gen;
    }

    public double next()
    {
        return g.next();
    }

    private CongGen g = null;
}
