package jpsgcs.random.example;

public class OneOff
{
    public static void main(String[] a)
    {
        long seed = 1;
        long mult = 16807;
        long shift = 0;
        long mod = 0x7FFFFFFF;
        
        for (int i=0; i<20; i++)
            {
                seed = (seed * mult + shift) % mod;
                System.out.println(seed/(double) mod);
            }
        
        for (int i=0; i<20; i++)
            {
                seed = (seed * mult + shift) % mod;
                System.out.println(-Math.log(seed/(double) mod));
            }
    }
}
