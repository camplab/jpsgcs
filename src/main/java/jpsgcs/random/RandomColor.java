package jpsgcs.random;

import java.awt.Color;

/**
   This class generates colours at random.
*/
public class RandomColor extends RandomBag
{
    /**
       The default set of colours.
    */
    public static final Color[] defaultColors = 
    { Color.red, Color.green, Color.blue, Color.yellow, Color.cyan, Color.magenta };

    /**
       Creates a new random bag containing the default set of
       colours.
    */
    public RandomColor()
    {
        this(defaultColors);
    }   

    /**
       Creates a new random bag and puts in the given array of 
       colours.
    */
    public RandomColor(Color[] c)
    {
        super();
        for (int i=0; i<c.length; i++)
            add(c[i]);
    }

    /**
       Returns the next colour.
    */
    public Color nextC()
    {
        return (Color) next();
    }
}
