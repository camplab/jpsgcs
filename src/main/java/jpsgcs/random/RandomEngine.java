package jpsgcs.random;

/**
   This is the base class for all random number generators in
   the package.
*/
abstract public class RandomEngine
{
    static protected String className = "RandomEngine";

    /**
       Repeated calls to this method are assumed to return independent
       realisation from a Uniform(0,1) distribution. 
       No checks are made on the distribution or of independence. It is
       up the to subclass to implement a sound method of generation.
    */
    abstract public double next();

    /**
       The main method simply outputs some realisations from the random
       engine that can be input to statistical software to check the 
       properties. By default 1000 values are output, but a different number
       can be stated on the command line.
       To use this main method the subclass only has to reset the "className"
       string to its name, and provide a default, no argument, constructor.
       Using the main of RandomEngine itself will just throw an exception.
    */
    public static void main(String[] args)
    {
        try
            {
                String s = "jpsgcs.random."+className;
                RandomEngine r = (RandomEngine)(Class.forName(s).newInstance());
                int n = 1000;
                if (args.length > 0)
                    n = Integer.parseInt(args[0]);
                for (int i=0; i<n; i++)
                    System.out.println(r.next());
            }
        catch (Exception e)
            {
                System.out.println("Caught in RandomEngine:main()");
                e.printStackTrace();
            }
    }
}
