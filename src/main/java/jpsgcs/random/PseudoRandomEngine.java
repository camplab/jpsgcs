package jpsgcs.random;

/**
   This subclass of RandomEngine allows a seed to be set and 
   any  sequence of output to be repeated exactly. 
*/
abstract public class PseudoRandomEngine extends RandomEngine
{
    /**
       This sets the seed for the pseduo random number generating method.
       The sequence of output should be determined exactly by the seed.
    */
    abstract public void seed(long s);

    /**
       This provides an arbitrary seed derived from the time of day to 
       allow generators to startup without explicit definition of different
       seeds each time.
    */
    public synchronized final long arbitrarySeed()
    {
        return System.currentTimeMillis();
    }
}
