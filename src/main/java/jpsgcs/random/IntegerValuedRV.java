package jpsgcs.random;

/**
   This is an abstract class for random variables that
   return integer values. 
   The next() method is overwritten to return the value 
   of nextI() as a double. The nextI() method by default
   returns the result of applying an integer method.
*/
public class IntegerValuedRV extends RandomVariable
{
    static { className = "IntegerValuedRV"; }

    /**
       A null constructor for subclassing only.
    */
    public IntegerValuedRV()
    {
    }


    /**
       Sets the method to be alias rejection returning integers
       0 to p.length -1 with probabilities proportional to the
       values given in p. The values need to be non negative.
    */
    public IntegerValuedRV(double[] p)
    {
        set(p);
    }

    public void set(double[] p)
    {
        setMethod(new AliasRejection(p));
    }

    /**
       Returns the value of nextI() as a double.
    */
    public final double next()
    {
        return nextI();
    }

    /**
       Returns the result of applying an integer transformation method.
    */
    public int nextI()
    {
        return ((IntegerMethod)getMethod()).applyI();
    }
}
