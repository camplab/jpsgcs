package jpsgcs.random;

/**
   This interface defines what is necessary for a random variable
   to implement in order that a tangent envelope, more efficient
   than a chord envelope, for exponential switching can be
   constructed.
*/
public interface DifferentiableLogConcave extends LogConcave
{
    /**
       Return the derivative of the log density function at 
       the point x.
    */
    public double derivativeLogDensity(double x);
}
