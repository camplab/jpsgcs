package jpsgcs.random;

/**
   Defines what is necessary for a discrete stochastic process
   to implement.
*/
abstract public class DiscreteStochasticProcess extends StochasticProcess
{
    /**
       Advances the process to the occurrence of the next event.
    */
    abstract public void next();
    /**
       Advances the process to the occurrence of the nth next event.
    */
    abstract public void next(int n);
}
