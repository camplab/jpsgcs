package jpsgcs.random;

/**
   This class provides variates from the Cauchy distribution.
   The method used is ration of uniforms.
*/
public class CauchyRV extends RandomVariable implements RatioRegion
{
    static { className = "CauchyRV"; }

    /**
       Creates a new Cauchy random variable.
    */
    public CauchyRV()
    {
        setMethod(new RatioOfUniforms(this,new UniformRV(0,1),new UniformRV(-1,1)));
    }

    /**
       Returns true iff the given point is contained in a half circle.
    */
    public boolean containsInRatioRegion(double x, double y)
    {
        return x*x + y*y <= 1;
    }
}
