package jpsgcs.random;

/**
   Returns a constant integer value.
*/
public class DegenerateIntegerRV extends IntegerValuedRV
{
    static { className = "DegenerateIntegerRV"; }

    /**
       Creates a new degenerate distribution that always returns 0.
    */
    public DegenerateIntegerRV()
    {
        this(0);
    }

    /**
       Creates a new degenerate distribution that always returns 
       the specified integer value.
    */
    public DegenerateIntegerRV(int i)
    {
        set(i);
    }

    /**
       Sets the return value.
    */
    public void set(int i)
    {
        a = i;
    }

    /**
       Returns the constant value.
    */
    public int nextI()
    {
        return a;
    }

    private int a = 0;
}
