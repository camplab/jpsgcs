package jpsgcs.random;

import java.util.Stack;

/**
   This class implements the general alias rejection method.
*/
public class AliasRejection extends GenerationMethod implements IntegerMethod
{
    /**
       Creates a new alias rejection scheme to generate values from 0 to pp.length-1
       with the probabilities given in pp.
       There is some overhead in setting this up.
    */
    public AliasRejection(double[] pp)
    {
        int n = pp.length;
        I = new UniformIntegerRV(0,n-1);
        q = new double[n];
        a = new int[n];

        Stack<Integer> lo = new Stack<Integer>();
        Stack<Integer> hi = new Stack<Integer>();
                
        double tot = 0;
                
        for (int i=0; i<n; i++)
            tot += pp[i];

        for (int i=0; i<n; i++)
            {
                q[i] = n*pp[i]/tot;
                if (q[i] < 1.0)
                    lo.push(i);
                else
                    hi.push(i);
            }

        while (!lo.isEmpty())
            {
                int j = lo.pop().intValue();
                        
                if (!hi.isEmpty())
                    {
                        int k = hi.peek().intValue();
                        a[j] = k;
                        q[k] -= 1-q[j];
                        if (q[k] <= 1.0)
                            lo.push(hi.pop());
                    }
            }
    }

    /**
       Applies the method and returns the result as a double.
    */
    public double apply()
    {
        return applyI();
    }

    /**
       Applies the method and returns the result as an integer.
    */
    public int applyI()
    {
        int i = I.nextI();
        return U() <= q[i] ? i : a[i];
    }

    private UniformIntegerRV I = null;
    private double[] q = null;
    private int[] a = null;
}
