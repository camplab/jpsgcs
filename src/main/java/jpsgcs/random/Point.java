package jpsgcs.random;

class Point
{
    public Point(double xx, double yy)
    {
        x = xx;
        y = yy;
    }

    public double x = 0;
    public double y = 0;
}
