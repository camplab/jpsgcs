package jpsgcs.random;

/**
   This class implements the simple congruential method of 
   generating pseudo random uniforms. 
*/
public class CongruentialGenerator extends PseudoRandomEngine
{
    static { className = "CongruentialGenerator"; }

    /**
       The default constructor sets the parameters to the so called minimum
       standard of Park and Miller.
    */
    public CongruentialGenerator()
    {
        this(16807,0,0x7FFFFFFF);
    }

    /**
       General constructor that allows the programmer to set the multiplier,
       shift and modulus parameters.
       Choose your own values at your own risk.
       The seed is set arbitrarily.
    */
    public CongruentialGenerator(long mult, long shift, long mod)
    {
        seed(arbitrarySeed());
        m = mult;
        h = shift;
        d = mod;
    }

    /**
       This sets the seed for the generator.
    */
    public final synchronized void seed(long i)
    {
        s = i;
    }

    /**
       Returns the next pseudo random Uniform.
    */
    public final synchronized double next()
    {
        s = ( s * m + h ) % d;
        return s / (double) d;
    }

    private long s, h, m, d;
}
