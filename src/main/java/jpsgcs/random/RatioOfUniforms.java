package jpsgcs.random;

/**
   This class creates a ratio of uniforms scheme to generate
   replicates of a random variable.
*/
public class RatioOfUniforms extends GenerationMethod
{
    /**
       Creates a new ratio of uniforms scheme by simulating (x,y) from (U,V)
       until the point is contained in the random variable's region.
       then the value y/x is returned.
    */
    public RatioOfUniforms(RatioRegion a, UniformRV U, UniformRV V)
    {
        X = U;
        Y = V;
        r = a;
    }

    /**
       Applies the method and returns the result.
    */
    public double apply()
    {
        double x = 0;
        double y = 0;
        do
            {
                x = X.next();
                y = Y.next();
            }
        while (!r.containsInRatioRegion(x,y));
        return y/x;
    }

    private RatioRegion r = null;
    private UniformRV X = null;
    private UniformRV Y = null;
}
