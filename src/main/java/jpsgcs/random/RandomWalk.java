package jpsgcs.random;

/**
   This class simlulates a random walk on the integers.
*/
public class RandomWalk extends MarkovChain
{
    static { className = "RandomWalk"; }

    /**
       Creates a new symetric random walk.
    */
    public RandomWalk()
    {
        this(0.5);
    }

    /**
       Creates a new random walk with P(+1) = p, P(-1) = 1-p.
    */
    public RandomWalk(double p)
    {
        set(p);
    }

    /**
       Sets P(+1) to the given value.
    */
    public void set(double p)
    {
        if (p < 0 || p > 1)
            throw new ParameterException("RandomWalk probability must be in [0,1]");
        prob = p;
    }

    /**
       Advances the walk one step.
    */
    public void next()
    {
        if (U() < prob)
            state++;
        else
            state--;
        count++;
        if (count > time)
            time = count;
    }

    private double prob = 0.5;
}
