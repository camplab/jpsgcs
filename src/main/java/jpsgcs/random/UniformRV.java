package jpsgcs.random;

/**
   Class which generates uniform random variates.
   The method used is a simple linear transformation of 
   the output of the random engine.
   It implements the density function interface so that it can be used as an
   envelope for generating other distributions.
*/
public class UniformRV extends RandomVariable implements Envelope
{
    static { className = "UniformRV"; }

    /**
       Constructs a uniform random variable with default range (0,1).
    */
    public UniformRV()
    {
        this(0,1);
    }

    /**
       Constructs a random variable which generates observations 
       uniformly in the given range.
    */
    public UniformRV(double minimum, double maximum)
    {
        set(minimum,maximum);
    }

    /**
       Resets the range parameters of the random variable.
    */
    public void set(double minimum, double maximum)
    {
        if (minimum >= maximum)
            throw new ParameterException("Uniform minimum must be less than maximum");
        min = minimum;
        max = maximum;
        range = maximum-minimum;
        invrange = 1/range;
    }
    /**
       Return the next variate.
       Overrides the standard method to return a linear function of
       the value given by the underlying random engine.
    */
    public double next()
    {
        return min + range * U();
    }

    /**
       Returns the density function.
    */
    public double densityFunction(double x)
    {
        return x >= min && x <= max ? invrange : 0;
    } 

    /**
       Returns a new point and its density.
    */
    public Point nextPoint()
    {
        double x = next();
        return new Point(x,densityFunction(x));
    }

    private double min = 0;
    private double max = 0;
    private double range = 1;
    private double invrange = 0;
}
