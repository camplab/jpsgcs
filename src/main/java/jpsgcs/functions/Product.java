package jpsgcs.functions;

import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;

import java.util.Set;

/*
	The first variable is the sum of the others.
*/

public class Product extends SkeletonFunction
{
	public Product(Variable x, Variable y, Variable z)
	{
		super(x,y,z);
	}

	public Product(Variable x, Set<Variable> y)
	{
		super(x,y);
	}

	public double getValue()
	{
		int x = 1;

		for (int i=1; i<v.length; i++)
			x *= v[i].getState();

		return v[0].getState() == x ? 1 : 0;
	}
}
