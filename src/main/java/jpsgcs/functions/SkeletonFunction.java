package jpsgcs.functions;

import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;

import java.util.Set;

abstract public class SkeletonFunction extends Function
{
	protected Variable v[] = null;

	public SkeletonFunction(Variable x)
	{
		v = new Variable[1];
		v[0] = x;
		
	}

	public SkeletonFunction(Variable x, Variable y)
	{
		v = new Variable[2];
		v[0] = x;
		v[1] = y;
	}

	public SkeletonFunction(Variable x, Variable y, Variable z)
	{
		v = new Variable[3];
		v[0] = x;
		v[1] = y;
		v[2] = z;
	}

	public SkeletonFunction(Set<Variable> y)
	{
		v = new Variable[y.size()];
		int i=0;
		for (Variable z : y)
			v[i++] = z;
	}

	public SkeletonFunction(Variable x, Set<Variable> y)
	{
		v = new Variable[1+y.size()];
		int i=0;
		v[i++] = x;
		for (Variable z : y)
			v[i++] = z;
	}

	public Variable[] getVariables()
	{
		return v;
	}
}
