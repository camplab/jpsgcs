package jpsgcs.functions;

import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;

/*
	If the second variable is 0, the first is 0.
	Otherwise, the second has a Poisson distribution with the given rate.
*/

public class ConditionalPoisson extends SkeletonFunction
{
	double l = 0.5;

	public ConditionalPoisson(Variable x, Variable y, double rate)
	{
		super(x,y);
		setRate(rate);
	}

	public void setRate(double rate)
	{
		l = rate;
	}

	public double getValue()
	{
		double rate = l * v[1].getState();

		double x = Math.exp(-rate);

		for (int i = 1; i <= v[0].getState(); i++)
			x *= rate/i;
		
		return x;
	}
}
