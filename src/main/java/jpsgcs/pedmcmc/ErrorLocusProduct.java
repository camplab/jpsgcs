package jpsgcs.pedmcmc;

import jpsgcs.genio.BasicGeneticData;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.Function;
import jpsgcs.markov.Functions;
import jpsgcs.markov.Variable;

public class ErrorLocusProduct extends LocusProduct
{
	public ErrorLocusProduct(BasicGeneticData d, int i, double error)
	{
		this(d,i,error,true,true);
	}

	public ErrorLocusProduct(BasicGeneticData d, int i, double error, boolean withpenetrances, boolean withpriors)
	{
		this(d,i,error,withpenetrances,withpriors,allocInheritances(d));
	}

	public ErrorLocusProduct(BasicGeneticData d, int i, double error, boolean withpenetrances, boolean withpriors, Inheritance[][] inher)
	{
		super(d,i,false,withpriors,inher);

		err = new Error[d.nIndividuals()];
		
		if (withpenetrances)
		{
			for (int j=0; j<all.length; j++)
			{
				if (d.penetrance(i,j) != null)
				{
					err[j] = new Error();
					add(new AlleleErrorPenetrance(all[j][0],all[j][1],err[j],d.penetrance(i,j)));
					add(new ErrorPrior(err[j],error));
				}
			}
		}
	}
	
	public Error[] getErrors()
	{
		return err;
	}

// Private data and methods.

	protected Error[] err = null;
}
