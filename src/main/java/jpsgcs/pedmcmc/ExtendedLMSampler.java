package jpsgcs.pedmcmc;

import jpsgcs.genio.GeneticDataSource;
import jpsgcs.markov.Variable;
import jpsgcs.markov.Function;
import jpsgcs.markov.MultiVariable;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.DenseTable;
import jpsgcs.markov.Table;
import jpsgcs.markov.Potential;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Random;

public class ExtendedLMSampler extends LocusMeiosisSampler
{
	private static int[] defups = {3};

	public ExtendedLMSampler(Random r)
	{
		super(r);
	}

	public ExtendedLMSampler(GeneticDataSource d, boolean linkfirst, Random r)
	{
		this(d,linkfirst,false,3,defups,1.00,r);
	}

	public ExtendedLMSampler(GeneticDataSource d, boolean linkfirst, boolean usepotentials, int opt, int[] ns, double rate, Random r)
	{
		this(d,linkfirst,usepotentials,opt,ns,rate,0,0,r);
	}

	public ExtendedLMSampler(GeneticDataSource d, boolean linkfirst, double error, int maxerral, Random r)
	{
		this(d,linkfirst,false,3,defups,1.00,error,maxerral,r);
	}

	public ExtendedLMSampler(GeneticDataSource d, boolean linkfirst, boolean usepotentials, int opt, int[] ns, double rate, double error, int maxerral, Random r)
	{
		this(r);

		if (verbo) System.err.println("ExtendedLM");

		LocusProduct[] p =  null;
		
		p = makeLocusSamplers(d,linkfirst,usepotentials,error,maxerral);

		gg = makeMeiosisSamplers(p,d,linkfirst,usepotentials,opt);

		dat = d;
		ups = ns;
		link = linkfirst;

		samplerate = rate;
	}

	public void sample(boolean report)
	{
		for (int i=0; i<ups.length; i++)
		{
			sampleRandomMeioses(ups[i],dat,link,false);
			if (report)
				System.err.print("R("+ups[i]+")");
		}

		super.sample(report);
	}
		
	public void maximize(boolean report)
	{
		for (int i=0; i<ups.length; i++)
		{
			sampleRandomMeioses(ups[i],dat,link,true);
			if (report)
				System.err.print("R("+ups[i]+")");
		}

		super.maximize(report);
	}
		
// Private data.

	private SplitableGraphicalModel[] gg = null;
	private GeneticDataSource dat = null;
	private int[] ups = null;
	private boolean link = false;

	private void sampleRandomMeioses(int nn, GeneticDataSource d, boolean link, boolean max)
	{
		int n = 0;

		// Find random set of individuals to update.

		int first = link ? 0 : 1;
		for (int j=first; j<h[first].length; j++)
			if (h[first][j][0] != null)
				n++;

		if (n > nn)
			n = nn;

		int[] x = new int[n];
		
		for (int i=0; i<x.length; i++)
		{
			while(true)
			{
				x[i] = (int) (random().nextDouble()*h[first].length);

				if (h[first][x[i]][0] == null)
					continue;

				if (h[first][x[i]][1] == null)
					continue;

				int z = 0;
				for (int j=0; j<i; j++)
					if (x[i] == x[j])
						z++;

				if (z == 0)
					break;
			}
		}

		MarkovRandomField p = new MarkovRandomField();
		Inheritance[] prev = null;
		
		for (int i= first; i<h.length; i++)
		{
			Inheritance[] cur = new Inheritance[2*x.length];

			for (int j=0; j<x.length; j++)
			{
				cur[2*j] = h[i][x[j]][0];
				cur[2*j+1] = h[i][x[j]][1];
			}

			Table t = locusMarginal(cur,gg[i]);
			p.add(t);

			if (prev != null)
			{
				for (int j=0; j<x.length; j++)
				{
					p.add(new Recombination(prev[2*j],cur[2*j],d.getMaleRecomFrac(i-1,i)));
					p.add(new Recombination(prev[2*j+1],cur[2*j+1],d.getFemaleRecomFrac(i-1,i)));
				}
			}

			prev = cur;
		}

		if (!p.getVariables().isEmpty())
		{
			GraphicalModel gm = new GraphicalModel(p,random(),false);
			if (max)
				gm.maximize();
			else
				gm.simulate();
		}
	}

	private Table locusMarginal(Inheritance[] n, SplitableGraphicalModel m)
	{
		Set<Variable> s = new LinkedHashSet<Variable>();
		for (Inheritance i : n)
			s.add(i);
		Potential[][] p = m.splitByOutvol(s);
		m.preLogPeel(p[0],true);

		MultiVariable v = new MultiVariable(n);
		Table t = new DenseTable(v);
		t.allocate();
		double max = Double.NEGATIVE_INFINITY;

		for (v.init(); v.next(); )
		{
			double x = m.postLogPeel(p[1],true);
			if (max < x)
				max = x;
			t.setValue(x);
		}

		for (v.init(); v.next(); )
		{
			double x = t.getValue();
			t.setValue(Math.exp(x-max));
		}

		return t;
	}
}
