package jpsgcs.pedmcmc;

import jpsgcs.util.InputFormatter;
import jpsgcs.genio.ParameterData;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.Variable;
import jpsgcs.markov.MultiVariable;
import jpsgcs.markov.Function;
import jpsgcs.markov.DenseTable;
import jpsgcs.markov.Table;

import java.util.Vector;
import java.util.Collection;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.LinkedHashSet;
import java.io.PrintStream;
import java.io.IOException;
import java.util.Random;

public class LDModel extends MarkovRandomField
{
	public LDModel(Variable[] l)
	{
		super();
		setLoci(l);
	}

	public LDModel(LDModel m, int[] x)
	{
		this(m.getLocusVariables());
		addAll(m.getFunctions());
		peelTo(x);
	}
	
	public LDModel(LDModel m, int lo, int hi)
	{
		this(m.getLocusVariables());
		addAll(m.getFunctions());
		peelTo(lo,hi);
	}

	public LDModel(ParameterData d)
	{
		super();
		loc = new Variable[d.nLoci()];
		for (int i=0; i<d.nLoci(); i++)
			loc[i] = new Variable(d.nAlleles(i));
		setLoci(loc);

		for (int i=0; i<d.nLoci(); i++)
		{
			Variable[] xx = {loc[i]};
			DenseTable dt = new DenseTable(xx);
			dt.setTable(d.alleleFreqs(i));
			add(dt);
		}

		par = d;
	}

	public void initialize()
	{
		initialize(0,loc.length);
	}

	public void initialize(int low, int wid)
	{
		clear();

		for (int i=low; i<low+wid; i++)
		{
			Variable[] xx = {loc[i]};
			DenseTable dt = new DenseTable(xx);
			dt.setTable(par.alleleFreqs(i));
			add(dt);
		}
	}

	public LDModel(InputFormatter f) throws IOException
	{
		this(f,false,0);
	}

	public LDModel(InputFormatter f, boolean longform) throws IOException
	{
		this(f,longform,0);
	}

	public LDModel(InputFormatter f, double epsilon) throws IOException
	{
		this(f,false,epsilon);
	}

	public LDModel(InputFormatter f, boolean longform, double epsilon) throws IOException
	{
		super();

		boolean got = false;

		if (longform)
			got = readLoci(f);
		else
			got = readStateSizes(f);

		if (got)
			readFunctions(f,epsilon);
	}

	public boolean readLoci(InputFormatter f) throws IOException
	{
		if (!f.newLine())
			return false;

		loc = new Variable[f.nextInt()];
		
		for (int i=0; i<loc.length; i++)
		{
			f.newLine();
			int[] s = new int[f.nextInt()];
			for (int j=0; j<s.length; j++)
				s[j] = f.nextInt();

			loc[i] = new Variable(s);
		}

		setLoci(loc);
		return true;
	}

	public boolean readStateSizes(InputFormatter f) throws IOException
	{
		if (!f.newLine())
			return false;

		Vector<Variable> vv = new Vector<Variable>();
		for (int i=0; f.newToken(); i++)
			vv.add(new Variable(f.getInt()));

		loc = new Variable[vv.size()];
		for (int i=0; i<loc.length; i++)
			loc[i] = vv.get(i);

		setLoci(loc);
		return true;
	}

	public void readFunctions(InputFormatter f, double epsilon) throws IOException
	{
		while (f.newLine())
		{
			Vector<Variable> vl = new Vector<Variable>();
			
			while (f.newToken())
				vl.add(loc[f.getInt()]);
			f.newLine();
		
			Variable[] u = (Variable[]) vl.toArray(new Variable[0]);

			MultiVariable m = new MultiVariable(u);
			Table t = new DenseTable(m);
			t.allocate();

			for (m.init(); m.next(); )
			{
				double x = f.nextDouble();
				if (x < epsilon)
					x = epsilon;
				t.setValue(x);
			}

			add(t);
		}
	}

	public void writeTo(PrintStream p)
	{
		writeTo(p,false);
	}

	public void writeTo(PrintStream p, boolean longform)
	{
		if (longform)
			writeVariables(p);
		else
			writeStateSizes(p);

		writeFunctions(p);
	}

	public void writeStateSizes(PrintStream p)
	{
		for (Variable l : loc)
			p.print(" "+l.getNStates());
		p.println();
		p.flush();
	}

	public void writeVariables(PrintStream p)
	{
		p.println(loc.length);
		for (Variable l : loc)
		{
			p.print(l.getNStates());
			for (l.init(); l.next(); )
				p.print(" "+l.getState());
			p.println();
		}
		p.flush();
	}

	public void writeFunctions(PrintStream p)
	{
		for (Function t : getFunctions())
			writeFunction(t,p);
		p.flush();
	}

	public void writeFunction(Function t, PrintStream p)
	{
		Variable[] u = t.getVariables();
		
		//if (u.length == 0 && t.getValue() == 1)
		if (u.length == 0)
			return;

		for (int i=0; i<u.length; i++)
			p.print(" "+index(u[i]));
		p.print("\n");

		MultiVariable m = new MultiVariable(u);
		for (m.init(); m.next(); )
			p.print(" "+t.getValue());

		p.print("\n");
	}

	public MarkovRandomField duplicate(Genotype[] g)
	{
		if (loc.length != g.length)
			throw new RuntimeException("Variable array length missmatch: LDModel.duplicate()");

		Map<Variable,Genotype> map = new LinkedHashMap<Variable,Genotype>();

		for (int i=0; i<loc.length; i++)
		{
			if (g[i].na != loc[i].getNStates())
				throw new RuntimeException("State space size missmatch: LDModel.duplicate()");
			map.put(loc[i],g[i]);
		}

		return duplicate(map);
	}

	public MarkovRandomField duplicate(Map<Variable,Genotype> map)
	{
		MarkovRandomField dup = new MarkovRandomField();
		for (Function f : getFunctions())
			dup.add(duplicate(map,f));

		Set<Variable> rem = new LinkedHashSet<Variable>();
		
		for (Object x : dup.bipartiteGraph().getVertices())
			if (x instanceof Variable)
				rem.add((Variable)x);

		for (Object x : g.getVertices())
			if (x instanceof Variable)
				rem.remove(map.get(x));

		for (Object x : rem)
			dup.remove(x);

		return dup;
	}

	private Function duplicate(Map<Variable,Genotype> map, Function f)
	{
		Set<Variable> v = new LinkedHashSet<Variable>();
		for (Variable x : f.getVariables())
			v.add(map.get(x));

		MultiVariable w = new MultiVariable(v);
		DenseTable t = new DenseTable(w);
		t.allocate();

		for (w.init(); w.next(); )
		{
			double z = 1;
	
			for (Variable x : f.getVariables())
				x.setState(map.get(x).pat());
			z *= f.getValue();

			for (Variable x : f.getVariables())
				x.setState(map.get(x).mat());
			z *= f.getValue();

			t.setValue(z);
		}

		return t;
	}

	public Variable[] getLocusVariables()
	{
		return loc;
	}

	public int[] usedLoci()
	{
		Set<Integer> x = new LinkedHashSet<Integer>();
		for (Object v : g.getVertices())
			if (v instanceof Variable)
				x.add(map.get(v));

		int[] s = new int[x.size()];
		int i = 0;
		for (Integer ii : x)
			s[i++] = ii.intValue();

		return s;
	}

	public void report()
	{
		System.err.println(usedLoci().length);
//		for (int i : usedLoci())
//			System.err.print(" "+i);
//		System.err.println();
	}

/*
	public void peelTo(Collection<Variable> keep)
	{
	//	Set<Variable> peel = new LinkedHashSet<Variable>();
	//	for (int i=loc.length-1; i>=0; i--)
	//		if (!keep.contains(loc[i]))
	//			peel.add(loc[i]);
	//	super.peelAll(peel);

		super.peelTo(keep);
		Set<Variable> rem = new LinkedHashSet<Variable>(map.keySet());
		rem.removeAll(keep);
		removeIndexes(rem);
	}
*/

	public void peelTo(int lo, int hi)
	{
		Set<Variable> peel = new LinkedHashSet<Variable>();
		for (int i=loc.length-1; i>hi; i--)
			peel.add(loc[i]);
		for (int i=0; i<lo; i++)
			peel.add(loc[i]);

		super.peelAll(peel);
		removeIndexes(peel);
	}

	public void peelTo(int[] x)
	{
		int lo = Integer.MAX_VALUE;
		int hi = 0;

		for (int i : x)
		{
			if (lo > i)
				lo = i;
			if (hi < i)
				hi = i;
		}

		Set<Variable> peel = new LinkedHashSet<Variable>();
		for (int i=0; i<lo; i++)
			peel.add(loc[i]);
		for (int i=loc.length-1; i>hi; i--)
			peel.add(loc[i]);
		for (int i=lo; i<=hi; i++)
			peel.add(loc[i]);
		for (int i : x)
			peel.remove(loc[i]);

		super.peelAll(peel);
		removeIndexes(peel);
	}

	public void setParameterData(ParameterData d)
	{
		par = d;
	}
	

// Private data.

	private Variable[] loc = null;
	private Map<Variable,Integer> map = null;
	private ParameterData par = null;

	private void removeIndexes(Set<Variable> rem)
	{
		for (Variable v : rem)
			map.remove(v);

		loc = new Variable[map.size()];

		int i = 0;
		for (Variable v : map.keySet())
		{
			map.put(v,i);
			loc[i++] = v;
		}
	}

	private void setLoci(Variable[] l)
	{
		loc = l;
		map = new LinkedHashMap<Variable,Integer>();
		for (int i=0; i<loc.length; i++)
			map.put(loc[i],i);
	}

	private int index(Variable v)
	{
		return map.get(v).intValue();
	}
}
