package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

public class AllelePenetrance extends GenepiFunction 
{
	protected AllelePenetrance()
	{
	}

	public AllelePenetrance(Allele paternal, Allele maternal)
	{
		this(paternal,maternal,null);
	}

	public AllelePenetrance(Allele paternal, Allele maternal, double[][] penet)
	{
		v = new Variable[2];
		v[0] = paternal;
		v[1] = maternal;
		fix(penet);
	}

	public double getValue()
	{
		return p == null ? 1 :  p[v[0].getState()][v[1].getState()];
	}

	public String toString()
	{
		return "PEN"+super.toString();
	}

	public void fix(double[][] penet)
	{
		p = penet;
	}

// Private data.

	protected double[][] p = null;
}
