package jpsgcs.pedmcmc;

import jpsgcs.genio.GeneticDataSource;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.util.Monitor;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Random;

public class LocusSampler
{
	private Random rand = null;

	protected Random random()
	{
		return rand;
	}

	public static boolean verbo = false;

	protected LocusSampler(Random r)
	{
		rand = r;
	}

	public LocusSampler(GeneticDataSource d,boolean linkfirst, Random r)
	{
		this(d,linkfirst,false,r);
	}

	public LocusSampler(GeneticDataSource d, boolean linkfirst, boolean usepotentials, Random r)
	{
		this(d,linkfirst,usepotentials,0,0,r);
	}

	public LocusSampler(GeneticDataSource d,boolean linkfirst, double error, int maxerral, Random r)
	{
		this(d,linkfirst,false,error,maxerral,r);
	}

	public LocusSampler(GeneticDataSource d, boolean linkfirst, boolean usepotentials, double error, int maxerral, Random r)
	{
		this(r);
		makeLocusSamplers(d,linkfirst,usepotentials,error,maxerral);
	}

	public void initialize()
	{
		for (int i=0; i<g.length; i++)
			if (g[i] != null)
				g[i].simulate(false);
	}

	public void maximize()
	{
		maximize(false);
	}

	public void maximize(boolean report)
	{
		for (int i=0; i<g.length; i++)
			if (g[i] != null)
			{
				g[i].maximize();
				if (report)
					System.err.print("L");
			}
	}

	public void sample()
	{
		sample(false);
	}

	public void sample(boolean report)
	{
		for (int i=0; i<g.length; i++)
			if (g[i] != null)
			{
				g[i].simulate();
				if (report)
					System.err.print("L");
			}
	} 

	public Inheritance[][][] getInheritances()
	{
		return h;
	}

	public Allele[][][] getAlleles()
	{
		return al;
	}

	public Error[][] getErrors()
	{
		return e;
	}

	protected LocusProduct[] makeLocusSamplers(GeneticDataSource d, boolean linkfirst, boolean usepotentials, double error, int maxerr)
	{
		if (verbo) System.err.println("Making locus samplers");

		LocusProduct[] p = makeLocusProducts(d,linkfirst,error,maxerr,true);

		g = new GraphicalModel[p.length];
		for (int i=0; i<g.length; i++)
			if (p[i] != null)
			{
				g[i] = new GraphicalModel(p[i],rand,usepotentials);
				if (verbo) System.err.println("Done "+i);
			}

		return p;
	}

// Private data.
// Protected methods to make locus and meiosis samplers to be used by this and derived classes.

	protected GraphicalModel[] g = null;
	protected Inheritance[][][] h = null;
	protected Error[][] e = null;
	protected Allele[][][] al = null;

	protected LocusProduct[] makeLocusProducts(GeneticDataSource d, boolean linkfirst, double error, int maxerr, boolean withpriors)
	{
		LocusProduct[] p = new LocusProduct[d.nLoci()];

		int first = linkfirst ? 0 : 1;

		h = new Inheritance[p.length][][];
		if (error > 0 && maxerr > 1)
			e = new Error[p.length][];

		for (int i=first; i<p.length; i++)
		{
			if (error > 0 && maxerr > 1 && d.nAlleles(i) <= maxerr)
			{
				p[i] = new ErrorLocusProduct(d,i,error,true,withpriors);
				h[i] = p[i].inh;
				e[i] = ((ErrorLocusProduct)p[i]).err;
			}
			else
			{
				p[i] = new LocusProduct(d,i,true,withpriors);
				h[i] = p[i].inh;
				GraphicalModel gm = new GraphicalModel(p[i],rand,false);
				gm.reduceStates();
			}
		}

		for (int j=0; j<h[1].length; j++)
		{
			if (h[first][j][0] != null)
			{
				for (int i = 1+first; i<p.length; i++)
				{
					Recombination f = new Recombination(h[i-1][j][0],h[i][j][0],d.getMaleRecomFrac(i-1,i));
					p[i-1].add(f);
					p[i-1].remove(h[i][j][0]);
					p[i].add(f);
					p[i].remove(h[i-1][j][0]);
				}
			}

			if (h[first][j][1] != null)
			{
				for (int i = 1+first; i<p.length; i++)
				{
					Recombination f = new Recombination(h[i-1][j][1],h[i][j][1],d.getFemaleRecomFrac(i-1,i));
					p[i-1].add(f);
					p[i-1].remove(h[i][j][1]);
					p[i].add(f);
					p[i].remove(h[i-1][j][1]);
				}
			}
		}

		al = new Allele[p.length][][];
		for (int i=first; i<p.length; i++)
			al[i] = p[i].all;

		return p;
	}
}
