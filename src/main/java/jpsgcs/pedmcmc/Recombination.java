package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

public class Recombination extends GenepiFunction
{
	public Recombination(Variable x, Variable y, double theta)
	{
		v = new Variable[2];
		v[0] = x;
		v[1] = y;
		t = theta;
		onemint = 1-t;
	}

	public void fix(double th)
	{
		t = th;
		onemint = 1-t;
	}

	public double getValue()
	{
		return v[0].getState() == v[1].getState() ? onemint : t;
	}

	public String toString()
	{
		return "REC"+super.toString();
	}

// Private data.

	private double t = 0;
	private double onemint = 0;
}
