package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

public class Allele extends Variable
{
	public Allele(int n)
	{
		super(n);
	}

	public String toString()
	{
		return "A("+super.toString()+")"+getNStates();
	}
}
