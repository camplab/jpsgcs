package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

public class ErrorPrior extends GenepiFunction 
{
	public ErrorPrior(Error e, double prob)
	{
		v = new Variable[1];
		v[0] = e;
		p = prob;
	}

	public double getValue()
	{
		return v[0].getState() == 0 ? 1-p : p;
	}

	public String toString()
	{
		return "EPRI"+super.toString();
	}

// Private data.

	private double p = 0;
}
