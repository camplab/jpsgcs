package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;

public class AuxLinkFunction extends GenepiFunction
{
	public AuxLinkFunction(AuxVariable xx, AuxVariable yy, double ptheta, double mtheta)
	{
		x = xx;
		y = yy;

		v = new Variable[2];
		v[0] = x;
		v[1] = y;

		pt = ptheta;
		mt = mtheta;
	}

	public double getValue()
	{
		double a = 1;

		for (int i=0; i<x.p.length; i++)
			a *= x.p[i].getState() == y.p[i].getState() ? 1-pt : pt;

		for (int i=0; i<x.m.length; i++)
			a *= x.m[i].getState() == y.m[i].getState() ? 1-mt : mt;

		return a;
	}

	public String toString()
	{
		return "ALINK"+super.toString();
	}
	
	private AuxVariable x = null;
	private AuxVariable y = null;
	private double pt = 0;
	private double mt = 0;
}
