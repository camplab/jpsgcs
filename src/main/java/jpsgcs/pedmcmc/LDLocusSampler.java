package jpsgcs.pedmcmc;

import jpsgcs.genio.GeneticDataSource;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.Variable;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.Function;
import jpsgcs.markov.FillIn;

import java.util.Map;
import java.util.Set;
import java.util.LinkedHashMap;
import java.util.Random;

public class LDLocusSampler extends LocusSampler
{
	public LDLocusSampler(Random r)
	{
		super(r);
	}

	public LDLocusSampler(GeneticDataSource d, LDModel ld, boolean linkfirst, Random r)
	{
		this(d,ld,linkfirst,false,r);
	}

	public LDLocusSampler(GeneticDataSource d, LDModel ld, boolean linkfirst, boolean usepotentials, Random r)
	{
		this(d,ld,linkfirst,usepotentials,0,0,r);
	}

	public LDLocusSampler(GeneticDataSource d, LDModel ld, boolean linkfirst, double error, int maxerral, Random r)
	{
		this(d,ld,linkfirst,false,error,maxerral,r);
	}

	public LDLocusSampler(GeneticDataSource d, LDModel ld, boolean linkfirst, boolean usepotentials, double error, int maxerral, Random r)
	{
		this(r);

		MarkovRandomField prod = new MarkovRandomField();

		// Make locus products without priors.
		LocusProduct[] p = makeLocusProducts(d,linkfirst,error,maxerral,false);

		for (int i=0; i<p.length; i++)
		{
			if (!p[i].getFunctions().isEmpty())
			{
				GraphicalModel gm = new GraphicalModel(p[i],random(),false);
				gm.reduceStates();
				prod.addAll(p[i].getFunctions());
			}
		}

		// Link locus products with recombinations between inheritances.
		for (int j=0; j<h[0].length; j++)
		{
			if (h[0][j][0] != null)
			{
				for (int i = (linkfirst ? 1 : 2); i<p.length; i++)
					prod.add(new Recombination(h[i-1][j][0],h[i][j][0],d.getMaleRecomFrac(i-1,i)));
			}

			if (h[0][j][1] != null)
			{
				for (int i = (linkfirst ? 1 : 2); i<p.length; i++)
					prod.add(new Recombination(h[i-1][j][1],h[i][j][1],d.getFemaleRecomFrac(i-1,i)));
			}
		}

		// Link locus products with ld between founder alleles.
		Variable[] v = ld.getLocusVariables();
		Map<Variable,Variable> map = new LinkedHashMap<Variable,Variable>();

		for (int j=0; j<h[0].length; j++)
		{
			if (h[0][j][0] == null)
			{
				map.clear();
				for (int i=0; i<v.length; i++)
					map.put(v[i],al[i][j][0]);
				prod.addAll(ld.replicate(map).getFunctions());
			}

			if (h[0][j][1] == null)
			{
				map.clear();
				for (int i=0; i<v.length; i++)
					map.put(v[i],al[i][j][1]);
				prod.addAll(ld.replicate(map).getFunctions());
			}
		}


		g = new GraphicalModel[p.length];

		for (int i=0; i<g.length; i++)
			g[i] = new GraphicalModel(prod.subField(p[i].getVariables()),random(),usepotentials);
		
	}
}
