package jpsgcs.pedmcmc;

import jpsgcs.markov.Variable;
import jpsgcs.markov.Potential;

import java.util.Set;
import java.util.LinkedHashSet;

public class AuxLocusFunction extends GenepiFunction
{
	public AuxLocusFunction (AuxVariable x, SplitableGraphicalModel g)
	{
		v = new Variable[1];
		v[0] = x;
		m = g;
		tab = new double[v[0].getNStates()];

		Set<Variable> s = new LinkedHashSet<Variable>();
		for (Variable u : x.p)
			s.add(u);
		for (Variable u : x.m)
			s.add(u);

		p = g.splitByOutvol(s);
	}

	Potential[][] p = null;

	public void prepare()
	{
		m.preLogPeel(p[0],true);

		for (v[0].init(); v[0].next(); )
			tab[v[0].getState()] = m.postLogPeel(p[1],true);

		m.cleanUpOutputs();

/*
		for (v[0].init(); v[0].next(); )
			tab[v[0].getState()] = m.logPeel(true);
*/

		double t = tab[0];

		for (int i=0; i<tab.length; i++)
			tab[i] = Math.exp(tab[i]-t);
	}

	public double getValue()
	{
		return tab[v[0].getState()];
	}

	public String toString()
	{
		return "ALOC"+super.toString();
	}

// Private data.

	private double[] tab = null;
	private SplitableGraphicalModel m = null;
}
