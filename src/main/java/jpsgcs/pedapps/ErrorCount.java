package jpsgcs.pedapps;

import jpsgcs.pedmcmc.Error;

public class ErrorCount
{
	public ErrorCount(Error[][] error)
	{
		e = error;

		int mx = 0;
		for (int i=0; i<e.length; i++)
			if (e[i] != null && e[i].length > mx)
				mx = e[i].length;
		c = new double[e.length][mx];

		n = 0;
	}

	public void update()
	{
		for (int i=0; i<e.length; i++)
			if (e[i] != null)
			{
				for (int j=0; j<e[i].length; j++)
					if (e[i][j] != null)
					{
						c[i][j] += e[i][j].getState();
					}
			}
		n += 1;
	}

	public double[][] results()
	{
		double[][] res = new double[c.length][c[0].length];

		for (int i=0; i<c.length; i++)
			for (int j=0; j<c[i].length; j++)
				res[i][j] = c[i][j] / n;

		return res;
	}

// Private data and methods.

	private Error[][] e = null;
	private double[][] c = null;
	private double n = 0;
}
