package jpsgcs.pedapps;

import jpsgcs.genio.BasicGeneticData;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.Table;
import jpsgcs.markov.Variable;
import jpsgcs.pedmcmc.Allele;
import jpsgcs.pedmcmc.Inheritance;
import jpsgcs.pedmcmc.LocusProduct;
import jpsgcs.markov.Function;
import jpsgcs.pedmcmc.AllelePrior;

import java.util.Map;
import java.util.LinkedHashMap;

public class GeneCounter
{
/** 
	Finds allele frequencies for the specified locus using Cedric Smith's gene counting
	method taking the whole pedigree structure into account.
	The estimates replace the existing values in the given genetic data
	struture.
	Retuns the standard errors of the estimates.
*/
	public static double[] geneCount(BasicGeneticData data, int i)
	{
		return geneCount(data,i,0.000001,1000);
	}

	public static double[] geneCount(BasicGeneticData d, int i, double max_diff, int max_its)
	{
		// Make the product of terms to peel for this locus.

		LocusProduct p = new LocusProduct(d,i);

		// Find the founder alleles.

		Inheritance[][] h = p.getInheritances();
		int nf = 0;
		for (int j=0; j<h.length; j++)
		{
			if (h[j][0] == null)
				nf++;
			if (h[j][1] == null)
				nf++;
		}

		Allele[][] al = p.getAlleles();
		Allele[] f = new Allele[nf];
		nf = 0;
		for (int j=0; j<h.length; j++)
		{
			if (h[j][0] == null)
				f[nf++] = al[j][0];
			if (h[j][1] == null)
				f[nf++] = al[j][1];
		}
		
		// Make the graphical model to peel the locus.

		GraphicalModel g = new GraphicalModel(p,null,false);
		g.reduceStates();
		
		// Track the locus alleles frequenceis.

		double[] freqs = new double[d.nAlleles(i)];
		double[] olderfreqs = new double[freqs.length];
		for (int k=0; k<olderfreqs.length; k++)
			olderfreqs[k] = 1.0 / olderfreqs.length;
		double[] oldfreqs = d.alleleFreqs(i);

		// Loop for gene counting.

		int its = 0;
		double  diff = 1;
		double lambda = 0;

		for ( ; its < max_its && diff > max_diff; its++)
		{
			Map<Variable,Function> map = g.variableMargins();
			//g.marginalize();

			for (int k=0; k<freqs.length; k++)
				freqs[k] = 0;

			for (Allele a : f)
			{
				Function t = map.get(a);
				for (a.init(); a.next(); )
					freqs[a.getState()] += t.getValue();
			}

			double tot = 0;
			for (int k=0; k<freqs.length; k++)
				tot += freqs[k];

			if (tot <= 0)
				throw new RuntimeException("Inconsistency in data. Cannot use gene counting. Locus: "+i);

			diff = 0;
			double ll = 0;
			int cc = 0;

			for (int k=0; k<freqs.length; k++)
			{
				freqs[k] /= tot;
				diff += Math.abs(oldfreqs[k] - freqs[k]);

				double dd = olderfreqs[k] - freqs[k];

				if (dd != 0)
				{
					dd = (oldfreqs[k] - freqs[k]) / dd;
					if (dd > 0 && dd < 1)
					{
						ll += dd;
						cc += 1;
					}
				}
			}

			if (cc > 0)
				lambda = ll/cc;

			for (int k=0; k<freqs.length; k++)
			{
				olderfreqs[k] = oldfreqs[k];
				oldfreqs[k] = freqs[k];
			}
		}

		if (its == max_its)
			System.err.println("Warning: maximum number of iterations exceeded in gene counting on locus "+i);

		for (int k=0; k<freqs.length; k++)
			freqs[k] = Math.sqrt( freqs[k] * (1-freqs[k]) / (1-lambda) / f.length );
			
		return freqs;
	}
}
