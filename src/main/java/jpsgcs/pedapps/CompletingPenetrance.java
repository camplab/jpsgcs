package jpsgcs.pedapps;

import jpsgcs.pedmcmc.GenotypeErrorPenetrance;
import jpsgcs.pedmcmc.Genotype;
import jpsgcs.pedmcmc.Error;
import jpsgcs.markov.Variable;

// The dummy variable makes the function depend on a variable that is
// not in the graphical model. Hence, if the graphical model is compiled
// this function is not included in the potential function. Hence,
// the graphical model can be compiled to speed up stuff that depends on
// other functions, but leaves this function changable. This is useful
// when the penetrance functions change to accomodate data for different
// individuals, but using the same graphical model.

public class CompletingPenetrance extends GenotypeErrorPenetrance
{
	public CompletingPenetrance(Genotype g, Error e, Variable dummy)
	{
		this(g,e,null,dummy);
	}

	public CompletingPenetrance(Genotype g, Error e, double[][] penet, Variable dummy)
	{
		super(g,e,penet);
		Variable[] u = new Variable[3];
		u[0] = v[0];
		u[1] = v[1];
		u[2] = dummy;
		v = u;
	}
}
