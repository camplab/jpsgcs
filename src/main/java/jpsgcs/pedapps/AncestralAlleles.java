package jpsgcs.pedapps;

import jpsgcs.genio.BasicGeneticData;
import jpsgcs.pedmcmc.Inheritance;
import java.util.Vector;

public class AncestralAlleles
{
	public AncestralAlleles(BasicGeneticData d, boolean linkfirst)
	{
		first = linkfirst ? 0 : 1;

		pa = Pedigrees.fathers(d);
		ma = Pedigrees.mothers(d);
		order = Pedigrees.canonicalOrder(d);

		anc = new int[d.nIndividuals()][2][d.nLoci()];

		int allele = 0;

		for (int j=0; j<anc.length; j++)
		{
			if (pa[j] < 0)
			{
				for (int i=0; i<anc[j][0].length; i++)
					anc[j][0][i] = allele;
				allele++;
			}

			if (ma[j] < 0)
			{
				for (int i=0; i<anc[j][1].length; i++)
					anc[j][1][i] = allele;
				allele++;
			}
		}
	}

	public void update(Inheritance[][][] h)
	{
		for (int i=first; i<h.length; i++)
		{
			for (int jj=0; jj<anc.length; jj++)
			{
				int j = order[jj];

				if (pa[j] >= 0)
					anc[j][0][i] = anc[pa[j]][h[i][j][0].getState()][i];
	
				if (ma[j] >= 0)
					anc[j][1][i] = anc[ma[j]][h[i][j][1].getState()][i];
			}
		}
	}

	public int[] haplotype(int j, int k)
	{
		return anc[j][k];
	}

	public void print()
	{
		for (int j=0; j<anc.length; j++)
		{
			for (int i=0; i<anc[j][0].length; i++)
				System.err.print(anc[j][0][i]);
			System.err.print(" ");
			for (int i=0; i<anc[j][1].length; i++)
				System.err.print(anc[j][1][i]);
			System.err.println();
		}
	}

// Private data and methods.

	private int[][][] anc = null;
	private int[] pa = null;
	private int[] ma = null;
	private int[] order = null;
	private int first = 0;
}
