package jpsgcs.pedapps;

import jpsgcs.jtree.IntegerMatrix;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.genio.BasicGeneticData;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.pedmcmc.Error;
import jpsgcs.pedmcmc.Genotype;
import jpsgcs.pedmcmc.GenotypeErrorPenetrance;
import jpsgcs.pedmcmc.GenotypePenetrance;
import jpsgcs.pedmcmc.ErrorPrior;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;
import java.util.Random;

public class CompleteHaplotypes implements IntegerMatrix
{
	public CompleteHaplotypes(GeneticDataSource x, double eprob, int hold, Random r)
	{
		rand = r;
		data = x;
		dummy = new Variable(1);

		p = new GenotypePenetrance[data.nLoci()];
		g = new Genotype[data.nLoci()];
		e = new ErrorPrior[data.nLoci()];

		for (int i=0; i<p.length; i++)
		{
			g[i] = new Genotype(data.nAlleles(i));
			Error err = new Error();
			e[i] = new ErrorPrior(err,eprob);
			p[i] = new CompletingPenetrance(g[i],err,dummy);
		}

		haps = new int[2*data.nIndividuals()][hold];

		offset = 0;
	}

	public CompleteHaplotypes(GeneticDataSource x, double eprob, Random r)
	{
		this (x,eprob,x.nLoci(),r);
	}

	public CompleteHaplotypes(GeneticDataSource x, LDModel ld, double eprob, Random r)
	{
		this(x,eprob,r);
		setModel(ld);
	}

	protected CompleteHaplotypes()
	{
	}

	public void simulate()
	{
		update(true);
	}

	public void maximize()
	{
		update(false);
	}

	public void update(boolean randomize)
	{

		for (int j=0; j<data.nIndividuals(); j++)
		{
			for (int i : lociNeeded)
				p[i].fix(data.penetrance(i,j));


			if (randomize)
				gm.simulate();
			else
				gm.maximize();

			for (int i : lociNeeded)
			{
				haps[2*j][i-offset] = g[i].pat();
				haps[2*j+1][i-offset] = g[i].mat();
			}
		}


	}

	public int[][] getHaplotypes()
	{
		return haps;
	}

	public int nRows()
	{
		return haps.length;
	}

	public int nColumns()
	{
		throw new RuntimeException("All columns are not held by CompleteHaplotypes. Can't give nCols()");
	}

	public int value(int j, int i)
	{
		return haps[j][i-offset];
	}

	public void shift(int n)
	{
		int m = n;
		if (offset + haps[0].length + m > data.nLoci())
			m = data.nLoci() - haps[0].length - offset;
		
		for (int j=0; j<haps.length; j++)
		{
			for (int i=0; i<haps[j].length-m; i++)
				haps[j][i] = haps[j][i+m];
			for (int i=haps[j].length-m; i<haps[j].length; i++)
				haps[j][i] = 0;
		}

		offset += m;
	}

	public void setModel(LDModel ld)
	{
		lociNeeded = ld.usedLoci();

		MarkovRandomField m = new MarkovRandomField();

		for (int i : lociNeeded)
		{
			m.add(p[i]);
			m.add(e[i]);
		}

		for (Function f : ld.duplicate(g).getFunctions())
			m.add(f);

		m.remove(dummy);

		gm = new GraphicalModel(m,rand,true);
	}

	public GenotypePenetrance[] getPenetrances()
	{
		return p;
	}

	public ErrorPrior[] getPriors()
	{
		return e;
	}

// Privat data.

	protected Random rand = null;
	protected GeneticDataSource data = null;
	protected int[][] haps = null;
	protected int offset = 0;

	protected GenotypePenetrance[] p = null;
	protected ErrorPrior[] e = null;

	private Genotype[] g = null;
	private GraphicalModel gm = null;
	private int[] lociNeeded = null;
	private Variable dummy = null;
}
