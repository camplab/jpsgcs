package jpsgcs.pedapps;

import jpsgcs.genio.GeneticDataSource;
import jpsgcs.genio.PedigreeData;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.linkage.LinkagePhenotype;
import jpsgcs.graph.DAGLocator;
import jpsgcs.viewgraph.PaintableGraph;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.viewgraph.VertexRepresentation;

import java.awt.Color;
import java.util.Map;
import java.util.LinkedHashMap;

public class HaplotypeGraphFrame extends GraphFrame<Integer,Object>
{
	private static PaintableGraph<Integer,Object> makeGraph(GeneticDataSource data, AncestralAlleles anc, boolean phenos, boolean liab, boolean arrows, boolean sexless, boolean mono)
	{
		int hapHeight = 1;

		Map<Integer,VertexRepresentation> map = new LinkedHashMap<Integer,VertexRepresentation>();
		MarriageNodeGraph g = new MarriageNodeGraph(data,map,false);
		PaintableGraph<Integer,Object> p = new PaintableGraph<Integer,Object>(g,map);

		if (mono)
			for (VertexRepresentation v : map.values())
				v.setColor(Color.white);

		if (anc != null)
		{
			Color[] cmap = colourMap(data);
				for (int i=0; i<data.nIndividuals(); i++)
					((HaplotypeNode)map.get(i)).setHaplotypes(anc.haplotype(i,0),anc.haplotype(i,1),cmap);
		}

		p.setArrows(arrows);
		p.setArrowParameters(4,3,0.5);

		if (phenos)
			colourPhenotypes(data,map,liab);

		if (sexless)
			for (int i=0; i<data.nIndividuals(); i++)
				((HaplotypeNode)map.get(i)).setShape(1);

		for (int i=0; i<data.nIndividuals(); i++)
			if (map.get(i) != null)
			((HaplotypeNode)map.get(i)).setHapHeight(hapHeight);

		return p;
	}

	public HaplotypeGraphFrame(GeneticDataSource data, boolean phenos, boolean liab, boolean arrows, boolean sexless, boolean mono)
	{
		this(data,null,phenos,liab,arrows,sexless,mono);
	}

	public HaplotypeGraphFrame(GeneticDataSource data, AncestralAlleles anc, boolean phenos, boolean liab, boolean arrows, boolean sexless, boolean mono)
	{
		super(makeGraph(data,anc,phenos,liab,arrows,sexless,mono), new DAGLocator<Integer,Object>());
	}

        public static void colourPhenotypes(GeneticDataSource data, Map<Integer,VertexRepresentation> map, boolean liab)
        {
                LinkageIndividual[] lind = ((LinkageInterface)data).raw().getPedigreeData().getIndividuals();
                for (int  i=0; i<lind.length ;i++)
                {
                        HaplotypeNode n = (HaplotypeNode) map.get(i);
                        LinkagePhenotype ph = lind[i].getPhenotype(0);

                        switch(ph.getAffectedStatus())
                        {
                        case 2: n.setColor(Color.black);
                                n.setTextColor(Color.white);
                                break;

                        case 1: n.setColor(Color.white);
                                n.setTextColor(Color.black);
                                break;

                        case 0: n.setColor(Color.lightGray);
                                n.setTextColor(Color.black);
                                break;

			}

			if (liab)
                        	n.setStatus(ph.getAffectedStatus(),ph.getLiability());
                }
        }

        public static Color[] colourMap(PedigreeData d)
        {
                int na = 0;
                for (int i=0; i<d.nIndividuals(); i++)
                {
                        if (d.pa(i) < 0)
                                na++;
                        if (d.ma(i) < 0)
                                na++;
                }
                Color[] c = new Color[na];
                na = 0;

                int nt = 0;
                int[] g = Pedigrees.generation(d);
                for (int i=0; i<g.length; i++)
                        if (g[i] == 0)
                                nt++;
                Color[] x = colourMap(2*nt);
                nt = 0;

                for (int i=0; i<d.nIndividuals(); i++)
                {
                        if (d.pa(i) < 0)
                        {
                                c[na++] = g[i] == 0 ? x[nt++] : Color.white;
                        }
                        if (d.ma(i) < 0)
                        {
                                c[na++] = g[i] == 0 ? x[nt++] : Color.white;
                        }
                }

                return c;
        }

        public static Color[] colourMap(int nn)
        {
                int n = nn + 1 - nn%2;
                Color[] c = new Color[n];
                double hh = (n/2 + 1)/(float)(n);

                for (int i=0; i<c.length; i++)
                        c[i] = Color.getHSBColor((float)(hh*i),1,1);

                return c;
        }
}
