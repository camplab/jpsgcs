package jpsgcs.pedapps;

import jpsgcs.genio.PedigreeData;
import jpsgcs.graph.Network;
import jpsgcs.viewgraph.Blob;
import jpsgcs.viewgraph.VertexRepresentation;

import java.util.Map;
import java.awt.Color;

public class MarriageNodeGraph extends Network<Integer,Object>
{
	public MarriageNodeGraph(PedigreeData ped, Map<Integer,VertexRepresentation> map)
	{
		this(ped,map,false);
	}

	public MarriageNodeGraph(PedigreeData ped, Map<Integer,VertexRepresentation> map, boolean usepedid)
	{
		super(true,false);
		int[][] fam = ped.nuclearFamilies();

		for (int i=0; i<fam.length; i++)
		{
			connect(fam[i][0],mar(i));
			connect(fam[i][1],mar(i));
			for (int k=2; k<fam[i].length; k++)
				connect(mar(i),fam[i][k]);
		}

		for (int i=0; i<fam.length; i++)
		{
			Blob mar = new Blob();
			mar.setColor(Color.white);
			mar.setSize(2,2);
			map.put(mar(i),mar);
			
			VertexRepresentation pa = map.get(fam[i][0]);
			if (pa == null)
			{
				pa = new HaplotypeNode(ind(ped,fam[i][0],usepedid),Color.cyan,0);
				map.put(fam[i][0],pa);
			}

			VertexRepresentation ma = map.get(fam[i][1]);
			if (ma == null)
			{
				ma = new HaplotypeNode(ind(ped,fam[i][1],usepedid),Color.yellow,1);
				map.put(fam[i][1],ma);
			}
		}

		for (int i=0; i<fam.length; i++)
		{
			for (int k=2; k<fam[i].length; k++)
			{
				VertexRepresentation kid = map.get(fam[i][k]);
				if (kid == null)
				{
					kid = new HaplotypeNode(ind(ped,fam[i][k],usepedid),Color.white,2);
					map.put(fam[i][k],kid);
				}
			}
		}

		for (int i=0; i<ped.nIndividuals(); i++)
		{
			if (!contains(i))
			{
				add(i);
				VertexRepresentation kid = new HaplotypeNode(ind(ped,i,usepedid),Color.white,2);
				map.put(i,kid);
			}
		}
		
	}

	private String ind(PedigreeData ped, int i, boolean usepedid)
	{
		if (usepedid)
			return ped.pedigreeName(i)+" "+ped.individualName(i);
		else
			return ped.individualName(i);
	}

	private int mar(int i)
	{
		return -(i+1);
	}
}
