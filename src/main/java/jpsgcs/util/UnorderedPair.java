package jpsgcs.util;

public class UnorderedPair<E> extends Pair<E,E>
{
    /**
       Creates a new null pair.
    */
    public UnorderedPair()
        {
        }

    /**
       Creates a new pair for the given two objects.
    */
    public UnorderedPair(E a, E b)
        {
            super(a,b);
        }

    public int hashCode()
    {
        return x.hashCode() + y.hashCode();
    }

    public boolean equals(Object o)
    {
        if (!(o instanceof Pair))
            return false;

        Pair<E,E> p = (Pair<E,E>)o;
        
        return (p.x.equals(x) && p.y.equals(y)) || (p.x.equals(y) && p.y.equals(x)); 
    }
}
