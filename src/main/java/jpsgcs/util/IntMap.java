package jpsgcs.util;

public class IntMap<V>
{
    public IntMap(int capacity)
        {
            x = (IntItem<V>[]) new IntItem[capacity];
            head = null;
            used = 0;
        }

    public IntItem<V> find(int i)
    {
        int h = hash(i);
                
        for (IntItem<V> y = x[h]; y != null && y.h == h; y = y.next)
            if (y.i == i)
                return y;

        return null;
    }

    public IntItem<V> force(int i)
    {
        int h = hash(i);

        IntItem<V> z = null;

        for (IntItem<V> y = x[h]; y != null && y.h == h; y = y.next)
            if (y.i == i)
                {
                    z = y;
                    break;
                }

        if (z == null)
            {
                z = new IntItem<V>(i,null,h);
                insert(z,x[h]);
                x[h] = z;
            }
        
        return z;
    }

    public IntItem<V> head()
    {
        return head;
    }

    public void put(int i, V d)
    {
        IntItem<V> y = force(i);
        y.d = d;
    }

    public V get(int i)
    {
        IntItem<V> y = find(i);
        return y == null ? null : y.d;
    }

    public void resize(int capacity)
    {
        IntItem<V> y = head;

        x = (IntItem<V>[]) new IntItem[capacity];
        head = null;
        used = 0;
                
        while (y != null)
            {
                IntItem<V> z = y;
                y = y.next;
                z.next = z.prev = null;
                z.h = hash(z.i);
                insert(z,x[z.h]);
                x[z.h] = z;
            }
    }

    public void clean()
    {
        for (IntItem<V> y = head; y != null; )
            {
                IntItem<V> z = y;
                y = y.next;
                if (z.d == null)
                    remove(z);
            }
    }

    public void clear()
    {
        x = (IntItem<V>[]) new IntItem[x.length];
        head = null;
        used = 0;
    }
                
    public int size()
    {
        return used;
    }

    // Private data and methods.

    private IntItem<V>[] x = null;
    private IntItem<V> head = null;
    private int used = 0;

    private int hash(int i)
    {
        return i % x.length;
    }
        
    public void remove(IntItem<V> z)
    {
        int h = z.h;

        if (x[h] == z)
            {
                x[h] = z.next;
                if (x[h] != null && x[h].h != h)
                    x[h] = null;
            }
                
        if (head == z)
            head = z.next;

        if (z.next != null)
            z.next.prev = z.prev;
        if (z.prev != null)
            z.prev.next = z.next;
                
        z.next = z.prev = null;

        used--;
    }

    private void insert(IntItem<V> z, IntItem<V> y)
    {
        if (y == null)
            {
                z.next = head;
                if (z.next != null)
                    z.next.prev = z;
                head = z;
            }
        else
            {
                z.next = y;
                z.prev = y.prev;
                if (head == y)
                    head = z;

                if (z.next != null)
                    z.next.prev = z;
                if (z.prev != null)
                    z.prev.next = z;
            }
                
        used++;
    }
}
