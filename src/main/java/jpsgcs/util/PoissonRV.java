package jpsgcs.util;

/**
   This class delivers realizations from a Poisson distribution.
   The method used is inversion.
*/
public class PoissonRV 
{
    /**
       Creates a new Poisson random variable with rate 1.
    */ 
    public PoissonRV()
    {
        this(1);
    }

    /**
       Creates a new Poisson random variable with the given rate.
    */
    public PoissonRV(double rate)
    {
        set(rate);
    }

    /**
       Generates the next realization.
    */
    public int next()
    {
        return inverseDistribution(Math.random());
    }

    /**
       Sets the rate parameter to the given value.
    */
    public void set(double rate)
    {
        if (rate <= 0)
            throw new RuntimeException("Poisson rate must be positive");
        l = rate;
        mode = (int) l;
        pmode = mode * Math.log(l) - l;
        for (int i=mode; i>0; i--)
            pmode -= Math.log(i);
        pmode = Math.exp(pmode);
    }

    // Private data and methods.

    private double l = 1;
    private int mode = 0;
    private double pmode = 0;

    private int inverseDistribution(double u)
    {
        int i = mode;
        int j = mode;
        int k = mode;
        double pi = pmode;
        double pj = pmode;
        double q = pmode;
                
        while (u > q)
            {
                j++;
                pj *= l/j;
                q += pj;
                k = j;
                        
                if (u>q && i>0)
                    {
                        pi *= i/l;
                        i--;
                        q += pi;
                        k = i;
                    }
            }
                
        return k;
    }

    /** 
        Test main
    */
    public static void main(String[] args)
    {
        PoissonRV X = new PoissonRV(10);
        for (int i=0; i<10000; i++)
            System.out.println(X.next());
    }
}
