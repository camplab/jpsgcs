package jpsgcs.util;

public interface Curve
{
	public double f(double x);
}
