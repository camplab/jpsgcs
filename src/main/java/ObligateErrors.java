import jpsgcs.linkage.Linkage;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LocusProduct;
import jpsgcs.markov.GraphicalModel;
import java.util.Random;

/**
	This program checks genotype data on pedigrees and indicates whether
	the data at any particular locus is inconsistent with Mendelian 
	inheritance.

<ul>
        Usage : <b> java ObligateErrros input.par input.ped </b> </li>
</ul>
        where
<ul>
<li> <b> input.par </b> is a LINKAGE parameter file </li>
<li> <b> input.ped </b> is a LINKAGE pedigree file </li>
</ul>

<p>
	The program works through each pedigree and locus in turn and writes
	a list of errors to the standard output file.
<p>
	A more complete analysis which finds the posterior error probabilities for
	each genotype call, and hence makes finding the source of error easier, is done by 
	<a href="CheckErrors.html"> CheckErrors </a>.
*/

public class ObligateErrors
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			GeneticDataSource[] x = null; 

			switch(args.length)
			{
			case 2: x = Linkage.readAndSplit(args[0],args[1]);
				break;
			default:
				System.err.println("Usage: java ObligateErrors input.par input.ped");
				System.exit(1);
			}


			for (int k=0; k<x.length; k++)
			{
				x[k].downcodeAlleles();

				for (int i=0; i<x[k].nLoci(); i++)
				{
					LocusProduct p = new LocusProduct(x[k],i);
					GraphicalModel g = new GraphicalModel(p,rand,false);
					double prob = g.peel();
					if (!(g.peel() > 0))
						System.out.println("Pedigree "+(1+k)+" " +"\tlocus "+(1+i)+" "+x[k].locusName(i)+"\tobligatory error");
				}

				System.err.println();
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in ObligateErrors:main().");
			e.printStackTrace();
		}
	}
}
