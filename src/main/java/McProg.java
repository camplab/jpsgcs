import jpsgcs.linkage.Linkage;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.util.Main;
import java.io.IOException;

import jpsgcs.markov.Table;
import jpsgcs.markov.Function;

import jpsgcs.pedmcmc.LocusSampler;
import jpsgcs.pedmcmc.LocusMeiosisSampler;
import jpsgcs.pedmcmc.LDLocusSampler;
import jpsgcs.pedmcmc.ExtendedLMSampler;
import jpsgcs.pedmcmc.TrueMultiLocusSampler;

import java.util.Random;

/** 
	This is a utility class for handling input to MCMC programs
	such as McLinkage, McTLods etc. It cannot itself be called directly
	from the command line.
*/
	
public class McProg
{
	public static boolean veryverbose = false;
	public static boolean verbose = true;
	public static int n_samples = 1000;
	public static int n_burnin = 0;
	public static int sampler = 0;
	public static double error = 0;
	public static int maxerrall = 0;
	public static GeneticDataSource[] alldata = null;
	public static LinkageDataSet linkdata = null;
	public static LDModel ldmodel = null;
	public static double ldepsilon = 0;

	public static void readOptions(String[] args, String prog) throws IOException
	{
		String[] bargs = Main.strip(args,"-t");
		if (bargs != args)
		{
			verbose = false;
			args = bargs;
		}

		bargs = Main.strip(args,"-v");
		if (bargs != args)
		{
			verbose = true;
			args = bargs;
		}

		bargs = Main.strip(args,"-vv");
		if (bargs != args)
		{
			veryverbose = true;
			verbose = true;
			LocusSampler.verbo = true;
			args = bargs;
		}

		switch (args.length)
		{
		case 6: sampler = Integer.parseInt(args[5]);

		case 5: error = Double.parseDouble(args[4]);
			if (error > 0)
				maxerrall = 4;

		case 4: n_burnin = Integer.parseInt(args[3]);
			
		case 3: n_samples = Integer.parseInt(args[2]);

		case 2: readData(args[0],args[1]);
			break;

		default:
			System.err.println("Usage: java "+prog+" input.par input.ped [n_samples] [n_burnin] [error_prob] [-v/t]");
			System.exit(1);
		}
	}

	public static void readData(String par, String ped) throws IOException
	{
		LinkageFormatter parin = new LinkageFormatter(par);
		LinkageFormatter pedin = new LinkageFormatter(ped);
		linkdata = new LinkageDataSet(parin,pedin);

		LinkageDataSet[] splits = linkdata.splitByPedigree();

		alldata = new GeneticDataSource[splits.length];
		for (int i=0; i<splits.length; i++)
			alldata[i] = new LinkageInterface(splits[i]);

		ldmodel = new LDModel(parin,ldepsilon);

		if (sampler == 3)
		{
			if (ldmodel.getLocusVariables() == null)
			{
				System.err.println("Warning: LD sampler requested but parameter file contains no LD model");
				System.err.println("\tMaking LD model using linkage equilibrium with allele frequencies from paramter file");
				ldmodel = new LDModel(alldata[0]);
			}
		}
		else
		{
			if (ldmodel.getLocusVariables() != null)
			{
				System.err.println("Warning: LD model specified in file, but non LD sampler requested. Ignoring specified LD model.");
			}

			ldmodel = null;
		}
	}

	public static LocusSampler makeSampler(GeneticDataSource data, boolean linkfirst, Random r)
	{
		LocusSampler mc = null;

		switch (sampler)
		{
		case 4: mc = new TrueMultiLocusSampler(data,linkfirst,r);
			if (verbose)
				System.err.println("Using TrueMultiLocusSampler");
			break;

		case 3: mc = new LDLocusSampler(data,ldmodel,linkfirst,error,maxerrall,r);
			if (verbose)
				System.err.println("Using LDLocusSampler");
			break;

		case 2: mc = new LocusSampler(data,linkfirst,error,maxerrall,r);
			if (verbose)
				System.err.println("Using LocusSampler");
			break;

		case 1: mc = new LocusMeiosisSampler(data,linkfirst,error,maxerrall,r);
			if (verbose)
				System.err.println("Using LocusMeiosisSampler");
			break;

		case 0:
		default:
			mc = new ExtendedLMSampler(data,linkfirst,error,maxerrall,r);
			if (verbose)
				System.err.println("Using ExtendedLMSampler");
		}

		return mc;
	}	
}
