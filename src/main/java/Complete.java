import jpsgcs.util.Monitor;
import jpsgcs.util.InputFormatter;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.Linkage;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.SteppedLinkageData;
import jpsgcs.markov.GraphicalModel;
import jpsgcs.markov.MarkovRandomField;
import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;
import jpsgcs.pedmcmc.Genotype;
import jpsgcs.pedmcmc.GenotypePenetrance;
import jpsgcs.pedapps.CompletingPenetrance;
import jpsgcs.pedmcmc.GenotypePrior;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.pedmcmc.Error;
import jpsgcs.pedmcmc.ErrorPrior;
import jpsgcs.util.Main;
import jpsgcs.linkage.LinkageFormatter;
import java.util.Random;

/**
	This program takes genotype data, specified as LINKAGE parameter and pedigree files,
	and either imputes or samples complete phase known hapolotype data, using a specified
	graphical model for linkage disequilibrum. 
<ul>
	<li> Usage : <b> java Complete input.parld input.ped > output.ped [-r] </b><li>
</ul>
	where
<ul>
<li> <b> input.parld </b> is a LINKAGE parameter file with a graphical model for LD attached as 
made by the FitGMLD program </li>
<li> <b> input.ped </b> is the input LINKAGE pedigree file </li>
<li> <b> -r </b> is an optional parameter which will make the program sample the haplotype imputation
at random from the posterior distribution of haplotypes given the genotype data. The default action
is to find a haplotype configuration that maximizes the posterior probability. </li>
</ul>
<p>
	The output is a linkage pedigree file with
	the same structure as the input pedigree file but with missing genotypes imputed, and
	the alleles phased so that the first allele at each locus is on one haplotype and the
	second on the other.
<p>
	The individuals are assumed to be unrelated, and any relationships specified in the
	input pedigree file will be ignored in the haplotyping.
	
*/

public class Complete
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			Monitor.quiet(Boolean.valueOf(System.getProperty("JPSGCS.monitor.quiet", "false")));

			boolean sample = false;
			double eprob = 0.001;
			double ldepsilon = 0.0001;

			LinkageFormatter parin = null;
			LinkageFormatter pedin = null;

			String[] bargs = Main.strip(args,"-r");
			if (bargs != args)
			{
				sample = true;
				args = bargs;
			}

			switch (args.length)
			{
			case 2: parin = new LinkageFormatter(args[0]);
				pedin = new LinkageFormatter(args[1]);
				break;

			default:
				System.err.println("Usage: java Complete in.parld in.ped [-r] > out.ped");
				System.exit(1);
			}

			SteppedLinkageData sld = new SteppedLinkageData(parin,pedin);
			GeneticDataSource x = new LinkageInterface(sld);

			LDModel ld = new LDModel(parin,ldepsilon);
			if (ld.getLocusVariables() == null)
			{
				System.err.println("Warning: parameter file has no LD model appended.");
				System.err.println("Will assume linakge equilibrium and given allele frequencies.");
				ld = new LDModel(x);
			}

			MarkovRandomField m = new MarkovRandomField();
			Genotype[] g = new Genotype[x.nLoci()];
			GenotypePenetrance[] p = new GenotypePenetrance[x.nLoci()];

			// This trick using the dummy variable allows the graphical model
			// to be compiled, but with the penetrance function changeable.
			Variable dummy = new Variable(1);

			for (int i=0; i<p.length; i++)
			{
				Error err = new Error();
				m.add(new ErrorPrior(err,eprob));

				g[i] = new Genotype(x.nAlleles(i)); 
			//	p[i] = new CompletingPenetrance(g[i],err,dummy);
				p[i] = new GenotypePenetrance(g[i]);
				m.add(p[i]);
			}

			m.remove(dummy);

			Monitor.show("Read     ");

			for (Function f : ld.duplicate(g).getFunctions())
				m.add(f);

			Monitor.show("Duplicated ");

			GraphicalModel gm = new GraphicalModel(m,rand,false);

			Monitor.show("Imputing ");

			while (sld.next())
			{
				for (int i=0; i<x.nLoci(); i++)
					p[i].fix(x.penetrance(i,0));

				if (sample)
					gm.simulate();
				else
					gm.maximize();
				
				for (int i=0; i<x.nLoci(); i++)
					x.setAlleles(i,0,g[i].pat(),g[i].mat());
				
				x.writeIndividual(0,System.out);
				System.out.println();
				System.err.print(".");
			}

			System.err.println();

			Monitor.show("Done     ");
		}
		catch (Exception e)
		{
			System.err.println("Caught in Complete.main()");
			e.printStackTrace();
		}
	}
}
