import jpsgcs.linkage.Linkage;
import jpsgcs.genio.Genio;
import jpsgcs.genio.BasicGeneticData;

/** 
	Takes data from LINKAGE format parameter and pedigree files and
	prepares it for input into the PHASE haplotyping program.

<ul>
	Usage : <b> java LinkageToPhase input.par input.ped </b> </li>
</ul>
	where 
<ul>
<li> <b> input.par </b> is the input LINKAGE parameter file. </li>
<li> <b> input.ped </b> is the input LINKAGE pedigree file. </li>
</ul>

*/

public class LinkageToPhase
{
	public static void main(String[] args)
	{
		try
		{
			switch(args.length)
			{
			case 2:
				break;
			default:
				System.err.println("Usage: java LinkageToPhase input.par input.ped");
				System.exit(0);
			}

			BasicGeneticData x = Linkage.read(args[0],args[1]);
			Genio.writeAsPhase(x,System.out);
		}
		catch (Exception e)
		{
			System.err.println("Caught in LinkageToPhase:main()");
			e.printStackTrace();
		}
	}
}
