import jpsgcs.linkage.LinkageInterface;
import jpsgcs.util.Main;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.pedmcmc.LocusSampler;
import jpsgcs.pedapps.FullInfoLinkageData;
import jpsgcs.pedapps.LogLikelihood;
import jpsgcs.pedapps.AncestralAlleles;
import jpsgcs.pedapps.HaplotypeGraphFrame;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Random;


public class McComplete extends McProg
{
	public static void main(String[] args)
	{
		try
		{
			Random rand = new Random();

			verbose = true;
			n_samples = 1000;
			n_burnin = 0;
			sampler = 3;
			error = 0.01;
			maxerrall = 4;
			ldepsilon = 0.0001;

			readOptions(args,"McComplete");

			System.err.println("Read data");

			for (GeneticDataSource data : alldata)
			{
				System.err.println("Pedigree: "+data.pedigreeName(0));

				LocusSampler mc = makeSampler(data,true,rand);

				System.err.println("Made sampler");

				mc.initialize();

				System.err.println("Initialized sampler");

				for (int i=0; i<n_samples; i++)
				{
					mc.sample();
					System.err.print(".");
				}

				System.err.println();

				for (int i=0; i<n_samples; i++)
				{
					mc.maximize();
					System.err.print("+");
				}

				System.err.println();

				for (int i=0; i<data.nLoci(); i++)
					for (int j=0; j<data.nIndividuals(); j++)
					{
						int a0 = mc.getAlleles()[i][j][0].getState();
						int a1 = mc.getAlleles()[i][j][1].getState();
						data.setAlleles(i,j,a0,a1);
					}

				data.writePedigree(System.out);
			}
		}
		catch (Exception e)
		{
			System.err.println("Caught in McComplete:main()");
			e.printStackTrace();
		}
	}
 }
