package jpsgcs.cusp;

import jpsgcs.util.InputFormatter;
import jpsgcs.graph.Network;
import jpsgcs.util.UnorderedPair;
import jpsgcs.viewgraph.GraphFrame;
import jpsgcs.markov.Parameter;
import jpsgcs.animate.ParameterScrollWidget;

import java.util.TreeSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.awt.*;

public class Test
{
    public static void main(String[] args)
    {
        try
            {
                InputFormatter f = new InputFormatter();
                Set<Ping> tree = new TreeSet<Ping>();
                        
                int min = Integer.MAX_VALUE;
                int max = 0;

                while (f.newLine())
                    {
                        int a = f.nextInt();
                        int b = f.nextInt();
                        int c = f.nextInt();
                        int d = f.nextInt();

                        if (min > c)
                            min = c;

                        if (max < c+d)
                            max = c+d;

                        // for (int i=c; i<c+d; i++)
                        //     tree.plus(new Ping(a,b,i));
                    }

                System.err.println("Data read in.");

                Set<UnorderedPair<Integer>>[] p = (Set<UnorderedPair<Integer>>[] ) new Set[max];
                for (int i=min; i<max; i++)
                    p[i] = new LinkedHashSet<UnorderedPair<Integer>>();

                Network<Integer,Object> g = new Network<Integer,Object>();

                for (Ping q : tree)
                    {
                        // g.plus(q.x);
                        // g.plus(q.y);
                        p[q.t].add(new UnorderedPair<Integer>(q.x,q.y));
                    }

                Parameter snooze = new Parameter("Speed",0,20,0);
                Parameter time = new Parameter("Time",0,100,0);
                Parameter[] pars = {snooze, time};
                GraphFrame<Integer,Object> gf = new GraphFrame<Integer,Object>(g,pars);
                ParameterScrollWidget psw = gf.getParameterScrollbar(time);

                System.err.println("Graph made");

                for (int i=min; i<max; i++)
                    {
                        while ((int)snooze.getValue() == 0)
                            Thread.sleep(100);

                        if (i == min)
                            {
                                for (UnorderedPair<Integer> q : p[min])
                                    g.connect(q.x,q.y);
                                continue;
                            }

                        Set<UnorderedPair<Integer>> cut = new LinkedHashSet<UnorderedPair<Integer>>(p[i-1]);
                        cut.remove(p[i]);
                        for (UnorderedPair<Integer> q : cut)
                            g.disconnect(q.x,q.y);

                        Set<UnorderedPair<Integer>> add = new LinkedHashSet<UnorderedPair<Integer>>(p[i]);
                        add.remove(p[i-1]);
                        for (UnorderedPair<Integer> q : add)
                            g.connect(q.x,q.y);

                        psw.setParameterValue((i-min)*100.0/(double)(max-min));

                        for (int j=0; j<10; j++)
                            Thread.sleep(401-(int)(snooze.getValue()*snooze.getValue()));
                    }
            }
        catch (Exception e)
            {
                e.printStackTrace();
            }
    }

}
