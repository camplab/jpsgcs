package jpsgcs.random.example;

import jpsgcs.random.*;

public class TestStudent
{
    public static void main(String[] args)
    {
        int[] a = {1,2,3,1000}; 
        int t = 10000;
        for (int i=0; i<4; i++)
            {
                StudentsRV T = new StudentsRV(a[i]);
                for (int j=0; j<10; j++)
                    { 
                        double m = 0;
                        double s = 0;
                        for (int k=0; k<t; k++)
                            {
                                double x = T.next();
                                m += x;
                                s += x*x;
                            }
                        System.out.println(m/t+" "+(s-m*m/t)/t);
                    }
                System.out.println();
            }
    }
}
