package jpsgcs.random.example;

import jpsgcs.random.*;

public class TestBinomial
{
    public static void main(String[] args)
    {
        int t = 100000;
        for (int n=10; n<1000000; n*=10)
            {
                for (double p=0.1; p<1.0; p+=0.2)
                    {
                        BinomialRV B = new BinomialRV(n,p);
                        System.out.println("True Mean = "+n*p+" True Variance = "+n*p*(1-p));
                        double m = 0;
                        double s = 0;
                        for (int i=0; i<t; i++)
                            {
                                double x = B.next();
                                m += x;
                                s += x*x;
                            }
                        System.out.println(" Est Mean = "+m/t+"  Est Variance = "+(s-m*m/t)/t);
                        System.out.println();
                    }
            }
    }
}
