package jpsgcs.random.example;

import jpsgcs.random.*;

public class TestGamma
{
    public static void main(String[] args)
    {
        int t = 100000;
        for (double a=0.1; a<10; a+=0.2)
            for (double b = 0.01; b<2; b+=0.5)
                {
                    System.out.println("\nTrue mean = "+a/b+" True variance = "+a/b/b);
                    GammaRV G = new GammaRV(a,b);
                    double m = 0;
                    double s = 0;
                    for (int i=0; i<t; i++)
                        {
                            double x = G.next();
                            m += x;
                            s += x*x;
                        }
                    System.out.println(" Est mean = "+m/t+" Est variance = "+(s-m*m/t)/t);
                }
    }
}
