
import jpsgcs.animate.Animator;
import jpsgcs.animate.Paintable;

import java.awt.Graphics;
import java.awt.Color;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Random;

public class TestAnimator
{
    public static void main(String[] args)
    {
        try
            {
                Random rand = new Random();

                Collection<Paintable> p = new LinkedHashSet<Paintable>();
                        
                for (int i=0; i<1000; i++)
                    {
                        Blob b = new Blob();
                        b.setX(rand.nextGaussian()*100);
                        b.setY(rand.nextGaussian()*100);

                        switch (i % 5)
                            {
                            case 0: b.setColor(Color.red);
                                break;
                            case 1: b.setColor(Color.yellow);
                                break;
                            case 2: b.setColor(Color.green);
                                break;
                            case 3: b.setColor(Color.cyan);
                                break;
                            case 4: b.setColor(Color.blue);
                                break;
                            }

                        p.add(b);
                    }
                        
                Animator a = new Animator(p);

                while (true)
                    {
                        for (Paintable q : p)
                            {
                                Blob b = (Blob) q;
                                b.setX(b.getX() + rand.nextGaussian());
                                b.setY(b.getY() + rand.nextGaussian());
                            }

                        Thread.sleep(1);
                    }
            }
        catch (Exception e)
            {
                e.printStackTrace();
            }
    }
}

class Blob implements Paintable
{
    private double x = 0;
    private double y = 0;

    private double w = 20;
    private double h = 20;

    private Color col = Color.red;

    public void setX(double d)
    {
        x = d;
        while (x > 500)
            x -= 1000;
        while (x < -500)
            x += 1000;
    }

    public void setY(double d)
    {
        y = d;
        while (y > 500)
            y -= 1000;
        while (y < -500)
            y += 1000;
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public void setColor(Color c)
    {
        col = c;
    }

    public void paint(Graphics g)
    {
        g.setColor(col);
        g.fillOval((int)(x-w/2),(int)(y-h/2),(int)w,(int)h);
        g.setColor(Color.black);
        g.drawOval((int)(x-w/2),(int)(y-h/2),(int)w,(int)h);
    }
}
